<!DOCTYPE html>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta id="myViewport" name="viewport" content="width=device-width, initial-scale=0.5">
    <title>Glutera - Home</title>
    <!-- UIkit CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/uikit.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css" />
</head>

<body>
    <header id="gt-header" class="uk-section uk-section-xsmall uk-section-default">
        <div class="uk-container uk-container-large">
            <div uk-grid>
                <div class="uk-width-1-1">
                    <nav class="uk-navbar" uk-navbar>
                        <div class="uk-navbar-left">
                            <div class="uk-margin">
                                <a href="<?php echo base_url();?>" class="uk-logo">
                                    <img src="<?php echo base_url();?>assets/img/orange-logo.png" alt="" uk-img>
                                </a>
                            </div>
                        </div>
                        <div class="uk-navbar-right">
                            <div class="uk-margin">
                                <?php echo form_open('search', 'class="uk-search uk-search-default" method="get'); ?>
                                    <a href="" class="uk-search-icon-flip gt-color-icon" uk-search-icon></a>
                                    <input class="uk-search-input uk-border-pill uk-text-small gt-header-search gt-background-search" type="search" placeholder="search" name="ar">
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header><!-- /header -->
    <section id="gt-headline-news" class="uk-section uk-section-xsmall uk-section-default">
        <div class="uk-container uk-container-large">
            <div uk-grid>
                <div class="uk-width-1-1">
                    <div class="uk-card uk-card-default uk-card-body gt-wrap-headline-news" uk-grid>
                        <div class="uk-width-1-5@s">
                            <ul class="uk-list gt-list-socmed">
                                <?php foreach ($socials as $social): ?>
                                    <?php if ($social->soc_guid != ""): ?>
                                        <li>
                                            <a href="<?php echo $social->soc_guid ?>" class="uk-icon-button uk-light" uk-icon="<?php echo $social->soc_icon ?>"></a>
                                        </li>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </ul>
                        </div>
                        <div class="uk-width-4-5@s uk-flex uk-flex-right gt-background-news" uk-grid>
                            <div class="uk-width-2-5@s">
                                <div class="gt-list-news">
                                    <?php foreach($newpost as $post) : ?>
                                        <?php if ($post['count'] != 0): ?>
                                            <a href="<?php echo $post['pst_guid'] ?>">
                                            <div style="background: url('<?php echo $post["img"];?>'); background-size: cover; background-repeat: no-repeat;" class="gt-item-news" data-title="<?php echo $post['pst_head'] ?>" data-url="<?php echo $post['pst_guid'] ?>"></div>
                                            </a>
                                        <?php else : ?>
                                            <a href="#">
                                            <div style="background: url('<?php echo base_url();?>attachments/00-default.jpg'); background-size: cover; background-repeat: no-repeat;" class="gt-item-news" data-title="Belum ada artikel" data-url="#"></div>
                                            </a>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                                <a href="#" id="gt-prev-news" class="uk-icon-button gt-prev-news" uk-icon="chevron-left"></a>
                                <a href="#" id="gt-next-news" class="uk-icon-button gt-next-news" uk-icon="chevron-right"></a>
                                <div class="gt-headline-description">
                                    <h4 class="uk-text-uppercase uk-text-small uk-light uk-margin-remove gt-the-newest">The Newest</h4>
                                    <a id="gt-link-title" href="#" target="_blank">
                                        <h3 id="gt-headline-title" class="uk-text-lead uk-margin-remove-top" style="text-transform: capitalize;"></h3>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /headline-news -->
    <section id="gt-menu" class="uk-section uk-section-xsmall uk-section-default">
        <div class="uk-container uk-container-large">
            <div uk-grid>
                <div class="uk-width-1-1">
                    <nav class="uk-navbar" uk-navbar>
                        <div class="uk-navbar-center">
                            <ul class="uk-navbar-nav gt-list-menu"> 
                                <?php if (!empty($content)):
                                    $i = 0;
                                    foreach ($content as $dContent) : $i += 1; ?>
                                        <li id="<?php echo 'menu'.$i ?>" data-slider="<?php echo $i ?>" <?php echo $i==2?'class="uk-active"':'' ?> ><a href="<?php echo $dContent['cat_guid'] ?>"><?php echo $dContent['cat_name']; ?></a></li>
                                    <?php endforeach; ?>
                                    <?php if (!empty($video->vdo_id)): ?>
                                        <li><a href="<?php echo base_url() ?>videos/">All Videos</a></li>
                                    <?php endif ?>
                                    <!-- <?php echo count($video->vdo_id) ?> -->
                                <?php else : ?>
                                    <li id="menu1" data-slider="1"><a href="">Belum ada menu</a></li>
                                <?php endif ?>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </section><!-- /menu -->
    <section id="gt-thumbnail" class="uk-section uk-section-xsmall uk-section-default">
        <div class="uk-container gt-container">
            <div uk-grid>
                <div class="uk-width-1-1">
                    <div id="gt-custom-slider" class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider="finite: false; draggable: false;">
                        <ul class="uk-slider-items uk-child-width-1-3 uk-child-width-1-4@s uk-child-width-1-4@m gt-slider-thumbnail">
                            <?php if (!empty($content)):
                                $i = 0;
                                foreach ($content as $dContent) : $i += 1; ?>
                                    <li id="<?php echo "slider".$i ?>" data-menu="<?php echo "menu".$i ?>" data-dot="<?php echo "dot".$i ?>" <?php echo $i==2?'class="gt-item-active"':'' ?> >
                                        <a href="<?php echo $dContent['pguid'] ?>">
                                            <img src="<?php echo $dContent['img'] ?>" alt="<?php echo $dContent['phead'] ?>" >
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <li id="slider1" data-menu="menu1" data-dot="dot1">
                                    <a href="#">
                                        <img src="<?php echo base_url();?>attachments/00-default.jpg" alt="">
                                    </a>
                                </li>
                            <?php endif ?>
                        </ul>
                    </div>
                    <ul class="gt-dot-slider">
                        <?php if (!empty($content)):
                            $i = 0;
                            foreach ($content as $dContent) : $i += 1; ?>
                                <li id="<?php echo "dot".$i ?>" data-slider="<?php echo $i ?>" <?php echo $i==2?'class="active"':'' ?> ></li>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <li id="dot1" data-slider="1"></li>
                        <?php endif ?>
                    </ul>
                </div>
            </div>
        </div>
    </section><!-- /thumbnail -->
    <section id="gt-subscribe" class="uk-section uk-section-xsmall uk-section-default">
        <div class="uk-container uk-container-large">
            <div uk-grid>
                <div class="uk-width-1-1">
                    <div class="uk-card uk-card-default uk-card-body uk-card-large gt-wrap-subscribe uk-flex uk-flex-center" uk-grid>
                        <div class="uk-width-2-5@m uk-width-3-5@s">
                            <a href="<?php echo base_url() ?>gallery/">
                            	<img src="<?php echo base_url();?>assets/img/everlasting-logo.png" alt="" uk-img>
                            </a>
                            <br>
                            <div class="uk-inline gt-subscribe-form">
                                <a class="uk-form-icon uk-form-icon-flip uk-border-circle uk-light" href="#" uk-icon="icon: check;"></a>
                                <input class="uk-input uk-border-pill uk-text-small" type="text" placeholder="put your email">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /subscribe -->
    <footer id="gt-footer" class="uk-section uk-section-xsmall uk-section-default">
        <div class="uk-container uk-container-large">
            <div uk-grid>
                <div class="uk-width-1-1 uk-text-center">
                    <p class="uk-margin-remove uk-text-small">GRIDLABS Studio</p>
                    <p class="uk-margin-remove uk-text-small uk-text-muted">Allrights reserved.</p>
                </div>
            </div>
        </div>
    </footer><!-- /footer -->
    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.4.1.min.js"></script>
    <!-- UIkit JS -->
    <script src="<?php echo base_url();?>assets/js/uikit.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/uikit-icons.min.js"></script>
    <!-- Main JS -->
    <script>
    $(document).ready(function() {
        var totalItem = $('.gt-list-news div').length;
        var arrItems = ['gt-item-active', 'gt-item-second', 'gt-item-third'];

        var arrItems21 = ['gt-item-second', 'gt-item-active'];
        var arrItems22 = ['gt-item-active', 'gt-item-second'];

        var prevArrItems31 = ['gt-item-third', 'gt-item-active', 'gt-item-second'];
        var prevArrItems32 = ['gt-item-second', 'gt-item-third', 'gt-item-active'];
        var prevArrItems33 = ['gt-item-active', 'gt-item-second', 'gt-item-third'];

        var nextArrItems31 = ['gt-item-second', 'gt-item-third', 'gt-item-active'];
        var nextArrItems32 = ['gt-item-active', 'gt-item-second', 'gt-item-third'];
        var nextArrItems33 = ['gt-item-third', 'gt-item-active', 'gt-item-second'];

        // Setup class item news
        var i = 0;
        $('.gt-list-news div').each(function() {
            $(this).addClass(arrItems[i]);

            if (i > 2) {
                $(this).hide();
            }

            if (arrItems[i] == 'gt-item-active') {
                var title = $(this).data('title');
                var url = $(this).data('url');

                $('#gt-headline-title').html(title);
                $('#gt-link-title').attr('href', url);
            }

            i++;
        });

        // Setup navigation item news
        if (totalItem < 2) {
            $('#gt-prev-news').hide();
            $('#gt-next-news').hide();
        }

        // Previous News
        $('#gt-prev-news').on('click', (function(e) {
            e.preventDefault();

            var currentActive = 0;
            var strActive = "gt-item-news gt-item-active";

            // Get index of active item
            var i = 0;
            $('.gt-list-news div').each(function() {
                if (strActive.indexOf($(this).attr('class')) >= 0) {
                    currentActive = i;
                }

                i++;
            })

            switch (currentActive) {
                case 0:
                    if (totalItem == 2) {
                        currentPosition = arrItems21;
                    } else {
                        currentPosition = prevArrItems31;
                    }
                    break;
                case 1:
                    if (totalItem == 2) {
                        currentPosition = arrItems22;
                    } else {
                        currentPosition = prevArrItems32;
                    }
                    break;
                case 2:
                    currentPosition = prevArrItems33;
                    break;
            }

            // Setup class item news
            var i = 0;
            $('.gt-list-news div').each(function() {
                $(this).removeAttr('class');

                $(this).addClass('gt-item-news');
                $(this).addClass(currentPosition[i]);

                if (currentPosition[i] == 'gt-item-active') {
                    var title = $(this).data('title');
                    var url = $(this).data('url');

                    $('#gt-headline-title').html(title);
                    $('#gt-link-title').attr('href', url);
                }

                i++;
            });
        }))

        // Next News
        $('#gt-next-news').on('click', (function(e) {
            e.preventDefault();

            var currentActive = 0;
            var strActive = "gt-item-news gt-item-active";
            var currentPosition = [];

            // Get index of active item
            var i = 0;
            $('.gt-list-news div').each(function() {
                if (strActive.indexOf($(this).attr('class')) >= 0) {
                    currentActive = i;
                }

                i++;
            })

            switch (currentActive) {
                case 0:
                    if (totalItem == 2) {
                        currentPosition = arrItems21;
                    } else {
                        currentPosition = nextArrItems31;
                    }
                    break;
                case 1:
                    if (totalItem == 2) {
                        currentPosition = arrItems22;
                    } else {
                        currentPosition = nextArrItems32;
                    }
                    break;
                case 2:
                    currentPosition = nextArrItems33;
                    break;
            }

            // Setup class item news
            var i = 0;
            $('.gt-list-news div').each(function() {
                $(this).removeAttr('class');

                $(this).addClass('gt-item-news');
                $(this).addClass(currentPosition[i]);

                if (currentPosition[i] == 'gt-item-active') {
                    var title = $(this).data('title');
                    var url = $(this).data('url');

                    $('#gt-headline-title').html(title);
                    $('#gt-link-title').attr('href', url);
                }

                i++;
            });
        }))

        $('.gt-list-menu li').on('click', (function(e) {
            e.preventDefault();
            var href = $(this).find('a').attr('href');
            document.location.href = href;

            var i = $(this).data('slider') - 2;
            var idSlider = "#slider" + $(this).data('slider');

            UIkit.slider('#gt-custom-slider').show(i);

            var menu = $('.gt-slider-thumbnail li' + idSlider).data('menu');
            var dot = $('.gt-slider-thumbnail li' + idSlider).data('dot');

            $('.gt-slider-thumbnail li').removeClass('gt-item-active');
            $('.gt-slider-thumbnail li' + idSlider).addClass('gt-item-active');

            $('.gt-list-menu li').removeClass('uk-active');
            $('.gt-list-menu #' + menu).addClass('uk-active');

            $('.gt-dot-slider li').removeClass('active');
            $('.gt-dot-slider #' + dot).addClass('active');
        }))

        $('.gt-dot-slider li').on('click', (function(e) {
            e.preventDefault();

            var i = $(this).data('slider') - 2;
            var idSlider = "#slider" + $(this).data('slider');

            UIkit.slider('#gt-custom-slider').show(i);

            var menu = $('.gt-slider-thumbnail li' + idSlider).data('menu');
            var dot = $('.gt-slider-thumbnail li' + idSlider).data('dot');

            $('.gt-slider-thumbnail li').removeClass('gt-item-active');
            $('.gt-slider-thumbnail li' + idSlider).addClass('gt-item-active');

            $('.gt-list-menu li').removeClass('uk-active');
            $('.gt-list-menu #' + menu).addClass('uk-active');

            $('.gt-dot-slider li').removeClass('active');
            $('.gt-dot-slider #' + dot).addClass('active');
        }))

        if(screen.width >= 750) {
            $('#myViewport').attr('content', 'width=device-width, initial-scale=0.5');
        } else {
            $('#myViewport').attr('content', 'width=device-width, initial-scale=0.47');
        }
        
        $(window).on('resize', (function() {
            if(screen.width >= 750) {
                $('#myViewport').attr('content', 'width=device-width, initial-scale=0.5');
            } else {
                $('#myViewport').attr('content', 'width=device-width, initial-scale=0.47');
            }
        }))
    })
    </script>
</body>

</html>