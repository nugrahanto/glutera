
<?=$head?>

<body class="animsition">
    <div class="page-wrapper">
        
        <!-- MENU OPENER -->
        <?=$menu?>
        <!-- END MENU -->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?=$header?>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="overview-wrap">
                                    <h2 class="title-1">Glutera</h2>
                                    <a class="au-btn au-btn-icon au-btn--blue" style="color: white" href="<?php echo site_url('dashboard/admin/addpost/') ?>">
                                        <i class="zmdi zmdi-plus"></i>add post</a>
                                </div>
                            </div>
                        </div>
                        <div class="row m-t-25">
                            <div class="col-sm-6 col-lg-4">
                                <div class="overview-item overview-item--c3 pb-1">
                                    <div class="overview__inner mb-4">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-calendar-note"></i>
                                            </div>
                                            <div class="text">
                                                <span>Total Artikel Terpublish</span>
                                                <h2><?php echo $posts[0]['count']; ?></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c2 pb-1">
                                    <div class="overview__inner mb-4">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-videocam"></i>
                                            </div>
                                            <div class="text">
                                                <span>Total Video</span>
                                                <h2><?php echo $cvideo; ?></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <div class="overview-item overview-item--c1 pb-1">
                                    <div class="overview__inner mb-4">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-account-o"></i>
                                            </div>
                                            <div class="text">
                                                <span>Total Seluruh User</span>
                                                <h2><?php echo $cuser; ?></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="au-card au-card--no-shadow au-card--no-pad m-b-40">
                                    <div class="au-card-title" style="background-image:url('images/bg-title-02.jpg');">
                                        <div class="bg-overlay bg-overlay--blue"></div>
                                        <h3>
                                            <i class="zmdi zmdi-comment-text"></i>Artikel Terbaru</h3>
                                        <a href="<?php echo site_url('dashboard/admin/addpost/') ?>" class="au-btn-plus">
                                            <i class="zmdi zmdi-plus"></i>
                                        </a>
                                    </div>
                                    <div class="au-inbox-wrap js-inbox-wrap">
                                        <div class="au-message js-list-load">
                                            <div class="au-message__noti">
                                                <div class="row">
                                                    <div class="col-lg-12 m-b-15">
                                                        <p><i><?php echo date('d F Y', strtotime($posts[0]['created_date'])) ." - ". $posts[0]['created_by']; ?></i></p>
                                                    </div>
                                                    <div class="col-lg-10">
                                                        <p><h3><a style="color: black;" href="<?php echo base_url() ?>dashboard/admin/editpost/<?php echo $posts[0]['pst_slug'] ?>"><?php echo $posts[0]['pst_head']; ?></a></h2></p>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <p><a href="<?php echo $posts[0]['pst_guid'] ?>"><i>visit</i></a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="au-card chart-percent-card">
                                    <div class="au-card-inner">
                                        <div class="row">
                                            <div class="col-lg-7"> <h3 class="title-2 tm-b-5">List Page</h3> </div>
                                            <div class="col-lg-5">
                                            <?php if ($this->session->userdata['uStat']== "root") : ?>
                                                <div class="table-data__tool-right">
                                                    <a class="au-btn au-btn-icon au-btn--green au-btn--small" style="color: white" href="<?php echo site_url('dashboard/admin/category/') ?>">
                                                    <i class="zmdi zmdi-plus"></i>add item</a>
                                                </div>
                                            <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="row no-gutters">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <div class="checkbox">
                                                            <label class="form-check-label ">
                                                            <?php foreach($category as $cat) : ?>
                                                                <ul>
                                                                    <li><?php echo $cat->ctr_name; ?></li>
                                                                </ul>
                                                            <?php endforeach; ?>
                                                            </label>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="<?=base_url()?>application/libraries/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?=base_url()?>application/libraries/vendor/slick/slick.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/wow/wow.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/animsition/animsition.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="<?=base_url()?>application/libraries/js/main.js"></script>

</body>

</html>
<!-- end document-->
