
<?=$head?>

<body class="animsition">
    <div id="message">
        <?php echo $this->session->tempdata('alert_form'); ?>
    </div>
    <div class="page-wrapper">
        
        <!-- MENU OPENER -->
        <?=$menu?>
        <!-- END MENU -->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?=$header?>
            <!-- HEADER DESKTOP-->
            
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">

                            <div class="col-lg-8">
                                <?php echo form_open('dashboard/admin/f_socialupdate');  ?>
                                <div class="card">
                                    <div class="card-header"><b>Pengaturan Social Media</b></div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label style="font-size: 14px; padding: 5px; color: #a0a0a0;">
                                                <i>Masukkan link social media glutera, pengaturan social media ini berguna untuk menampilkan alamat social media dari glutera melalui icon yang berada di halaman utama website glutera.</i>
                                            </label>
                                        </div>
                                        <hr>
                                        <?php foreach ($socials as $social): ?>
                                        <div class="form-group">
                                            <label class="control-label mb-1"><b><?php echo $social->soc_name; ?></b></label>
                                            <div class="row">
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <label for="cc-exp" class="control-label mb-1" style="padding: 5px; color: #a0a0a0;"><i>Icon</i></label>
                                                        <input id="ic-<?php echo $social->soc_name ?>" name="ic-<?php echo $social->soc_name ?>" type="text" class="form-control cc-exp" value="<?php echo $social->soc_icon; ?>" data-val="true" >
                                                    </div>
                                                </div>
                                                <div class="col-8">
                                                    <label for="x_card_code" class="control-label mb-1" style="padding: 5px; color: #a0a0a0;"><i>Link</i></label>
                                                    <div class="input-group">
                                                        <input id="ln-<?php echo $social->soc_name ?>" name="ln-<?php echo $social->soc_name ?>" type="text" class="form-control cc-cvc" value="<?php echo $social->soc_guid; ?>" data-val="true" >

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach ?>
                                            
                                        
                                        <hr>
                                        <button class="btn btn-outline-success" type="submit" name="accsumbit" value="useredit" style="float: right;">Simpan</button>
                                    </div>
                                </div>
                            <?php echo form_close(); ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="<?=base_url()?>application/libraries/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?=base_url()?>application/libraries/vendor/slick/slick.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/wow/wow.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/animsition/animsition.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/select2/select2.min.js"></script>
    <!-- CKEditor-->
    <script src="<?=base_url()?>application/libraries/ckeditor/ckeditor.js"></script>
    <!-- Main JS-->
    <script src="<?=base_url()?>application/libraries/js/main.js"></script>

    <script>
        setTimeout(function() {
            $('#alert').hide('fast');
        }, 5000);
    </script>

</body>

</html>
<!-- end document-->
