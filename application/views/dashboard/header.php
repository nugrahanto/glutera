<?php 
    if (isset($this->session->userdata['uLstat'])) :
        if ($this->session->userdata['uLstat'] != "true") :
            $this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">User Authentification Eror!</div>', 3);
            redirect(base_url('login'));
        else :
            if ($this->session->userdata['uStat'] == "active" || $this->session->userdata['uStat']== "root") {}
            else { redirect('404'); }
        endif;
    else :
        $this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">User Authentification Eror!</div>', 3);
        redirect( base_url('login') );
    endif;
?>

<header class="header-desktop">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="header-wrap" style="float: right !important;">
                <div class="header-button">
                   <!--  <div class="noti-wrap" style="margin: 0px !important">
                        <div class="noti__item js-item-menu" style="margin: 0px !important">
                            <div class="content">
                                <i class="zmdi zmdi-home" style="margin-right: 15px"></i>
                                <a class="js-acc-btn" href="<?php echo base_url() ?>" style="text-transform: capitalize; padding: 9px 0;
    color: black; "><b>visit site</b></a>
                            </div>
                        </div>
                    </div> -->
                    <div class="homevisit-wrap">
                        <div class="homevisit-item ">
                            <div class="content">
                                <i class="fas fa-home" style="margin-right: 10px"></i>
                                <a class="js-acc-btn" href="<?php echo base_url() ?>" style="text-transform: capitalize;"><b><i>Visit Site</i></b></a>
                            </div>
                        </div>
                    </div>
                    <div class="account-wrap">
                        <div class="account-item js-item-menu">
                            <div class="content" style="margin: 0px !important">
                                <i class="fas fa-user" style="margin-right: 10px"></i>
                                <a class="js-acc-btn" href="#" style="text-transform: capitalize;"><b><?php echo $this->session->userdata['uName']; ?></b></a>
                            </div>
                            <div class="account-dropdown js-dropdown">
                                <div class="account-dropdown__body">
                                    <div class="account-dropdown__item">
                                        <a href="<?php echo site_url('dashboard/admin/account/') ?>">
                                            <i class="zmdi zmdi-settings"></i>Account Setting</a>
                                    </div>
                                </div>
                                <div class="account-dropdown__footer">
                                    <a href="<?php echo site_url('user/logout/') ?>">
                                        <i class="zmdi zmdi-power"></i>Logout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>