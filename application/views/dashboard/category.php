
<?=$head?>

<body class="animsition">
    <div id="message">
        <?php echo $this->session->tempdata('alert_form'); ?>
    </div>
    <div class="page-wrapper">
        
        <!-- MENU OPENER -->
        <?=$menu?>
        <!-- END MENU -->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?=$header?>
            <!-- HEADER DESKTOP-->
            
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-4">
                            <!-- <form action="" method="post" novalidate="novalidate"> -->
                                <?php echo form_open('dashboard/admin/f_categoryadd');  ?>
                                <div class="card">
                                    <div class="card-header"><b>Tambah Kategori Baru</b></div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label class="control-label mb-1">Nama</label>
                                            <input id="title" name="name" type="text" class="form-control" aria-required="true" aria-invalid="false" >
                                            <label style="font-size: 14px; padding: 5px; color: #a0a0a0;">
                                                <i>Nama yang akan ditampilkan pada web.</i>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-1">Slug</label>
                                            <input id="slug" name="slug" type="text" class="form-control" aria-required="true" aria-invalid="false" >
                                            <label style="font-size: 14px; padding: 5px; color: #a0a0a0;">
                                                <i>"Slug" merupakan URL yang mudah dipahami oleh user. terdiri dari kata atau kalimat dengan huruf kecil dan tanpa spasi (spasi akan digantikan dengan garis penghubung).</i>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-1">Deskripsi</label>
                                            <textarea id="description" name="description" class="form-control" aria-required="true" aria-invalid="false" ></textarea>
                                            <label style="font-size: 14px; padding: 5px; color: #a0a0a0;">
                                                <i>Deskripsi dari kategori yang akan dibuat, deskripsi tidak akan ditampilkan pada halaman web dan digunakan hanya untuk memudahkan user dalam memahami jenis kategori yang dibuat.</i>
                                            </label>
                                        </div>
                                        <hr>
                                        <button class="btn btn-outline-success" type="submit" name="postpublish" style="float: right;">Simpan</button>
                                    </div>
                                </div>
                            <!-- </form> -->
                            <?php echo form_close(); ?>
                            </div>
                            <div class="col-lg-8">
                                <div class="card">
                                    <div class="card-header"><b>List Kategori</b></div>
                                    <div class="card-body">
                                        <div class="table-responsive table-data">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td>nama</td>
                                                    <td>deskripsi</td>
                                                    <td style="text-align: center;">jumlah</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach($category as $cat) : ?>
                                                <tr>
                                                    <td>
                                                        <div class="table-data__info">
                                                            <h6><?php echo $cat->ctr_name; ?></h6>
                                                            <span>
                                                                <a href="<?php echo site_url('dashboard/admin/category/edit/'.$cat->ctr_slug) ?>">edit</a> | <button type="button" class="" data-toggle="modal" data-target="<?php echo '#del-'.$cat->ctr_slug ?>" style="font-size: 12px; color: #999;">delete</button>
                                                            </span>
                                                            <!-- delete modal -->
                                                            <div class="modal fade" id="<?php echo 'del-'.$cat->ctr_slug ?>" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog modal-sm" role="document">
                                                                    <div class="modal-content" style="z-index: 10000 !important;">
                                                                        <?php echo form_open('dashboard/admin/f_categorydelete'); ?>
                                                                            <input type="hidden" name="f_cdel" value="<?php echo $cat->ctr_slug; ?>">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="smallmodalLabel">Delete</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <p>
                                                                                Yakin ingin menghapus kategori <b><?php echo $cat->ctr_name; ?></b>
                                                                            </p>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                            <button type="submit" class="btn btn-primary">Confirm</button>
                                                                        </div>
                                                                        <?php echo form_close(); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- end delete modal -->
                                                        </div>
                                                    </td>
                                                    <td width="50%">
                                                        <span><?php echo $cat->ctr_desc; ?></span>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <span><i><?php echo $cat->jumlah; ?></i></span>
                                                    </td>
                                                </tr>

                                                
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="<?=base_url()?>application/libraries/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?=base_url()?>application/libraries/vendor/slick/slick.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/wow/wow.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/animsition/animsition.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/select2/select2.min.js"></script>
    <!-- CKEditor-->
    <script src="<?=base_url()?>application/libraries/ckeditor/ckeditor.js"></script>
    <!-- Main JS-->
    <script src="<?=base_url()?>application/libraries/js/main.js"></script>

    <script>
        setTimeout(function() {
            $('#alert').hide('fast');
        }, 5000);
    </script>

</body>

</html>
<!-- end document-->
