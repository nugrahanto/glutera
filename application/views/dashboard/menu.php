

        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="<?php echo site_url('dashboard/admin/') ?>">
                            <b><i>DASHBOARD</i></b>
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="<?php echo site_url('dashboard/admin/') ?>">Home</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-copy"></i>Artikel</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="<?php echo site_url('dashboard/admin/allposts/') ?>">Semua Artikel</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('dashboard/admin/addpost/') ?>">Tambah Artikel</a>
                                </li>
                            </ul>
                        </li>
                    <?php if ($this->session->userdata['uStat']== "root") : ?>
                        <li>
                            <a href="<?php echo site_url('dashboard/admin/category/') ?>">
                                <i class="fas fa-archive"></i>Kategori</a>
                        </li>
                    <?php endif ?>
                        <li>
                            <a href="<?php echo site_url('dashboard/admin/video/') ?>">
                                <i class="fas fa-video-camera"></i>Video</a>
                        </li>
                    <?php if ($this->session->userdata['uStat']== "root") : ?>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-user"></i>User</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="<?php echo site_url('dashboard/admin/users/') ?>">Pengaturan User</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('dashboard/admin/adduser') ?>">Tambah User</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo site_url('dashboard/admin/socials/') ?>">
                                <i class="fas fa-rss"></i>Social Media</a>
                        </li>
                    <?php endif; ?>
                        <li>
                            <a href="<?php echo site_url('dashboard/admin/account/') ?>">
                                <i class="fas fa-cog"></i>Pengaturan Akun</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <b><i>DASHBOARD</i></b>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="active has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="<?php echo site_url('dashboard/admin/') ?>">Home</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-copy"></i>Artikel</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="<?php echo site_url('dashboard/admin/allposts/') ?>">Semua Artikel</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('dashboard/admin/addpost/') ?>">Tambah Artikel</a>
                                </li>
                            </ul>
                        </li>
                    <?php if ($this->session->userdata['uStat']== "root") : ?>
                        <li>
                            <a href="<?php echo site_url('dashboard/admin/category/') ?>">
                                <i class="fas  fa-archive"></i>Kategori</a>
                        </li>
                    <?php endif ?>
                        <li>
                            <a href="<?php echo site_url('dashboard/admin/video/') ?>">
                                <i class="fas fa-video-camera"></i>Video</a>
                        </li>
                    <?php if ($this->session->userdata['uStat']== "root") : ?>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-user"></i>User</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="<?php echo site_url('dashboard/admin/users/') ?>">Pengaturan User</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('dashboard/admin/adduser') ?>">Tambah User</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo site_url('dashboard/admin/socials/') ?>">
                                <i class="fas fa-rss"></i>Social Media</a>
                        </li>
                    <?php endif ?>
                        <li>
                            <a href="<?php echo site_url('dashboard/admin/account/') ?>">
                                <i class="fas fa-cog"></i>Pengaturan Akun</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
