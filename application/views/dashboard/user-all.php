
<?=$head?>

<body class="animsition">
    <div id="message">
        <?php echo $this->session->tempdata('alert_form'); ?>
    </div>
    <div class="page-wrapper">
        
        <!-- MENU OPENER -->
        <?=$menu?>
        <!-- END MENU -->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?=$header?>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- DATA TABLE -->
                                <h3 class="title-5 m-b-35">Pengaturan Users</h3>
                                <div class="table-data__tool">
                                    <div class="table-data__tool-left">
                                    <?php echo form_open('dashboard/admin/users/', 'method="get"');  ?>
                                        <div class="rs-select2--light rs-select2--sm">
                                            <select class="js-select2" name="filter">
                                                <option value="all" <?php if ($filter == 'all') echo "selected"; ?> >All</option>
                                                <option value="root" <?php if ($filter == 'root') echo "selected"; ?>>Admin</option>
                                                <option value="active" <?php if ($filter == 'active') echo "selected"; ?>>Author</option>
                                                <option value="deactive" <?php if ($filter == 'deactive') echo "selected"; ?>>Deactive</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                        <button class="au-btn-filter">
                                            <i class="zmdi zmdi-search"></i>filters</button>
                                    <?php echo form_close(); ?>
                                    </div>
                                    <div class="table-data__tool-right">
                                        <a class="au-btn au-btn-icon au-btn--green au-btn--small" style="color: white" href="<?php echo site_url('dashboard/admin/adduser/') ?>">
                                            <i class="zmdi zmdi-plus"></i>add user</a>
                                    </div>
                                </div>
                                <div class="user-data m-b-30">
                                    <div class="table-responsive table-data">
                                        <table id="tab-start" class="table">
                                            <thead>
                                                <tr>
                                                    <td width="5%"></td>
                                                    <td width="20%">Username</td>
                                                    <td width="25%">Nama</td>
                                                    <td width="5%">Status</td>
                                                    <td style="text-align: center;">Role</td>
                                                    <td width="3%" style="text-align: center;">Action</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i = 0; foreach($users as $user) : $i++; ?>
                                                    <?php if ($count != 0): ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $i . '.'; ?>
                                                    </td>
                                                    <td>
                                                        <div class="table-data__info">
                                                            <h6><?php echo $user->usr_nick; ?></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data__info">
                                                            <h6><?php echo $user->usr_name; ?></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <label class="switch switch-text switch-success switch-pill">
                                                            <input id="stat-toggle" type="checkbox" class="switch-input" value="<?php echo $user->usr_stat ?>" <?php if ($user->usr_stat == 'active' || $user->usr_stat == 'root') echo "checked"; ?> data-user="<?php echo $user->usr_nick ?>" onclick="status(this)" />
                                                            <span data-on="On" data-off="Off" class="switch-label"></span>
                                                            <span class="switch-handle"></span>
                                                        </label>
                                                    </td>
                                                    <td class="table-data__info" style="text-align: center;">
                                                        <div class="rs-select2--dark rs-select2--sm rs-select2--border">
                                                            <select id="role" class="js-select2 au-select-dark" name="role" data-user="<?php echo $user->usr_nick ?>" <?php if ($user->usr_stat == 'deactive') echo "disabled"; ?> onchange="role(this)">
                                                                <option value="deactive" <?php if ($user->usr_stat == 'deactive') echo "selected"; ?> >Deactive</option>
                                                                <option value="active" <?php if ($user->usr_stat == 'active') echo "selected"; ?> >Author</option>
                                                                <option value="root" <?php if ($user->usr_stat == 'root') echo "selected"; ?> >Admin</option>
                                                            </select>
                                                            <div class="dropDownSelect2"></div>
                                                        </div>
                                                    </td>
                                                    <td class="table-data__info" style="text-align: center;">
                                                        <button type="button" class="" data-toggle="modal" data-target="<?php echo '#del-'.$user->usr_nick ?>" ><i class="zmdi zmdi-delete"></i></button>

                                                        <!-- delete modal -->
                                                            <div class="modal fade" id="<?php echo 'del-'.$user->usr_nick ?>" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog modal-sm" role="document">
                                                                    <div class="modal-content" style="z-index: 10000 !important;">
                                                                        <?php echo form_open('dashboard/admin/f_userdelete'); ?>
                                                                            <input type="hidden" name="user" value="<?php echo $user->usr_nick; ?>">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="smallmodalLabel">Delete</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <p>
                                                                                Yakin ingin menghapus user <b><?php echo $user->usr_name ?></b>
                                                                            </p>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                            <button type="submit" class="btn btn-primary">Confirm</button>
                                                                        </div>
                                                                        <?php echo form_close(); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- end delete modal -->
                                                    </td>
                                                </tr>
                                                    <?php else : ?>
                                                <tr>
                                                    <td colspan="5" style="text-align: center !important; opacity: 0.6;"><div class="table-data__info"><i>Data belum ada</i></div></td>
                                                </tr>
                                                    <?php endif ?>
                                                <?php endforeach; ?>                                              
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END DATA TABLE -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT -->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="<?=base_url()?>application/libraries/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?=base_url()?>application/libraries/vendor/slick/slick.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/wow/wow.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/animsition/animsition.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/select2/select2.min.js"></script>
    <!-- CKEditor-->
    <script src="<?=base_url()?>application/libraries/ckeditor/ckeditor.js"></script>
    <!-- Main JS-->
    <script src="<?=base_url()?>application/libraries/js/main.js"></script>

    <script>
        setTimeout(function() {
            $('#alert').hide('fast');
        }, 5000);
    </script>

    <script type="text/javascript">
        function status($this) {
            var user    = $this.dataset.user;
            var status  = $this.value;

            if (status == 'deactive') {
                status = 'active';
            } else {
                status = 'deactive';
            }

            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('dashboard/admin/f_userupdate/')?>",
                dataType : "JSON",
                data : {user:user, status:status},
                success: function(data){
                    var html = '';
                    if (data == 'true') {
                        $( "#tab-start" ).load(window.location.href + " #tab-start" );
                    } else if (data == 404) {
                        $( "#tab-start" ).load(window.location.href + " #tab-start" );
                        html += '<div id="alert" class="alert alert-danger" role="alert">Gagal mengubah data!<br>Error: Access Function Failed!</div>';
                        $('#message').html(html);
                        setTimeout(function() { $('#message').html(''); }, 3000);
                    } else {
                        $( "#tab-start" ).load(window.location.href + " #tab-start" );
                        html += '<div id="alert" class="alert alert-danger" role="alert">Gagal mengubah data!<br>Error Code: '+data+'!</div>';
                        $('#message').html(html);
                        setTimeout(function() { $('#message').html(''); }, 3000);
                    }
                }
            });
            return false;
        }

        function role($this) {
            var user    = $this.dataset.user;
            var status  = $this.options[$this.selectedIndex].value

            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('dashboard/admin/f_userupdate/')?>",
                dataType : "JSON",
                data : {user:user, status:status},
                success: function(data){
                    var html = '';
                    if (data == 'true') {
                        $( "#tab-start" ).load(window.location.href + " #tab-start" );
                        html += '<div id="alert" class="alert alert-success" role="alert">Role User berhasil dirubah!</div>';
                        $('#message').html(html);
                        setTimeout(function() { $('#message').html(''); }, 3000);
                    } else if (data == 404) {
                        $( "#tab-start" ).load(window.location.href + " #tab-start" );
                        html += '<div id="alert" class="alert alert-danger" role="alert">Gagal mengubah data!<br>Error: Access Function Failed!</div>';
                        $('#message').html(html);
                        setTimeout(function() { $('#message').html(''); }, 3000);
                    } else {
                        $( "#tab-start" ).load(window.location.href + " #tab-start" );
                        html += '<div id="alert" class="alert alert-danger" role="alert">Gagal mengubah data!<br>Error Code: '+data+'!</div>';
                        $('#message').html(html);
                        setTimeout(function() { $('#message').html(''); }, 3000);
                    }
                }
            });
        }
    </script>

</body>

</html>
<!-- end document-->
