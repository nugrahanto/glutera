
<?=$head?>

<body class="animsition">
    <div id="message">
        <?php echo $this->session->tempdata('alert_form'); ?>
    </div>
    <div class="page-wrapper">
        
        <!-- MENU OPENER -->
        <?=$menu?>
        <!-- END MENU -->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?=$header?>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- DATA TABLE -->
                                <h3 class="title-5 m-b-35">All Posts</h3>
                                <div class="table-data__tool">
                                    <div class="table-data__tool-left">
                                    <?php echo form_open('dashboard/admin/allposts/', 'method="get"');  ?>
                                        <div class="rs-select2--light rs-select2--md">
                                            <select class="js-select2" name="cat">
                                                    <option value="" <?php if ($filterone == '') echo "selected"; ?> >All</option>
                                                <?php foreach ($categories as $category): ?>
                                                    <option value="<?php echo $category->ctr_id; ?>" <?php if ($filterone == $category->ctr_id) echo "selected"; ?> ><?php echo $category->ctr_name; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                        <div class="rs-select2--light rs-select2--sm">
                                            <select class="js-select2" name="st">
                                                <option value="" <?php if ($filterone == '') echo "selected"; ?> >All</option>
                                                <option value="publish" <?php if ($filterone == 'publish') echo "selected"; ?> >Publish</option>
                                                <option value="draft" <?php if ($filterone == 'draft') echo "selected"; ?> >Draft</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                        <button class="au-btn-filter">
                                            <i class="zmdi zmdi-search"></i>filters</button>

                                    <?php echo form_close(); ?>
                                    </div>
                                    <div class="table-data__tool-right">
                                        <a class="au-btn au-btn-icon au-btn--green au-btn--small" style="color: white" href="<?php echo site_url('dashboard/admin/addpost/') ?>">
                                            <i class="zmdi zmdi-plus"></i>add item</a>
                                    </div>
                                </div>
                                <div class="user-data m-b-30">
                                    <div class="table-responsive table-data">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td></td>
                                                    <td>Judul</td>
                                                    <td>Status</td>
                                                    <td>Author</td>
                                                    <td>Categories</td>
                                                    <td>Date</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($posts as $post) : ?>
                                                    <?php if ($post['count'] != 0): ?>
                                                <tr>
                                                    <td>
                                                        <label class="au-checkbox">
                                                            <input type="checkbox">
                                                            <span class="au-checkmark"></span>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <div class="table-data__info">
                                                            <h6><?php echo $post['pst_head']; ?></h6>
                                                            <span>
                                                                <a href="<?php echo site_url('dashboard/admin/editpost/'.$post['pst_slug']) ?>">edit</a> | <button type="button" class="" data-toggle="modal" data-target="<?php echo '#del-'.$post['pst_slug'] ?>" style="font-size: 12px; color: #999;">delete</button>
                                                            </span>
                                                            <!-- delete modal -->
                                                            <div class="modal fade" id="<?php echo 'del-'.$post['pst_slug'] ?>" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog modal-sm" role="document">
                                                                    <div class="modal-content" style="z-index: 10000 !important;">
                                                                        <?php echo form_open('dashboard/admin/f_postdelete'); ?>
                                                                            <input type="hidden" name="f_pdel" value="<?php echo $post['pst_slug']; ?>">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="smallmodalLabel">Delete</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <p>
                                                                                Yakin ingin menghapus artikel <b><?php echo $post['pst_head']; ?></b>
                                                                            </p>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                            <button type="submit" class="btn btn-primary">Confirm</button>
                                                                        </div>
                                                                        <?php echo form_close(); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- end delete modal -->
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data__info">
                                                            <h6><?php echo $post['pst_status']; ?></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data__info">
                                                            <h6><?php echo $post['created_by']; ?></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data__info">
                                                            <h6><?php echo $post['category']; ?></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data__info">
                                                            <h6><?php echo date('d/m/Y', strtotime($post['created_date'])); ?></h6>
                                                        </div>
                                                    </td>
                                                </tr>
                                                    <?php else : ?>
                                                <tr>
                                                    <td colspan="5" style="text-align: center !important; opacity: 0.6;"><div class="table-data__info"><i>Data belum ada</i></div></td>
                                                </tr>
                                                    <?php endif ?>
                                                <?php endforeach; ?>                                              
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END DATA TABLE -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT -->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="<?=base_url()?>application/libraries/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?=base_url()?>application/libraries/vendor/slick/slick.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/wow/wow.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/animsition/animsition.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/select2/select2.min.js"></script>
    <!-- CKEditor-->
    <script src="<?=base_url()?>application/libraries/ckeditor/ckeditor.js"></script>
    <!-- Main JS-->
    <script src="<?=base_url()?>application/libraries/js/main.js"></script>

    <script>
        setTimeout(function() {
            $('#alert').hide('fast');
        }, 5000);
    </script>

</body>

</html>
<!-- end document-->
