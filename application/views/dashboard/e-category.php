
<?=$head?>

<body class="animsition">
    <div id="message">
        <?php echo $this->session->tempdata('alert_form'); ?>
    </div>
    <div class="page-wrapper">
        
        <!-- MENU OPENER -->
        <?=$menu?>
        <!-- END MENU -->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?=$header?>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <?php foreach($category as $cat) : ?>
                        <?php echo form_open('dashboard/admin/f_categoryedit/'.$cat->ctr_slug);  ?>
                        <div class="row">
                            <div class="col-lg-8">
                            <!-- <form action="" method="post" novalidate="novalidate"> -->
                                <div class="card">
                                    <div class="card-header"><b>Edit Kategori</b></div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <input type="hidden" name="id" value="<?php echo $cat->ctr_id;?>">
                                            <label class="control-label mb-1">Nama</label>
                                            <input id="title" name="name" type="text" class="form-control" value="<?php echo $cat->ctr_name;?>" aria-required="true" aria-invalid="false" >
                                            <label style="font-size: 14px; padding: 5px; color: #a0a0a0;">
                                                <i>Nama yang akan ditampilkan pada web.</i>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-1">Slug</label>
                                            <input id="slug" name="slug" type="text" class="form-control" value="<?php echo $cat->ctr_slug;?>" aria-required="true" aria-invalid="false" >
                                            <label style="font-size: 14px; padding: 5px; color: #a0a0a0;">
                                                <i>"Slug" merupakan URL yang mudah dipahami oleh user. terdiri dari kata atau kalimat dengan huruf kecil dan tanpa spasi (spasi akan digantikan dengan garis penghubung).</i>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-1">Deskripsi</label>
                                            <textarea id="description" name="description" class="form-control" aria-required="true" aria-invalid="false" ><?php echo $cat->ctr_desc;?></textarea>
                                            <label style="font-size: 14px; padding: 5px; color: #a0a0a0;">
                                                <i>Deskripsi dari kategori yang akan dibuat, deskripsi tidak akan ditampilkan pada halaman web dan digunakan hanya untuk memudahkan user dalam memahami jenis kategori yang dibuat.</i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card">
                                    <div class="card-header"><b>Terbitkan Kategori</b></div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <button class="btn btn-outline-success" type="submit" name="postpublish">Publish</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>

                        <!-- </form> -->
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="<?=base_url()?>application/libraries/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?=base_url()?>application/libraries/vendor/slick/slick.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/wow/wow.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/animsition/animsition.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/select2/select2.min.js"></script>
    <!-- CKEditor-->
    <script src="<?=base_url()?>application/libraries/ckeditor/ckeditor.js"></script>
    <!-- Main JS-->
    <script src="<?=base_url()?>application/libraries/js/main.js"></script>

    <script>
        setTimeout(function() {
            $('#alert').hide('fast');
        }, 5000);
    </script>

</body>

</html>
<!-- end document-->
