
<?=$head?>

<body class="animsition">
    <div id="message">
        <?php echo $this->session->tempdata('alert_form'); ?>
    </div>
    <div class="page-wrapper">
        
        <!-- MENU OPENER -->
        <?=$menu?>
        <!-- END MENU -->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?=$header?>
            <!-- HEADER DESKTOP-->
            
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <?php if (!empty($user->usr_nick)): ?>

                            <div class="col-lg-6">
                                <?php echo form_open('user/update');  ?>
                                <div class="card">
                                    <div class="card-header"><b>Ubah User Profile</b></div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label class="control-label mb-1">Nama User</label>
                                            <input type="hidden" name="own" value="<?php echo md5($user->usr_nick) ?>">
                                            <input id="title" name="name" type="text" class="form-control" aria-required="true" aria-invalid="false" value="<?php echo $user->usr_name ?>" >
                                            <label style="font-size: 14px; padding: 5px; color: #a0a0a0;">
                                                <i>Nama lengkap pengguna.</i>
                                            </label>
                                        </div>
                                        <hr>
                                        <button class="btn btn-outline-success" type="submit" name="accsumbit" value="useredit" style="float: right;">Ubah</button>
                                    </div>
                                </div>
                            <?php echo form_close(); ?>
                            </div>
                            <div class="col-lg-6">
                                <?php echo form_open('user/update');  ?>
                                <div class="card">
                                    <div class="card-header"><b>Ubah Password</b></div>
                                    <div class="card-body">
                                        <input type="hidden" name="own" value="<?php echo md5($user->usr_nick) ?>">
                                        <div class="form-group">
                                            <label class="control-label mb-1">Password Lama</label>
                                            <input id="title" name="oldpass" type="password" class="form-control" aria-required="true" aria-invalid="false" >
                                            <label style="font-size: 14px; padding: 5px; color: #a0a0a0;">
                                                <i>Masukkan password lama anda untuk verifikasi.</i>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-1">Password Baru</label>
                                            <input id="slug" name="newpass" type="password" class="form-control" aria-required="true" aria-invalid="false" >
                                            <label style="font-size: 14px; padding: 5px; color: #a0a0a0;">
                                                <i>Masukkan password baru yang akan digunakan seterusnya.</i>
                                            </label>
                                        </div>
                                        <hr>
                                        <button class="btn btn-outline-success" type="submit"  name="accsumbit" value="passedit" style="float: right;">Ubah</button>
                                    </div>
                                </div>
                            <?php echo form_close(); ?>
                            </div>
                            <?php endif ?>

                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="<?=base_url()?>application/libraries/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?=base_url()?>application/libraries/vendor/slick/slick.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/wow/wow.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/animsition/animsition.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/select2/select2.min.js"></script>
    <!-- CKEditor-->
    <script src="<?=base_url()?>application/libraries/ckeditor/ckeditor.js"></script>
    <!-- Main JS-->
    <script src="<?=base_url()?>application/libraries/js/main.js"></script>

    <script>
        setTimeout(function() {
            $('#alert').hide('fast');
        }, 5000);
    </script>

</body>

</html>
<!-- end document-->
