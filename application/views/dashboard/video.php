
<?=$head?>

<body class="animsition">
    <div id="message">
        <?php echo $this->session->tempdata('alert_form'); ?>
    </div>
    <div class="page-wrapper">
        
        <!-- MENU OPENER -->
        <?=$menu?>
        <!-- END MENU -->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?=$header?>
            <!-- HEADER DESKTOP-->
            
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-5">
                                <?php echo form_open_multipart('dashboard/admin/f_videoadd');  ?>
                                <div class="card">
                                    <div class="card-header"><b>Tambah Post Video</b></div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label class="control-label mb-1">Judul</label>
                                            <input id="title" name="name" type="text" class="form-control" aria-required="true" aria-invalid="false" >
                                            <label style="font-size: 14px; padding: 5px; color: #a0a0a0;">
                                                <i>Judul postingan yang akan ditampilkan pada web.</i>
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-1">Video</label><br>
                                            <label style="font-size: 14px; padding: 5px; color: #a0a0a0;">
                                                <i>Hanya gunakan salah satu fitur dibawah, "Upload Video" akan lebih diprioritaskan jika user menggunakan kedua fitur.</i>
                                            </label>
                                            <label class="control-label mb-1"><i>Link Embeded Video</i></label>
                                            <input id="otherlink" name="otherlink" type="text" class="form-control" aria-required="true" aria-invalid="false">
                                            <label style="font-size: 14px; padding: 5px; color: #a0a0a0;">
                                                <i>Link URL atau tautan dari web lain. <br>ex. youtube, facebook, dll.</i>
                                            </label>
                                            <br><label>OR</label><br>
                                            <label class="control-label mb-1"><i>Upload Video</i></label>
                                            <div class="row">
                                                <div class="col-12">
                                                    <input type="file" id="video" name="video" value="" accept="video/*" hidden>
                                                </div>
                                            </div>

                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <div class="btn-group">
                                                        <button type="button" id='bt-video'class="btn btn-primary">Browse..</button>
                                                    </div>
                                                </div>
                                                <input type="text" id="in-video" name="in-video" class="form-control" value="" readonly>
                                                <div id="dvideo" class="col-12 mt-3" style="display: none;">
                                                    <button type="button" id="bt-dvideo" class="btn btn-outline-danger btn-sm float-right">Delete</button>
                                                </div>
                                            </div>
                                            <label style="font-size: 14px; padding: 5px; color: #a0a0a0;">
                                                <i>Video upload akan tersimpan secara local. Menggunakan video upload membutuhkan koneksi yang stabil dan dalam prosesnya terkadang membutuhkan waktu yang relatif lama tergantung besaran video yang di upload</i>
                                            </label><br>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label mb-1"><i>Video Thumbnail</i></label>
                                            <div class="row">
                                                <div class="col-12">
                                                    <input type="file" id="thumb" name="thumb" value="" accept="image/*" hidden>
                                                </div>
                                            </div>

                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <div class="btn-group">
                                                        <button type="button" id='bt-thumb'class="btn btn-primary">Browse..</button>
                                                    </div>
                                                </div>
                                                <input type="text" id="in-thumb" class="form-control" value="" readonly>
                                                <div id="dthumb" class="col-12 mt-3" style="display: none;">
                                                    <button type="button" id="bt-dthumb" class="btn btn-outline-danger btn-sm float-right">Delete</button>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <button class="btn btn-outline-success" type="submit" name="postpublish" style="float: right;">Simpan</button>
                                    </div>
                                </div>
                            <!-- </form> -->
                            <?php echo form_close(); ?>
                            </div>
                            <div class="col-lg-7">
                                <div class="card">
                                    <div class="card-header"><b>List Video</b></div>
                                    <div class="card-body">
                                        <div class="table-responsive table-data">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td>nama</td>
                                                    <td>author</td>
                                                    <td style="text-align: center;">tanggal</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach($video as $vdo) : ?>
                                                <tr>
                                                    <td>
                                                        <div class="table-data__info">
                                                            <h6><?php echo $vdo->vdo_head; ?></h6>
                                                            <span>
                                                                <!-- <a href="<?php echo site_url('dashboard/admin/video/edit/'.$vdo->vdo_slug) ?>">detail</a> -->
                                                                <button type="button" class="" data-toggle="modal" data-target="<?php echo '#det-'.$vdo->vdo_slug ?>" style="font-size: 12px; color: #999;">detail</button> | <button type="button" class="" data-toggle="modal" data-target="<?php echo '#del-'.$vdo->vdo_slug ?>" style="font-size: 12px; color: #999;">delete</button>
                                                            </span>
                                                            <!-- delete modal -->
                                                            <div class="modal fade" id="<?php echo 'del-'.$vdo->vdo_slug ?>" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog modal-sm" role="document">
                                                                    <div class="modal-content" style="z-index: 10000 !important;">
                                                                        <?php echo form_open('dashboard/admin/f_videodelete'); ?>
                                                                            <input type="hidden" name="f_vdel" value="<?php echo $vdo->vdo_slug; ?>">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="smallmodalLabel">Delete</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <p>
                                                                                Yakin ingin menghapus video <b><?php echo $vdo->vdo_head; ?></b>
                                                                            </p>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                            <button type="submit" class="btn btn-primary">Confirm</button>
                                                                        </div>
                                                                        <?php echo form_close(); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- end delete modal -->
                                                            <!-- detail modal -->
                                                            <div class="modal fade" id="<?php echo 'det-'.$vdo->vdo_slug ?>" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg" role="document">
                                                                    <div class="modal-content" style="z-index: 10000 !important;">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="smallmodalLabel">Detail</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <?php if ($vdo->vdo_videoname != "nofile"): ?>
                                                                                <div style="width: 100%; padding: 10px 30px;">
                                                                                    <h4><i><?php echo $vdo->vdo_head; ?></i></h4><hr>
                                                                                    <video controls playsinline uk-video="false" src="<?php echo $vdo->vdo_link ?>" width="100%" class="mt-4" />
                                                                                </div>
                                                                            <?php else : ?>
                                                                                <p style="text-align: center;">Video dari web lain tidak dapat langsung ditampilkan pada dashboard. <br><a href="<?php echo $vdo->vdo_guid ?>">Klik disini</a> untuk langsung melihat hasil.</p>
                                                                            <?php endif ?>
                                                                        </div>
                                                                        <div class="modal-footer">

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- end detail modal -->
                                                        </div>
                                                    </td>
                                                    <td class="table-data__info" width="25%">
                                                        <h6><?php echo $vdo->created_by; ?></h6>
                                                    </td>
                                                    <td class="table-data__info" width="30%" style="text-align: center;">
                                                        <h6><i><?php echo date('d F Y', strtotime($vdo->created_date)); ?></i></h6>
                                                    </td>
                                                </tr>

                                                
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="<?=base_url()?>application/libraries/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?=base_url()?>application/libraries/vendor/slick/slick.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/wow/wow.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/animsition/animsition.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/select2/select2.min.js"></script>
    <!-- CKEditor-->
    <script src="<?=base_url()?>application/libraries/ckeditor/ckeditor.js"></script>
    <!-- Main JS-->
    <script src="<?=base_url()?>application/libraries/js/main.js"></script>
    <!-- UIkit JS -->
    <script src="<?php echo base_url();?>assets/js/uikit.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/uikit-icons.min.js"></script>

    <script>
        setTimeout(function() {
            $('#alert').hide('fast');
        }, 5000);
    </script>

    <script>
        window.onload = function() {
            document.getElementById('bt-video').addEventListener('click', openDialog);
            function openDialog() {
                document.getElementById('video').click();
            }

            document.getElementById('bt-thumb').addEventListener('click', openDialogImg);
            function openDialogImg() {
                document.getElementById('thumb').click();
            }
        };

        function videoPreview(input) {
            if (input.files.length != 0) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#in-video').val(input.files[0].name);
                    $('#in-video + video').remove();
                    $('#in-video').after('<video controls playsinline uk-video="false" src="'+e.target.result+'" width="100%" class="mt-4" />');
                    $('#dvideo').show();
                };
                reader.readAsDataURL(input.files[0]);
            } else {
                $('#in-video').val("");
                $('#in-video + video').remove();
                $('#dvideo').hide();
            }
        }

        function _video_del(input) {
            $('#video').val("");
            $('#in-video').val("");
            $('#in-video + video').remove();
            $('#dvideo').hide();
        }

        function filePreview(input) {
            if (input.files.length != 0) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#in-thumb').val(input.files[0].name);
                    $('#in-thumb + img').remove();
                    $('#in-thumb').after('<img src="'+e.target.result+'" width="100%" height="auto" class="mt-4" />');
                    $('#dthumb').show();
                };
                reader.readAsDataURL(input.files[0]);
            } else {
                $('#in-thumb').val("");
                $('#in-thumb + img').remove();
                $('#dthumb').hide();
            }
        }

        function _file_del(input) {
            $('#thumb').val("");
            $('#in-thumb').val("");
            $('#in-thumb + img').remove();
            $('#dthumb').hide();
        }

        $("#video").change(function () {
            videoPreview(this);
        });

        $("#bt-dvideo").click(function () {
            _video_del(this);
        });

        $("#thumb").change(function () {
            filePreview(this);
        });

        $("#bt-dthumb").click(function () {
            _file_del(this);
        });
    </script> 

</body>

</html>
<!-- end document-->
