
<?=$head?>

<body class="animsition">
    <div id="message">
        <?php echo $this->session->tempdata('alert_form'); ?>
    </div>
    <div class="page-wrapper">
        
        <!-- MENU OPENER -->
        <?=$menu?>
        <!-- END MENU -->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?=$header?>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <?php echo form_open_multipart('dashboard/admin/f_postadd/');  ?>
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-header"><b>Judul Artikel</b></div>
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <input id="title" name="title" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <textarea class="ckeditor" id="ckedtor" name="text"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-header"><b>Tags</b></div>
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <input id="tags" type="text" name="tags" class="form-control" value="" data-role="tagsinput" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="card form-group">
                                            <div class="card-header"><b>Source</b></div>
                                            <textarea class="ckeditor" id="ckedtor" name="source"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-header"><b>Terbitkan Artikel</b></div>
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <button class="btn btn-outline-danger" type="submit" name="submit" value="draft">Draft</button>
                                                    <button class="btn btn-outline-success" type="submit" name="submit" value="publish" style="float: right;">Publish</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-header"><b>Kategori Artikel</b></div>
                                            <div class="card-body">
                                                <div class="form-group">
                                                    
                                                    <div class="form-check">
                                                        <?php foreach($category as $cat) : ?>
                                                        <div class="checkbox">
                                                            <label class="form-check-label ">
                                                                <input type="checkbox" id="<?php echo 'box-'.$cat->ctr_id; ?>" name="category[]" value="<?php echo $cat->ctr_id; ?>" class="form-check-input"><?php echo $cat->ctr_name; ?>
                                                            </label>
                                                        </div>
                                                        <?php endforeach; ?>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-header"><b>Feature Image 1</b></div>
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <input type="file" id="file-one" name="file-one" value="" accept="image/*" hidden>
                                                        </div>
                                                    </div>

                                                    <div class="input-group">
                                                        <div class="input-group-btn">
                                                            <div class="btn-group">
                                                                <button type="button" id='bt-fone'class="btn btn-primary">Browse..</button>
                                                            </div>
                                                        </div>
                                                        <input type="text" id="in-fone" class="form-control" value="" readonly>
                                                        <div id="dfone" class="col-12 mt-3" style="display: none;">
                                                            <button type="button" id="bt-dfone" class="btn btn-outline-danger btn-sm float-right">Delete</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-header"><b>Feature Image 2</b></div>
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <input type="file" id="file-two" name="file-two" value="" accept="image/*" hidden>
                                                        </div>
                                                    </div>

                                                    <div class="input-group">
                                                        <div class="input-group-btn">
                                                            <div class="btn-group">
                                                                <button type="button" id='bt-ftwo'class="btn btn-primary">Browse..</button>
                                                            </div>
                                                        </div>
                                                        <input type="text" id="in-ftwo" class="form-control" value="" readonly>
                                                        <div id="dftwo" class="col-12 mt-3" style="display: none;">
                                                            <button type="button" id="bt-dftwo" class="btn btn-outline-danger btn-sm float-right">Delete</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-header"><b>Feature Image 3</b></div>
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <input type="file" id="file-three" name="file-three" value="" accept="image/*" hidden>
                                                        </div>
                                                    </div>

                                                    <div class="input-group">
                                                        <div class="input-group-btn">
                                                            <div class="btn-group">
                                                                <button type="button" id='bt-fthree'class="btn btn-primary">Browse..</button>
                                                            </div>
                                                        </div>
                                                        <input type="text" id="in-fthree" class="form-control" value="" readonly>
                                                        <div id="dfthree" class="col-12 mt-3" style="display: none;">
                                                            <button type="button" id="bt-dfthree" class="btn btn-outline-danger btn-sm float-right">Delete</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-header"><b>Feature Image 4</b></div>
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <input type="file" id="file-four" name="file-four" value="" accept="image/*" hidden>
                                                        </div>
                                                    </div>

                                                    <div class="input-group">
                                                        <div class="input-group-btn">
                                                            <div class="btn-group">
                                                                <button type="button" id='bt-ffour'class="btn btn-primary">Browse..</button>
                                                            </div>
                                                        </div>
                                                        <input type="text" id="in-ffour" class="form-control" value="" readonly>
                                                        <div id="dffour" class="col-12 mt-3" style="display: none;">
                                                            <button type="button" id="bt-dffour" class="btn btn-outline-danger btn-sm float-right">Delete</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-header"><b>Feature Image 5</b></div>
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <input type="file" id="file-five" name="file-five" value="" accept="image/*" hidden>
                                                        </div>
                                                    </div>

                                                    <div class="input-group">
                                                        <div class="input-group-btn">
                                                            <div class="btn-group">
                                                                <button type="button" id='bt-ffive'class="btn btn-primary">Browse..</button>
                                                            </div>
                                                        </div>
                                                        <input type="text" id="in-ffive" class="form-control" value="" readonly>
                                                        <div id="dffive" class="col-12 mt-3" style="display: none;">
                                                            <button type="button" id="bt-dffive" class="btn btn-outline-danger btn-sm float-right">Delete</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            <!-- END MAIN CONTENT -->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="<?=base_url()?>application/libraries/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?=base_url()?>application/libraries/vendor/slick/slick.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/wow/wow.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/animsition/animsition.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>>
    <script src="<?=base_url()?>application/libraries/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/select2/select2.min.js"></script>
    <!-- CKEditor-->
    <script src="<?=base_url()?>application/libraries/ckeditor/ckeditor.js"></script>
    <!-- Main JS-->
    <script src="<?=base_url()?>application/libraries/js/main.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('form input:not([type="submit"])').keydown(function(e){
                if (e.keyCode == 13) {
                    e.preventDefault();
                }
            });

            $('.bootstrap-tagsinput input').keydown(function(e){
                if (e.keyCode == 13) {
                    var key = $.Event('keypress');
                    key.which = 32;
                    $('.bootstrap-tagsinput input').trigger(key);
                    e.preventDefault();
                };
            });
        });
    </script>
    
    <script>
        setTimeout(function() {
            $('#alert').hide('fast');
        }, 5000);
    </script>

    <script>
        window.onload = function() {
            document.getElementById('bt-fone').addEventListener('click', openDialog1);
            function openDialog1() {
                document.getElementById('file-one').click();
            }

            document.getElementById('bt-ftwo').addEventListener('click', openDialog2);
            function openDialog2() {
                document.getElementById('file-two').click();
            }

            document.getElementById('bt-fthree').addEventListener('click', openDialog3);
            function openDialog3() {
                document.getElementById('file-three').click();
            }

            document.getElementById('bt-ffour').addEventListener('click', openDialog4);
            function openDialog4() {
                document.getElementById('file-four').click();
            }

            document.getElementById('bt-ffive').addEventListener('click', openDialog5);
            function openDialog5() {
                document.getElementById('file-five').click();
            }
        };
    </script> 

    <script type="text/javascript">
        function file1Preview(input) {
            if (input.files.length != 0) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#in-fone').val(input.files[0].name);
                    $('#in-fone + img').remove();
                    $('#in-fone').after('<img src="'+e.target.result+'" width="100%" height="auto" class="mt-3" />');
                    $('#dfone').show();
                };
                reader.readAsDataURL(input.files[0]);
            } else {
                $('#in-fone').val("");
                $('#in-fone + img').remove();
                $('#dfone').hide();
            }
        }

        function file2Preview(input) {
            if (input.files.length != 0) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#in-ftwo').val(input.files[0].name);
                    $('#in-ftwo + img').remove();
                    $('#in-ftwo').after('<img src="'+e.target.result+'" width="100%" height="auto" class="mt-3" />');
                    $('#dftwo').show();
                };
                reader.readAsDataURL(input.files[0]);
            } else {
                $('#in-ftwo').val("");
                $('#in-ftwo + img').remove();
                $('#dftwo').hide();
            }
        }

        function file3Preview(input) {
            if (input.files.length != 0) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#in-fthree').val(input.files[0].name);
                    $('#in-fthree + img').remove();
                    $('#in-fthree').after('<img src="'+e.target.result+'" width="100%" height="auto" class="mt-3" />');
                    $('#dfthree').show();
                };
                reader.readAsDataURL(input.files[0]);
            } else {
                $('#in-fthree').val("");
                $('#in-fthree + img').remove();
                $('#dfthree').hide();
            }
        }

        function file4Preview(input) {
            if (input.files.length != 0) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#in-ffour').val(input.files[0].name);
                    $('#in-ffour+ img').remove();
                    $('#in-ffour').after('<img src="'+e.target.result+'" width="100%" height="auto" class="mt-3" />');
                    $('#dffour').show();
                };
                reader.readAsDataURL(input.files[0]);
            } else {
                $('#in-ffour').val("");
                $('#in-ffour + img').remove();
                $('#dffour').hide();
            }
        }

        function file5Preview(input) {
            if (input.files.length != 0) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#in-ffive').val(input.files[0].name);
                    $('#in-ffive + img').remove();
                    $('#in-ffive').after('<img src="'+e.target.result+'" width="100%" height="auto" class="mt-3" />');
                    $('#dffive').show();
                };
                reader.readAsDataURL(input.files[0]);
            } else {
                $('#in-ffive').val("");
                $('#in-ffive + img').remove();
                $('#dffive').hide();
            }
        }

        function _file1_del(input) {
            $('#file-one').val("");
            $('#in-fone').val("");
            $('#in-fone + img').remove();
            $('#dfone').hide();
        }

        function _file2_del(input) {
            $('#file-two').val("");
            $('#in-ftwo').val("");
            $('#in-ftwo + img').remove();
            $('#dftwo').hide();
        }

        function _file3_del(input) {
            $('#file-three').val("");
            $('#in-fthree').val("");
            $('#in-fthree + img').remove();
            $('#dfthree').hide();
        }

        function _file4_del(input) {
            $('#file-four').val("");
            $('#in-ffour').val("");
            $('#in-ffour + img').remove();
            $('#dffour').hide();
        }

        function _file5_del(input) {
            $('#file-five').val("");
            $('#in-ffive').val("");
            $('#in-ffive + img').remove();
            $('#dffive').hide();
        }

        $("#file-one").change(function () {
            file1Preview(this);
        });

        $("#file-two").change(function () {
            file2Preview(this);
        });

        $("#file-three").change(function () {
            file3Preview(this);
        });

        $("#file-four").change(function () {
            file4Preview(this);
        });

        $("#file-five").change(function () {
            file5Preview(this);
        });

        $("#bt-dfone").click(function () {
            _file1_del(this);
        });

        $("#bt-dftwo").click(function () {
            _file2_del(this);
        });

        $("#bt-dfthree").click(function () {
            _file3_del(this);
        });

        $("#bt-dffour").click(function () {
            _file4_del(this);
        });

        $("#bt-dffive").click(function () {
            _file5_del(this);
        });
    </script>

</body>

</html>
<!-- end document-->
