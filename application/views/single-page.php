<!DOCTYPE html>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta id="myViewport" name="viewport" content="width=device-width, initial-scale=0.5">
    <title>Glutera - <?php echo ucwords($post->pst_head) ?></title>
    <meta name="description" content="<?php echo $metadesc ?>">
    <meta name="keywords" content="<?php echo $metakey ?>">
    <meta name="author" content="<?php echo ucwords($post->created_by) ?>">
    <!-- UIkit CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/uikit.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css" />
</head>

<body>
    <div id="menu-sidebar-right" uk-offcanvas="flip: true">
        <div class="uk-offcanvas-bar right">

            <button class="uk-offcanvas-close" type="button" uk-close></button>

            <div class="uk-margin">
                <a href="<?php echo base_url();?>" class="uk-logo">
                    <img src="<?php echo base_url();?>assets/img/white-logo.png" class="gt-logo-sidebar" alt="" uk-img>
                </a>
            </div>

            <ul class="uk-nav-default uk-nav-parent-icon gt-list-menu-sidebar-right" uk-nav>
                <?php if (!empty($sidebar[0])):
                    foreach ($sidebar as $menu) : ?>
                        <li>
                            <a href="<?php echo $menu->ctr_guid ?>" class="uk-text-uppercase">
                                <?php echo $menu->ctr_name ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                    <?php if (!empty($video->vdo_id)): ?>
                        <li><a href="<?php echo base_url() ?>videos/" class="uk-text-uppercase">All Videos</a></li>
                    <?php endif ?>
                <?php else : ?>
                    <li><a href="" class="uk-text-uppercase">Belum ada menu</a></li>
                <?php endif ?>
                <li><a href="<?php echo base_url() ?>gallery/" class="uk-text-uppercase">Be Everlasting</a></li>
            </ul>

        </div>
    </div>
    <section id="gt-article" class="uk-grid-collapse uk-child-width-expand@s" uk-grid>
        <div class="uk-width-4-5@m uk-width-4-5@s">
            <div class="uk-card uk-card-muted uk-card-body" uk-grid>
                <div class="uk-width-1-1">
                    <nav class="uk-navbar" uk-navbar>
                        <div class="uk-navbar-left">
                            <div class="uk-margin">
                                <a href="<?php echo base_url(); ?>" class="uk-logo">
                                    <img src="<?php echo base_url();?>assets/img/orange-logo.png" alt="" uk-img>
                                </a>
                            </div>
                        </div>
                        <div class="uk-navbar-right uk-hidden@s">
                            <div class="uk-margin uk-text-right">
                                <button class="uk-button uk-button-text gt-item-menu" uk-toggle="#menu-sidebar-right"><span class="uk-icon uk-margin-small-right" uk-icon="icon: menu"></span></button>
                                <button class="uk-button uk-button-text gt-item-search"><span class="uk-icon uk-margin-small-right" uk-icon="icon: search"></span></button>
                                <?php echo form_open('search', 'class="uk-search uk-search-default gt-custom-search" method="get'); ?>
                                    <button type="button" class="uk-search-icon-flip gt-color-icon gt-hide-search" uk-search-icon></button>
                                    <input class="uk-search-input uk-border-pill uk-text-small gt-header-search gt-background-search" type="search" placeholder="search" name="ar">
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </nav>
                </div>
                <div class="uk-grid-collapse uk-margin-small-bottom uk-flex-center@m uk-flex-left@s" uk-grid>
                    <div class="uk-width-3-5@m uk-width-4-5@s">
                        <h1 class="gt-single-title"><?php echo $post->pst_head ?></h1>
                        <ul class="gt-article-meta">
                            <li>Ditulis oleh <a href="#"><?php echo $post->created_by ?></a></li>
                        </ul>
                        <ul class="gt-article-meta">
                            <li><?php echo date('d F Y', strtotime($post->created_date)); ?></li>
                        </ul>
                        <ul class="gt-article-meta">
                            <li>Share:</li>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $post->pst_guid ?>">Facebook</a></li>
                            <li><a href="https://twitter.com/share?url=<?php echo $post->pst_guid ?>>">Twitter</a></li>
                            <li><a href="https://www.glutera.com/web/"><img src="<?php echo base_url();?>assets/img/small-logo.png" alt="" uk-img></a></li>
                            <!--<li><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url();?>assets/img/small-logo.png" alt="" uk-img></a></li>-->
                        </ul>
                        <?php echo $post->pst_txt ?>
                        <?php if (!empty($tags)): ?>
                            <span class="uk-text-small uk-text-muted">
                                <?php foreach ($tags as $tag): $tslug = slugify($tag); ?>
                                    <a href="<?php echo base_url().'tags/'.$tslug ?>"><i><?php echo $tag; ?></i></a>
                                <?php endforeach ?>
                            </span>
                        <?php endif ?>
                        <?php if (!empty($source->meta_value)): ?>
                            <ul class="gt-article-meta"><li>Source:</li></ul>
                            <span class="uk-text-small uk-text-muted"><?php echo $source->meta_value; ?></span>
                        <?php endif ?>
                        <ul class="gt-article-meta">
                            <li>Share:</li>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $post->pst_guid ?>">Facebook</a></li>
                            <li><a href="https://twitter.com/share?url=<?php echo $post->pst_guid ?>>">Twitter</a></li>
                             <li><a href="https://www.glutera.com/web/"><img src="<?php echo base_url();?>assets/img/small-logo.png" alt="" uk-img></a></li>
                            <!--<li><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url();?>assets/img/small-logo.png" alt="" uk-img></a></li>-->
                        </ul>
                    </div>
                    
                    <div class="uk-width-3-5@m uk-width-4-5@s uk-grid-collapse uk-margin-medium-top" uk-grid>
                        <div class="uk-width-1-1">
                            <h5 class="uk-text-uppercase gt-related-text">Related Topic</h5>
                        </div>
                        <?php foreach ($related as $rpost): ?>
                            <div class="uk-width-1-3@s uk-width-1-2 uk-padding-small uk-padding-remove-left gt-related-img">
                                <a href="<?php echo $rpost['pst_guid'] ?>">
                                    <img src="<?php echo $rpost['img'] ?>" alt="" class="uk-transition-scale-up uk-transition-opaque" >
                                </a>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-1-5@m uk-width-1-5@s">
            <div class="uk-background-default uk-padding gt-menu-sidebar-right uk-light" uk-height-viewport uk-sticky>
                <div class="uk-margin uk-text-right">
                    <button class="uk-button uk-button-text gt-item-menu" uk-toggle="#menu-sidebar-right"><span class="uk-icon uk-margin-small-right" uk-icon="icon: menu"></span></button>
                    <button class="uk-button uk-button-text gt-item-search"><span class="uk-icon uk-margin-small-right" uk-icon="icon: search"></span></button>
                    <?php echo form_open('search', 'class="uk-search uk-search-default gt-custom-search" method="get'); ?>
                        <button type="button" class="uk-search-icon-flip gt-color-icon gt-hide-search" uk-search-icon></button>
                        <input class="uk-search-input uk-border-pill uk-text-small gt-header-search gt-background-search" type="search" placeholder="search" name="ar">
                    <?php echo form_close(); ?>
                </div>

                <div class="gt-float-thumbnail">
                    <ul id="gt-nav-float" class="uk-subnav uk-subnav-pill gt-nav-float" uk-switcher="animation: uk-animation-scale-down; duration: 600;">
                        <?php $i = 0; foreach ($imeta as $img): $i += 1; ?>
                            <li id="<?php echo 'navFloat'.$i; ?>"><a href="#"></a></li>
                        <?php endforeach ?>
                    </ul>
                    <ul id="gt-img-float" class="uk-switcher uk-margin gt-img-float" uk-lightbox="animation: scale">
                        <?php foreach ($imeta as $img): ?>
                            <li>
                                <img class="uk-transition-scale-up uk-transition-opaque" src="<?php echo $img->img_guid ?>" alt="">
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.4.1.min.js"></script>
    <!-- UIkit JS -->
    <script src="<?php echo base_url();?>assets/js/uikit.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/uikit-icons.min.js"></script>
    <!-- Main JS -->
    <script>
    $(document).ready(function() {
        if(screen.width >= 750) {
            $('#myViewport').attr('content', 'width=device-width, initial-scale=0.5');
        } else {
            $('#myViewport').attr('content', 'width=device-width, initial-scale=0.47');
        }
        
        $(window).on('resize', (function() {
            if(screen.width >= 750) {
                $('#myViewport').attr('content', 'width=device-width, initial-scale=0.5');
            } else {
                $('#myViewport').attr('content', 'width=device-width, initial-scale=0.47');
            }
        }))

        $('.gt-item-search').on('click', (function(e) {
            e.preventDefault();

            $('.gt-custom-search').show();
            $('.gt-item-search').hide();
            $('.gt-menu-sidebar-right .gt-item-menu').css({
                'margin-right': '126px'
            })
        }))

        $('.gt-hide-search').on('click', (function(e) {
            e.preventDefault();

            $('.gt-custom-search').hide();
            $('.gt-item-search').show();
            $('.gt-menu-sidebar-right .gt-item-menu').css({
                'margin-right': '0'
            })
        }))

        var totalItem = ($('#gt-nav-float li').length - 1);

        setInterval(function() {
            var i = 0;
            var activeItem = 0;

            $('#gt-nav-float li').each(function(e) {
                if($(this).hasClass("uk-active")) {
                    if(i == totalItem) {
                        activeItem = 0;
                    } else {
                        activeItem = (i + 1);
                    }
                }

                i++;
            })

            $('#gt-nav-float li a')[activeItem].click();
        }, 5000)
    })
    </script>
</body>

</html>