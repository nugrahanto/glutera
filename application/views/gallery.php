<!DOCTYPE html>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta id="myViewport" name="viewport" content="width=device-width, initial-scale=0.5">
    <title>Glutera - Gallery</title>
    <!-- UIkit CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/uikit.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css" />
</head>

<body>
    <section id="wrapper-bottom"></section>
    <div id="menu-sidebar-right" uk-offcanvas="flip: true">
        <div class="uk-offcanvas-bar right">

            <button class="uk-offcanvas-close" type="button" uk-close></button>

            <div class="uk-margin">
                <a href="<?php echo base_url();?>" class="uk-logo">
                    <img src="<?php echo base_url();?>assets/img/white-logo.png" class="gt-logo-sidebar" alt="" uk-img>
                </a>
            </div>

            <ul class="uk-nav-default uk-nav-parent-icon gt-list-menu-sidebar-right" uk-nav>
                <?php if (!empty($sidebar[0])):
                    foreach ($sidebar as $menu) : ?>
                        <li>
                            <a href="<?php echo $menu->ctr_guid ?>" class="uk-text-uppercase">
                                <?php echo $menu->ctr_name ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                    <?php if (!empty($video->vdo_id)): ?>
                        <li><a href="<?php echo base_url() ?>videos/" class="uk-text-uppercase">All Videos</a></li>
                    <?php endif ?>
                <?php else : ?>
                    <li><a href="" class="uk-text-uppercase">Belum ada menu</a></li>
                <?php endif ?>
                <li><a href="<?php echo base_url() ?>gallery/" class="uk-text-uppercase">Be Everlasting</a></li>
            </ul>

        </div>
    </div>
    <header id="gt-header" class="uk-section uk-section-xsmall uk-section-default">
        <div class="uk-container uk-container-large">
            <div uk-grid>
                <div class="uk-width-1-1">
                    <nav class="uk-navbar" uk-navbar>
                        <div class="uk-navbar-left">
                            <div class="uk-margin">
                                <a href="<?php echo base_url();?>" class="uk-logo">
                                    <img src="<?php echo base_url();?>assets/img/orange-logo.png" alt="" uk-img>
                                </a>
                            </div>
                        </div>
                        <div class="uk-navbar-right">
                            <div class="uk-margin uk-text-right">
                                <button class="uk-button uk-button-text gt-item-menu" uk-toggle="#menu-sidebar-right"><span class="uk-icon uk-margin-small-right" uk-icon="icon: menu"></span></button>
                                <button class="uk-button uk-button-text gt-item-search"><span class="uk-icon uk-margin-small-right" uk-icon="icon: search"></span></button>
                                <?php echo form_open('search', 'class="uk-search uk-search-default gt-custom-search" method="get'); ?>
                                    <button type="button" class="uk-search-icon-flip gt-color-icon gt-hide-search" uk-search-icon></button>
                                    <input class="uk-search-input uk-border-pill uk-text-small gt-header-search gt-background-search" type="search" placeholder="search" name="ar">
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header><!-- /header -->
    <section id="gt-gallery" class="uk-section uk-section-xsmall uk-section-default">
        <div class="uk-container uk-container-large">
            <div uk-grid>
                <div class="uk-width-2-5@m uk-width-1-1@s uk-text-left">
                    <img src="<?php echo base_url();?>assets/img/merchandise.png" class="gt-gallery-category" alt="" uk-img width="90%">
                </div>
                <div class="uk-width-3-5@m uk-width-1-1@s uk-grid-collapse uk-flex uk-flex-center" uk-grid>
                    <div class="uk-width-1-3 uk-width-1-3@s uk-padding-small gt-gallery-thumbnail">
                        <a href="<?php echo base_url();?>gallery/angels">
                            <img src="<?php echo base_url();?>media/merchandise/13-23-februari-2020.jpg" alt="" uk-img >
                        </a>
                    </div>
                    <div class="uk-width-1-3 uk-width-1-3@s uk-padding-small gt-gallery-thumbnail">
                        <a href="<?php echo base_url();?>gallery/angels">
                            <img src="<?php echo base_url();?>media/merchandise/1-15-januari-2020.jpg" alt="" uk-img >
                        </a>
                    </div>
                    <div class="uk-width-1-3 uk-width-1-3@s uk-padding-small gt-gallery-thumbnail">
                        <a href="<?php echo base_url();?>gallery/angels">
                            <img src="<?php echo base_url();?>media/merchandise/24-29-februari-2020.jpg" alt="" uk-img >
                        </a>
                    </div>
                </div>
            </div>
            <hr class="gt-hr-color">
            <div uk-grid>
                <div class="uk-width-1-1@s uk-hidden@m uk-text-center">
                    <img src="<?php echo base_url();?>assets/img/merchandise.png" class="gt-gallery-category" alt="" uk-img width="100%">
                </div>
                <div class="uk-width-3-5@m uk-width-1-1@s uk-grid-collapse uk-flex uk-flex-center" uk-grid>
                    <div class="uk-width-1-3 uk-width-1-3@s uk-padding-small gt-gallery-thumbnail">
                        <a href="<?php echo base_url();?>gallery/merchandise">
                            <img src="<?php echo base_url();?>media/merchandise/1-15-mei-2020.jpg" alt="" uk-img >
                        </a>
                    </div>
                    <div class="uk-width-1-3 uk-width-1-3@s uk-padding-small gt-gallery-thumbnail">
                        <a href="<?php echo base_url();?>gallery/merchandise">
                            <img src="<?php echo base_url();?>media/merchandise/13-19-april-2020.jpg" alt="" uk-img >
                        </a>
                    </div>
                </div>
                <div class="uk-width-2-5@m uk-width-1-1@s uk-visible@m uk-text-left">
                    <img src="<?php echo base_url();?>assets/img/merchandise.png" class="gt-gallery-category" alt="" uk-img width="90%">
                </div>
            </div>
        </div>
    </section>
    <script src="<?php echo base_url();?>assets/js/jquery-3.4.1.min.js"></script>
    <!-- UIkit JS -->
    <script src="<?php echo base_url();?>assets/js/uikit.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/uikit-icons.min.js"></script>
    <!-- Main JS -->
    <script>
    $(document).ready(function() {
        if(screen.width >= 750) {
            $('#myViewport').attr('content', 'width=device-width, initial-scale=0.5');
        } else {
            $('#myViewport').attr('content', 'width=device-width, initial-scale=0.47');
        }
        
        $(window).on('resize', (function() {
            if(screen.width >= 750) {
                $('#myViewport').attr('content', 'width=device-width, initial-scale=0.5');
            } else {
                $('#myViewport').attr('content', 'width=device-width, initial-scale=0.47');
            }
        }))

        $('.gt-item-search').on('click', (function(e) {
            e.preventDefault();

            $('.gt-custom-search').show();
            $('.gt-item-menu').css({
                'margin-right': '126px'
            })
        }))

        $('.gt-hide-search').on('click', (function(e) {
            e.preventDefault();

            $('.gt-custom-search').hide();
            $('.gt-item-menu').css({
                'margin-right': '0'
            })
        }))
    })
    </script>
</body>

</html>