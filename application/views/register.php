<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title><?=$this->config->item("appname")?> | Register</title>

    <!-- Fontfaces CSS-->
    <link href="<?=base_url()?>application/libraries/css/font-face.css" rel="stylesheet" media="all">
    <link href="<?=base_url()?>application/libraries/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url()?>application/libraries/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url()?>application/libraries/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?=base_url()?>application/libraries/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?=base_url()?>application/libraries/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url()?>application/libraries/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url()?>application/libraries/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?=base_url()?>application/libraries/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url()?>application/libraries/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?=base_url()?>application/libraries/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url()?>application/libraries/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?=base_url()?>application/libraries/css/theme.css" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-logo">
                            <a href="#">
                                Register
                            </a>
                        </div>
                        <div id="message">
                            <?php echo $this->session->tempdata('alert_form'); ?>
                        </div>
                        <!-- <div class="login-form"> -->
                        <?php echo form_open('user/save');  ?>
                            <!-- <form action="" method="post"> -->
                                <div class="form-group">
                                    <label>Username</label>
                                    <input class="au-input au-input--full" type="text" name="username" placeholder="Username">
                                </div>
                                <div class="form-group">
                                    <label>Password *min 5 karakter</label>
                                    <input class="au-input au-input--full" type="password" name="password" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <label>Fullname</label>
                                    <input class="au-input au-input--full" type="text" name="name" placeholder="Fullname">
                                </div>
                                <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">register</button>
                            <?php echo form_close(); ?>
                            <!-- </form> -->
                            <div class="register-link">
                                <p>
                                    Already have account?
                                    <a href="<?=site_url()?>">Sign In</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="<?=base_url()?>application/libraries/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?=base_url()?>application/libraries/vendor/slick/slick.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/wow/wow.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/animsition/animsition.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?=base_url()?>application/libraries/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?=base_url()?>application/libraries/vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="<?=base_url()?>application/libraries/js/main.js"></script>

    <script>
        setTimeout(function() {
            $('#alert').hide('fast');
        }, 3000);
    </script>

</body>

</html>
<!-- end document-->