<!DOCTYPE html>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta id="myViewport" name="viewport" content="width=device-width, initial-scale=0.5">
    <title>Glutera - Video | <?php echo $videos->vdo_head; ?></title>
    <!-- UIkit CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/uikit.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css" />
</head>

<body>
    <div id="menu-sidebar" uk-offcanvas>
        <div class="uk-offcanvas-bar">

            <button class="uk-offcanvas-close" type="button" uk-close></button>

            <div class="uk-margin">
                <a href="<?php echo base_url() ?>" class="uk-logo">
                    <img src="<?php echo base_url();?>assets/img/white-logo.png" class="gt-logo-sidebar" alt="" uk-img>
                </a>
            </div>

            <ul class="uk-nav-default uk-nav-parent-icon gt-list-menu-sidebar" uk-nav>
                <?php if (!empty($sidebar[0])):
                    foreach ($sidebar as $menu) : ?>
                        <li>
                            <a href="<?php echo $menu->ctr_guid ?>" class="uk-text-uppercase">
                                <?php echo $menu->ctr_name ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                    <li><a href="<?php echo base_url() ?>videos/" class="uk-text-uppercase">All Videos</a></li>
                <?php else : ?>
                    <li><a href="" class="uk-text-uppercase">Belum ada menu</a></li>
                <?php endif ?>
                <li><a href="<?php echo base_url() ?>gallery/" class="uk-text-uppercase">Be Everlasting</a></li>
            </ul>

        </div>
    </div>
    <section id="gt-article" class="uk-grid-collapse uk-child-width-expand@s" uk-grid>
        <div class="uk-width-1-5@m uk-visible@m">
            <div class="uk-background-default gt-menu-sidebar uk-light" uk-height-viewport uk-sticky>
                <div class="uk-margin">
                    <a href="<?php echo base_url() ?>" class="uk-logo">
                        <img src="<?php echo base_url();?>assets/img/white-logo.png" class="gt-logo-sidebar" alt="" uk-img>
                    </a>
                </div>
                <ul class="uk-nav-default uk-nav-parent-icon gt-list-menu-sidebar" uk-nav>
                    <?php if (!empty($sidebar[0])):
                    foreach ($sidebar as $menu) : ?>
                        <li>
                            <a href="<?php echo $menu->ctr_guid ?>" class="uk-text-uppercase">
                                <?php echo $menu->ctr_name ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                    <li><a href="<?php echo base_url() ?>videos/" class="uk-text-uppercase">All Videos</a></li>
                <?php else : ?>
                    <li><a href="" class="uk-text-uppercase">Belum ada menu</a></li>
                <?php endif ?>
                <li><a href="<?php echo base_url() ?>gallery/" class="uk-text-uppercase">Be Everlasting</a></li>
                </ul>
            </div>
        </div>
        <div class="uk-width-4-5@m">
            <div class="uk-card uk-card-muted uk-card-body" uk-grid>
                <div class="uk-width-1-1">
                    <!-- <nav class="uk-navbar uk-hidden@m" uk-navbar>
                        <div class="uk-navbar-left">
                            <div class="uk-margin">
                                <ul class="uk-navbar-nav">
                                    <li>
                                        <a href="#" class="uk-padding-remove-left" uk-toggle="target: #menu-sidebar">
                                            <span class="uk-icon uk-margin-small-right" uk-icon="icon: menu"></span>&nbsp;Menu
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav> -->
                    <nav class="uk-navbar" uk-navbar>
                        <div class="uk-navbar-left uk-hidden@m">
                            <div class="uk-margin">
                                <button class="uk-button uk-button-text gt-item-menu" uk-toggle="#menu-sidebar"><span class="uk-icon uk-margin-small-right" uk-icon="icon: menu"></span></button>
                            </div>
                        </div>
                    </nav>
                    <nav class="uk-navbar" uk-navbar>
                        <div class="uk-navbar-left">
                            <div class="uk-margin">
                            </div>
                        </div>
                        <div class="uk-navbar-right">
                            <div class="uk-margin">
                                <?php echo form_open('search', 'class="uk-search uk-search-default" method="get'); ?>
                                    <a href="" class="uk-search-icon-flip gt-color-icon" uk-search-icon></a>
                                    <input class="uk-search-input uk-border-pill uk-text-small gt-header-search gt-background-search" type="search" placeholder="search" name="ar">
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </nav>
                </div>
                <div class="uk-child-width-1-2 uk-grid-collapse uk-child-width-1-4@m uk-child-width-1-3@s" uk-grid>
                        <div uk-lightbox>
                            <a href="<?php echo $videos->vdo_link ?>" class="gt-decoration-none">
                                <div class="uk-card uk-card-small uk-card-muted uk-card-body gt-card-video">
                                    <img src="<?php echo $videos->vdo_img ?>" alt="" uk-img >
                                    <h3 class="uk-card-title uk-text-small uk-margin-small-top uk-margin-remove-bottom"><?php echo $videos->vdo_head; ?></h3>
                                    <p class="uk-text-muted uk-text-small uk-margin-remove"><?php echo date('d F Y', strtotime($videos->created_date)); ?></p>
                                </div>
                            </a>
                        </div>
                </div>
            </div>
        </div>
    </section>
    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.4.1.min.js"></script>
    <!-- UIkit JS -->
    <script src="<?php echo base_url();?>assets/js/uikit.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/uikit-icons.min.js"></script>
    <!-- Main JS -->
    <script>
    $(document).ready(function() {
        if(screen.width >= 750) {
            $('#myViewport').attr('content', 'width=device-width, initial-scale=0.5');
        } else {
            $('#myViewport').attr('content', 'width=device-width, initial-scale=0.47');
        }
        
        $(window).on('resize', (function() {
            if(screen.width >= 750) {
                $('#myViewport').attr('content', 'width=device-width, initial-scale=0.5');
            } else {
                $('#myViewport').attr('content', 'width=device-width, initial-scale=0.47');
            }
        }))
    })
    </script>
</body>

</html>