<!DOCTYPE html>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta id="myViewport" name="viewport" content="width=device-width, initial-scale=0.5">
    <title>Glutera - <?php if (isset($category)) { echo ucwords($category->ctr_name); } else if (isset($tag)) { echo "Hashtag #".$tag;} else { echo "All Article"; } ?></title>
    <!-- UIkit CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/uikit.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css" />
</head>

<body>
    <div id="menu-sidebar" uk-offcanvas>
        <div class="uk-offcanvas-bar">

            <button class="uk-offcanvas-close" type="button" uk-close></button>

            <div class="uk-margin">
                <a href="<?php echo base_url() ?>" class="uk-logo">
                    <img src="<?php echo base_url();?>assets/img/white-logo.png" class="gt-logo-sidebar" alt="" uk-img>
                </a>
            </div>

            <ul class="uk-nav-default uk-nav-parent-icon gt-list-menu-sidebar" uk-nav>
                <?php if (!empty($sidebar[0])):
                    foreach ($sidebar as $menu) : ?>
                        <li>
                            <a href="<?php echo $menu->ctr_guid ?>" class="uk-text-uppercase">
                                <?php echo $menu->ctr_name ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                    <?php if (!empty($video->vdo_id)): ?>
                        <li><a href="<?php echo base_url() ?>videos/" class="uk-text-uppercase">All Videos</a></li>
                    <?php endif ?>
                <?php else : ?>
                    <li><a href="" class="uk-text-uppercase">Belum ada menu</a></li>
                <?php endif ?>
                <li><a href="<?php echo base_url() ?>gallery/" class="uk-text-uppercase">Be Everlasting</a></li>
            </ul>

        </div>
    </div>
    <section id="gt-article" class="uk-grid-collapse uk-child-width-expand@s" uk-grid>
        <div class="uk-width-1-5@m uk-visible@m">
            <div class="uk-background-default gt-menu-sidebar uk-light" uk-height-viewport uk-sticky>
                <div class="uk-margin">
                    <a href="<?php echo base_url() ?>" class="uk-logo">
                        <img src="<?php echo base_url();?>assets/img/white-logo.png" class="gt-logo-sidebar" alt="" uk-img>
                    </a>
                </div>
                <ul class="uk-nav-default uk-nav-parent-icon gt-list-menu-sidebar" uk-nav>
                    <?php if (!empty($sidebar[0])):
                        foreach ($sidebar as $menu) : ?>
                            <li><a href="<?php echo $menu->ctr_guid ?>" class="uk-text-uppercase"><?php echo $menu->ctr_name ?></a></li>
                        <?php endforeach; ?>
                        <?php if (!empty($video->vdo_id)): ?>
                            <li><a href="<?php echo base_url() ?>videos/" class="uk-text-uppercase">All Videos</a></li>
                        <?php endif ?>
                    <?php else : ?>
                        <li><a href="">Belum ada menu</a></li>
                    <?php endif ?>
                    <li><a href="<?php echo base_url() ?>gallery/" class="uk-text-uppercase">Be Everlasting</a></li>
                </ul>
            </div>
        </div>
        <div class="uk-width-4-5@m">
            <div class="uk-card uk-card-muted uk-card-body" uk-grid>
                <div class="uk-width-1-1">
                    <nav class="uk-navbar" uk-navbar>
                        <div class="uk-navbar-left uk-hidden@m">
                            <div class="uk-margin">
                                <button class="uk-button uk-button-text gt-item-menu" uk-toggle="#menu-sidebar"><span class="uk-icon uk-margin-small-right" uk-icon="icon: menu"></span></button>
                            </div>
                        </div>
                    </nav>
                    <nav class="uk-navbar" uk-navbar>
                        <div class="uk-navbar-left">
                            <div class="uk-margin">
                                <!-- <label class="uk-form-label" for="form-horizontal-select">SORT:</label>
                                <div uk-form-custom="target: > * > span:first-child">
                                    <select>
                                        <option value="">Please select...</option>
                                        <option value="1">Most Popular</option>
                                        <option value="2">Newest</option>
                                    </select>
                                    <button class="uk-button uk-button-default uk-padding-remove-left uk-padding-remove-right" type="button" tabindex="-1" style="border-color: transparent; margin-top: -0.8px;">
                                        <span></span>
                                        <span uk-icon="icon: chevron-down"></span>
                                    </button>
                                </div> -->
                            </div>
                        </div>
                        <div class="uk-navbar-right">
                            <div class="uk-margin">
                                <?php echo form_open('search', 'class="uk-search uk-search-default" method="get'); ?>
                                    <a href="" class="uk-search-icon-flip gt-color-icon" uk-search-icon></a>
                                    <input class="uk-search-input uk-border-pill uk-text-small gt-header-search gt-background-search" type="search" placeholder="search" name="ar">
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </nav>
                </div>

                <?php if ($posts[0]['count'] != 0): ?>
                    <?php foreach ($posts as $post): ?>
                        <div class="uk-grid-collapse uk-margin-top uk-margin-bottom" uk-grid style="width: 100% !important">
                            <div class="uk-width-3-5 uk-width-4-5@m uk-width-3-5@s uk-padding uk-padding-remove-top uk-padding-remove-left">
                                <hr class="gt-divider">
                                <ul class="gt-article-meta">
                                    <?php if (isset($category)) : ?>
                                        <li><?php echo $category->ctr_name; ?></li>
                                    <?php elseif (isset($tag)) : ?>
                                        <li style="text-transform: capitalize !important;"><?php echo "#".$tag; ?></li>
                                    <?php endif ?>
                                        <li><?php echo date('d F Y', strtotime($post['created_date'])); ?></li>
                                </ul>
                                <a href="<?php echo $post['pst_guid'] ?>"><h2 class="uk-margin-remove-top uk-margin-remove-bottom gt-article-title" style="text-transform: capitalize;"><?php echo $post['pst_head'] ?></h2></a>
                                <ul class="gt-article-meta">
                                    <li>Ditulis oleh <?php echo $post['created_by'] ?></li>
                                </ul>
                            </div>
                            <div class="uk-width-2-5 uk-width-1-5@m uk-width-2-5@s gt-article-thumbnail">
                                <a href="<?php echo $post['pst_guid'] ?>"><img src="<?php echo $post['img'] ?>" alt="" uk-img></a>
                            </div>
                        </div>
                    <?php endforeach ?>
                <?php else : ?>
                    Belum ada artikel
                <?php endif ?>

            </div>
        </div>
    </section>
    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.4.1.min.js"></script>
    <!-- UIkit JS -->
    <script src="<?php echo base_url();?>assets/js/uikit.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/uikit-icons.min.js"></script>
    <!-- Main JS -->
    <script>
    $(document).ready(function() {
        if(screen.width >= 750) {
            $('#myViewport').attr('content', 'width=device-width, initial-scale=0.5');
        } else {
            $('#myViewport').attr('content', 'width=device-width, initial-scale=0.47');
        }
        
        $(window).on('resize', (function() {
            if(screen.width >= 750) {
                $('#myViewport').attr('content', 'width=device-width, initial-scale=0.5');
            } else {
                $('#myViewport').attr('content', 'width=device-width, initial-scale=0.47');
            }
        }))
    })
    </script>
</body>

</html>