<?php
class Home_model extends CI_Model {

    var $p_table = 'grgl_posts';
    var $v_table = 'grgl_video';
    var $s_table = 'grgl_socialmedia';

    function __construct() 
    {
        parent::__construct();
    }

    function newArticle()
    {
        $ret_ = array();
		$post = $this->db->query("SELECT * FROM `grgl_posts` WHERE `pst_status` = 'publish' ORDER BY `pst_id` DESC LIMIT 3");

        if ($post->num_rows() != 0) {
            foreach ($post->result() as $data) {
                $id  = $data->pst_id;
                $img = $this->db->query("SELECT I.* FROM `grgl_img` I LEFT JOIN `grgl_imgmeta` IM ON I.`img_id` = IM.`img_id` WHERE `meta_key` = '_img_post_' AND `meta_value` = $id ORDER BY RAND() LIMIT 1");
                if ($img->num_rows() > 0) {
                    $img = $img->row();
                    $img = $img->img_guid;
                } else {
                    $img = base_url()."attachments/00-default.jpg";
                }
                array_push($ret_, array(
                    'count'=>$post->num_rows(),
                    'pst_id'=>$id,
                    'pst_head'=>$data->pst_head,
                    'pst_slug'=>$data->pst_slug,
                    'pst_guid'=>$data->pst_guid,
                    'img'=>$img,
                ));
            }
        } else {
            array_push($ret_, array(
                'count'=>0,
            ));
        }

        return $ret_;
    }

    function homeMenu() 
    {
        $ret_ = array();
        $cat = $this->db->query("SELECT * FROM `grgl_category` WHERE ctr_status = 'publish'");
        if ($cat->num_rows() > 0) {
            foreach ($cat->result() as $data) {
                $id = $data->ctr_id;
                $pId = $this->db->query("SELECT PM.`pst_id`, P.`pst_slug`, P.`pst_guid`, P.`pst_head` FROM `grgl_posts` P LEFT JOIN `grgl_postsmeta` PM ON P.`pst_id` = PM.`pst_id` WHERE `meta_key` = '_post_category_' AND `meta_value` = $id AND P.`pst_status` = 'publish' ORDER BY RAND() DESC LIMIT 1");
                if ($pId->num_rows() > 0) {
                    $pId = $pId->row();
                    $img = $this->db->query("SELECT I.* FROM `grgl_img` I LEFT JOIN `grgl_imgmeta` IM ON I.`img_id` = IM.`img_id` WHERE `meta_key` = '_img_post_' AND `meta_value` = $pId->pst_id ORDER BY RAND() LIMIT 1");
                    if ($img->num_rows() > 0) {
                        $phead  = $pId->pst_head;
                        $pguid  = $pId->pst_guid;
                        $pslug  = $pId->pst_slug;
                        $img    = $img->row();
                        $img    = $img->img_guid;
                    } else {
                        $phead  = $pId->pst_head;
                        $pguid  = $pId->pst_guid;
                        $pslug  = $pId->pst_slug;
                        $img = base_url()."attachments/00-default.jpg";
                    }
                } else {
                    $phead  = "";
                    $pguid  = "";
                    $pslug  = "";
                    $img    = base_url()."attachments/00-default.jpg";
                }

                array_push($ret_, array(
                    'count'=>$cat->num_rows(),
                    'cat_id'=>$id,
                    'cat_name'=>$data->ctr_name,
                    'cat_slug'=>$data->ctr_slug,
                    'cat_guid'=>$data->ctr_guid,
                    'phead'=>$phead,
                    'pguid'=>$pguid,
                    'pslug'=>$pslug,
                    'img'=>$img,
                ));
            }
        } else {
            array_push($ret_, array(
                'count'=>0,
            ));
        }
        return $ret_;
    }

    function postbyCategory($category)
    {
        $ret_ = array();
        $ct = $this->getCategory($category);

        if ($ct->ctr_id != "") {
            $post = $this->db->query("SELECT * FROM `grgl_posts` P LEFT JOIN `grgl_postsmeta` PM ON P.`pst_id` = PM.`pst_id` WHERE P.`pst_status` = 'Publish' AND PM.`meta_key` = '_post_category_' AND PM.`meta_value` = $ct->ctr_id ORDER BY P.`created_date` DESC");

            if ($post->num_rows() != 0) {
                foreach ($post->result() as $data) {
                    $id  = $data->pst_id;
                    $img = $this->db->query("SELECT I.* FROM `grgl_img` I LEFT JOIN `grgl_imgmeta` IM ON I.`img_id` = IM.`img_id` WHERE `meta_key` = '_img_post_' AND `meta_value` = $id ORDER BY RAND() LIMIT 1");
                    if ($img->num_rows() > 0) {
                        $img = $img->row();
                        $img = $img->img_guid;
                    } else {
                        $img = base_url()."attachments/00-default.jpg";
                    }
                    array_push($ret_, array(
                        'count'=>$post->num_rows(),
                        'pst_id'=>$id,
                        'pst_head'=>$data->pst_head,
                        'pst_slug'=>$data->pst_slug,
                        'pst_guid'=>$data->pst_guid,
                        'pst_txt'=>$data->pst_txt,
                        'created_date'=>$data->created_date,
                        'created_by'=>$data->created_by,
                        'img'=>$img,
                    ));
                }
            } else {
                array_push($ret_, array(
                    'count'=>0,
                ));
            }
        } else {
            array_push($ret_, array(
                'count'=>0,
            ));
        }

        return $ret_;
    }

    function postbyTags($tag)
    {
        $ret_ = array();
        $condition = '%'.$tag.'%';
        $post = $this->db->query("SELECT P.* FROM `grgl_posts` P LEFT JOIN `grgl_postsmeta` PM ON P.`pst_id` = PM.`pst_id` WHERE P.`pst_status` = 'publish' AND PM.`meta_key` = '_post_tags_' AND PM.`meta_value` LIKE '$condition' ORDER BY P.`created_date` DESC");

        if ($post->num_rows() > 0) {
            foreach ($post->result() as $data) {
                $id  = $data->pst_id;
                $img = $this->db->query("SELECT I.* FROM `grgl_img` I LEFT JOIN `grgl_imgmeta` IM ON I.`img_id` = IM.`img_id` WHERE `meta_key` = '_img_post_' AND `meta_value` = $id ORDER BY RAND() LIMIT 1");
                if ($img->num_rows() > 0) {
                    $img = $img->row();
                    $img = $img->img_guid;
                } else {
                    $img = base_url()."attachments/00-default.jpg";
                }
                array_push($ret_, array(
                    'count'=>$post->num_rows(),
                    'pst_id'=>$id,
                    'pst_head'=>$data->pst_head,
                    'pst_slug'=>$data->pst_slug,
                    'pst_guid'=>$data->pst_guid,
                    'pst_txt'=>$data->pst_txt,
                    'created_date'=>$data->created_date,
                    'created_by'=>$data->created_by,
                    'img'=>$img,
                ));
            }
        } else {
            array_push($ret_, array(
                'count'=>0,
            ));
        }

        return $ret_;
    }

    function getallPost()
    {
        $ret_ = array();

        $post = $this->db->query("SELECT * FROM `grgl_posts` WHERE `pst_status` = 'publish' ORDER BY `created_date` DESC");

        if ($post->num_rows() != 0) {
            foreach ($post->result() as $data) {
                $id  = $data->pst_id;
                $img = $this->db->query("SELECT I.* FROM `grgl_img` I LEFT JOIN `grgl_imgmeta` IM ON I.`img_id` = IM.`img_id` WHERE `meta_key` = '_img_post_' AND `meta_value` = $id ORDER BY RAND() LIMIT 1");
                if ($img->num_rows() > 0) {
                    $img = $img->row();
                    $img = $img->img_guid;
                } else {
                    $img = base_url()."attachments/00-default.jpg";
                }
                array_push($ret_, array(
                    'count'=>$post->num_rows(),
                    'pst_id'=>$id,
                    'pst_head'=>$data->pst_head,
                    'pst_slug'=>$data->pst_slug,
                    'pst_guid'=>$data->pst_guid,
                    'pst_txt'=>$data->pst_txt,
                    'created_date'=>$data->created_date,
                    'created_by'=>$data->created_by,
                    'img'=>$img,
                ));
            }
        } else {
            array_push($ret_, array(
                'count'=>0,
            ));
        }

        return $ret_;
    }

    function getfimgbyPost($slug) {
        $post  = $this->getpostbySlug($slug)->row();
        $fimg1 = $this->db->query("SELECT I.`img_id`, I.`img_name`, I.`img_guid` FROM `grgl_img` I LEFT JOIN `grgl_imgmeta` IM ON I.`img_id` = IM.`img_id` LEFT JOIN `grgl_imgmeta` IMM ON IM.`img_id` = IMM.`img_id` WHERE IM.`meta_key` = '_img_post_' AND IM.`meta_value` = $post->pst_id AND IMM.`meta_key`='_feature_img_' AND IMM.`meta_value` = 1 GROUP BY I.`img_id`");
        $fimg2 = $this->db->query("SELECT I.`img_id`, I.`img_name`, I.`img_guid` FROM `grgl_img` I LEFT JOIN `grgl_imgmeta` IM ON I.`img_id` = IM.`img_id` LEFT JOIN `grgl_imgmeta` IMM ON IM.`img_id` = IMM.`img_id` WHERE IM.`meta_key` = '_img_post_' AND IM.`meta_value` = $post->pst_id AND IMM.`meta_key`='_feature_img_' AND IMM.`meta_value` = 2 GROUP BY I.`img_id`");

        if ($fimg1->num_rows() > 0) {
            $fimg1      = $fimg1->row();
            $fimg1_id   = $fimg1->img_id;
            $fimg1_name = $fimg1->img_name;
            $fimg1_guid = $fimg1->img_guid;
        } else {
            $fimg1_name = "";
            $fimg1_id   = "";
            $fimg1_guid = base_url()."attachments/00-default.jpg";
        }

        if ($fimg2->num_rows() > 0) {
            $fimg2      = $fimg2->row();
            $fimg2_id   = $fimg2->img_id;
            $fimg2_name = $fimg2->img_name;
            $fimg2_guid = $fimg2->img_guid;
        } else {
            $fimg2_name = "";
            $fimg2_id   = "";
            $fimg2_guid = base_url()."attachments/00-default.jpg";
        }

        $imTemp_ = array();
        array_push($imTemp_, array(
            'ifimg1'=>$fimg1_id,
            'ifimg2'=>$fimg2_id,
            'nfimg1'=>$fimg1_name,
            'nfimg2'=>$fimg2_name,
            'gfimg1'=>$fimg1_guid,
            'gfimg2'=>$fimg2_guid,
        ));

        return $imTemp_;
    }

    function getfimgbyPost2($slug) {
        $post  = $this->getpostbySlug($slug)->row();
        $fimg = $this->db->query("SELECT I.`img_id`, I.`img_name`, I.`img_guid` FROM `grgl_img` I LEFT JOIN `grgl_imgmeta` IM ON I.`img_id` = IM.`img_id` LEFT JOIN `grgl_imgmeta` IMM ON IM.`img_id` = IMM.`img_id` WHERE IM.`meta_key` = '_img_post_' AND IM.`meta_value` = $post->pst_id GROUP BY I.`img_id`");

        return $fimg->result();
    }

    function getrelatedPost($slug)
    {
        $ret_ = array();
        $post = $this->db->query("SELECT * FROM `grgl_posts` WHERE `pst_slug` = '$slug'")->row();

        $id  = $post->pst_id;
        $pmeta = $this->db->query("SELECT `meta_value` FROM `grgl_postsmeta` WHERE `meta_key` = '_post_category_' AND `pst_id` = $id ORDER BY RAND() LIMIT 1");

        if ($pmeta->num_rows() > 0) {
            $pmeta  = $pmeta->row();
            $post = $this->db->query("SELECT P.* FROM `grgl_posts` P LEFT JOIN `grgl_postsmeta` PM ON P.`pst_id` = PM.`pst_id` WHERE P.`pst_status` = 'publish' AND PM.`meta_key` = '_post_category_' AND PM.`meta_value` = $pmeta->meta_value GROUP BY RAND() LIMIT 3");

            if ($post->num_rows() > 3) {
                foreach ($post->result() as $data) {
                    $pid = $data->pst_id;
                    $img = $this->db->query("SELECT I.* FROM `grgl_img` I LEFT JOIN `grgl_imgmeta` IM ON I.`img_id` = IM.`img_id` WHERE `meta_key` = '_img_post_' AND `meta_value` = $pid ORDER BY RAND() LIMIT 1");
                    if ($img->num_rows() > 0) {
                        $img = $img->row();
                        $img = $img->img_guid;
                    } else {
                        $img = base_url()."attachments/00-default.jpg";
                    }

                    array_push($ret_, array(
                        'count'=>$post->num_rows(),
                        'pst_id'=>$pid,
                        'pst_head'=>$data->pst_head,
                        'pst_slug'=>$data->pst_slug,
                        'pst_guid'=>$data->pst_guid,
                        'img'=>$img,
                    )); 
                }
            } else {
                $post = $this->db->query("SELECT * FROM `grgl_posts` WHERE `pst_status` = 'publish' GROUP BY RAND() LIMIT 3");
                foreach ($post->result() as $data) {
                    $pid = $data->pst_id;
                    $img = $this->db->query("SELECT I.* FROM `grgl_img` I LEFT JOIN `grgl_imgmeta` IM ON I.`img_id` = IM.`img_id` WHERE `meta_key` = '_img_post_' AND `meta_value` = $pid ORDER BY RAND() LIMIT 1");
                    if ($img->num_rows() > 0) {
                        $img = $img->row();
                        $img = $img->img_guid;
                    } else {
                        $img = base_url()."attachments/00-default.jpg";
                    }

                    array_push($ret_, array(
                        'count'=>$post->num_rows(),
                        'pst_id'=>$pid,
                        'pst_head'=>$data->pst_head,
                        'pst_slug'=>$data->pst_slug,
                        'pst_guid'=>$data->pst_guid,
                        'img'=>$img,
                    )); 
                }
            } 
        } else {
            $post = $this->db->query("SELECT * FROM `grgl_posts` WHERE `pst_status` = 'publish' GROUP BY RAND() LIMIT 3");
            foreach ($post->result() as $data) {
                $pid = $data->pst_id;
                $img = $this->db->query("SELECT I.* FROM `grgl_img` I LEFT JOIN `grgl_imgmeta` IM ON I.`img_id` = IM.`img_id` WHERE `meta_key` = '_img_post_' AND `meta_value` = $pid ORDER BY RAND() LIMIT 1");
                if ($img->num_rows() > 0) {
                    $img = $img->row();
                    $img = $img->img_guid;
                } else {
                    $img = base_url()."attachments/00-default.jpg";
                }

                array_push($ret_, array(
                    'count'=>$post->num_rows(),
                    'pst_id'=>$pid,
                    'pst_head'=>$data->pst_head,
                    'pst_slug'=>$data->pst_slug,
                    'pst_guid'=>$data->pst_guid,
                    'img'=>$img,
                )); 
            }
        }

        return $ret_;
    }

    function getpostbySearch()
    {
        $ret_ = array();
        $condition = '%'.$this->input->get('ar').'%';
        $post = $this->db->query("SELECT * FROM `grgl_posts` WHERE `pst_status` = 'publish' AND `pst_head` LIKE '$condition' ORDER BY `created_date` DESC");

        if ($post->num_rows() != 0) {
            foreach ($post->result() as $data) {
                $id  = $data->pst_id;
                $img = $this->db->query("SELECT I.* FROM `grgl_img` I LEFT JOIN `grgl_imgmeta` IM ON I.`img_id` = IM.`img_id` WHERE `meta_key` = '_img_post_' AND `meta_value` = $id ORDER BY RAND() LIMIT 1");
                if ($img->num_rows() > 0) {
                    $img = $img->row();
                    $img = $img->img_guid;
                } else {
                    $img = base_url()."attachments/00-default.jpg";
                }
                array_push($ret_, array(
                    'count'=>$post->num_rows(),
                    'pst_id'=>$id,
                    'pst_head'=>$data->pst_head,
                    'pst_slug'=>$data->pst_slug,
                    'pst_guid'=>$data->pst_guid,
                    'pst_txt'=>$data->pst_txt,
                    'created_date'=>$data->created_date,
                    'created_by'=>$data->created_by,
                    'img'=>$img,
                ));
            }
        } else {
            array_push($ret_, array(
                'count'=>0,
            ));
        }

        return $ret_;
    }

    function getpostbySlug($slug)
    {
        return $this->db->get_where($this->p_table,array('pst_slug =' => $slug));
    }

    function getpostSourcebySlug($slug)
    {
        return $this->db->query("SELECT `grgl_postsmeta`.* FROM `grgl_postsmeta` LEFT JOIN `grgl_posts` ON `grgl_postsmeta`.`pst_id` = `grgl_posts`.`pst_id` WHERE `grgl_posts`.`pst_slug` = '$slug' AND `meta_key` = '_post_source_'")->row();
    }

    function getpostTagsbySlug($slug)
    {
        $tags = $this->db->query("SELECT `grgl_postsmeta`.* FROM `grgl_postsmeta` LEFT JOIN `grgl_posts` ON `grgl_postsmeta`.`pst_id` = `grgl_posts`.`pst_id` WHERE `grgl_posts`.`pst_slug` = '$slug' AND `meta_key` = '_post_tags_'");
        if ($tags->num_rows() > 0) {
            $tags = $tags->row();
            $ret_ = explode(',', $tags->meta_value);
        } else {
            $ret_ = "";
        }
        return $ret_;
    }

    function getCategory($slug)
    {
        return $this->db->query("SELECT * FROM `grgl_category` WHERE `ctr_slug` = '$slug'")->row();
    }

    function getSidebar()
    {
        return $this->db->query("SELECT * FROM `grgl_category` WHERE `ctr_status` = 'Publish'")->result();
    }

    function getAllVideo()
    {
        $this->db->order_by('created_date', 'desc' );
        return $this->db->get($this->v_table);
    }

    function getVideo($slug)
    {
        return $this->db->get_where($this->v_table,array('vdo_slug =' => $slug));
    }

    function getSocial()
    {
        return $this->db->get($this->s_table);
    }
}