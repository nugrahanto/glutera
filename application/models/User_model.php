<?php
class User_model extends CI_Model {

    var $table = 'grgl_user';
	var $no = 0;

    function __construct() {
        parent::__construct();
		// $this->load->library('encrypt');
    }

    function getone($w)
    {
        return $this->db->get_where($this->table,$w);
    }

    function getall()
    {
        $this->db->order_by("created_date", "desc");
        return $this->db->get($this->table);
    }

    function userbyFilters($w)
    {
        $this->db->where('usr_stat', $w);
        $this->db->order_by("created_date", "desc");
        return $this->db->get($this->table);
    }

    function insert()
    {
		$user = $this->input->post('username');
		if(empty($user))
			return null;
		else{
		
			$data['usr_nick']=$this->input->post('username');
			$data['usr_psswrd']=md5($this->input->post('password'));
			$data['usr_name']=$this->input->post('name');
			
			return $this->db->insert($this->table, $data);
		}
    }

    function update_account($method)
    {
        if ($method == 'useredit') {
            $name        = $this->input->post('name');
            $own         = $this->input->post('own');
            $modified_by = $this->session->userdata['uNick'];

            if ($modified_by == "") {
                return 404;
            } else {
                if ($own == md5($modified_by)) {
                    if (! $this->db->query("UPDATE `grgl_user` SET `usr_name` = '$name', `modified_by` = '$modified_by' WHERE `usr_nick` = '$modified_by'")) {
                        $code = $this->db->error();
                        return $code;
                    } else {
                        $data_session = array(
                        'uName' => $name
                        );
         
                        $this->session->set_userdata($data_session);
                        return 200;
                    }
                } else {
                    return 404;
                }
            }
        } elseif ($method == 'passedit') {
            $passold     = $this->input->post('oldpass');
            $passnew     = md5($this->input->post('newpass'));
            $own         = $this->input->post('own');
            $modified_by = $this->session->userdata['uNick'];

            if ($modified_by == "") {
                return 404;
            } else {
                if ($own == md5($modified_by)) {

                    $where = array(
                        'usr_nick' => $this->session->userdata['uNick'],
                        'usr_psswrd' => md5($passold)
                    );
                    $usercek = $this->lCheck($where)->num_rows();

                    if ($usercek > 0) {
                        if (! $this->db->query("UPDATE `grgl_user` SET `usr_psswrd` = '$passnew', `modified_by` = '$modified_by' WHERE `usr_nick` = '$modified_by'")) {
                            $code = $this->db->error();
                            return $code;
                        } else {
                            return 200;
                        }
                    } else {
                        return 500;
                    }

                } else {
                    return 404;
                }
            }
        } else {
            return 404;
        }
    }

    function update_role() 
    {
        $user        = $this->input->post('user');
        $status      = $this->input->post('status');
        $modified_by = $this->session->userdata['uNick'];
        
        if ($modified_by == "") {
            return 404;
        } else {
            $this->db->set('usr_stat', $status);
            $this->db->set('modified_by', $modified_by);
            $this->db->where('usr_nick', $user);
            if (! $this->db->update($this->table)) {
                $code = $this->db->error();
                return $code;
            } else {
                return 200;
            }
        }
    }

    function delete()
    {
        $user = $this->input->post('user');
        $modified_by = $this->session->userdata['uNick'];

        if ($modified_by == "") {
            return 404;
        } else {
            $this->db->where('usr_nick', $user);
            if (! $this->db->delete($this->table)) {
                $code = $this->db->error();
                return $code;
            } else {
                return 200;
            }
        }
    }

    function login($w)
    {
		$where = array('usr_nick' => $w['usr_nick'], 'usr_psswrd' => $w['usr_psswrd']);

    	$ret = $this->db->select('usr_nick, usr_name, usr_stat')
    				->where($where)
    				->get($this->table);
    	return $ret->row();
    }

    function lCheck($w)
    {
    	return $this->db->get_where($this->table,$w);
    }
}