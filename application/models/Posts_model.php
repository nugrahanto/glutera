<?php
class Posts_model extends CI_Model {

    var $c_table = 'grgl_category';
    var $p_table = 'grgl_posts';
    var $v_table = 'grgl_video';
	var $no = 0;

    function __construct() {
        parent::__construct();
    }

    function c_get_all()
    {
        return $this->db->query("SELECT `grgl_category`.*, COUNT(`grgl_posts`.`pst_id`) AS `jumlah` FROM `grgl_category` LEFT JOIN `grgl_posts` ON `grgl_category`.`ctr_id` = `grgl_posts`.`pst_cat` WHERE `grgl_category`.ctr_status = 'publish' GROUP BY `grgl_category`.`ctr_id`");

        // SELECT `grgl_category`.*, COUNT(`grgl_postsmeta`.`pst_id`) AS `jumlah` FROM `grgl_category` LEFT JOIN `grgl_postsmeta` ON `grgl_category`.`ctr_id` = `grgl_postsmeta`.`meta_value` LEFT JOIN `grgl_posts` ON `grgl_postsmeta`.`pst_id` = `grgl_posts`.`pst_id` WHERE `grgl_category`.ctr_status = 'publish' AND  `grgl_postsmeta`.`meta_key` = '_post_category_' AND `grgl_posts`.`pst_status` = 'publish' GROUP BY `grgl_category`.`ctr_id`
    }

    function c_get_slug($slug)
    {
        return $this->db->get_where($this->c_table,array('ctr_slug =' => $slug));
    }

    function c_get_notin_id($id, $slug)
    {
        return $this->db->query("SELECT `grgl_category`.* FROM `grgl_category` WHERE `grgl_category`.ctr_slug = '$slug' AND `grgl_category`.ctr_id NOT IN ($id)");
    }

    function c_insert()
    {
		$name = $this->input->post('name');
        $slug = $this->input->post('slug');
        $desc = $this->input->post('description');
		if(empty($slug)) {
			$slug = slugify($name);
        } else {
            $slug = slugify($slug);
        }

        if ($desc == "") {
            $desc = '-';
        }

		$data['ctr_name']    = $name;
		$data['ctr_slug']    = $slug;
		$data['ctr_desc']    = $desc;
        $data['ctr_status']  = "publish";
        $data['ctr_guid']    = base_url().'category/'.$slug.'/';
        $data['created_by']  = $this->session->userdata['uNick'];
        $data['modified_by'] = $this->session->userdata['uNick'];

        if ($data['created_by'] == "" OR $data['modified_by'] == "") {
            $code = "400";
            return $code;
        } else {
            if (! $this->db->insert($this->c_table, $data)) {
                $code = $this->db->error();
                return $code;
            } else {
                return 200;
            }
        }
    }

    function c_update()
    {  
        $id          = $this->input->post('id');
        $name        = $this->input->post('name');
        $slug        = $this->input->post('slug');
        $desc        = $this->input->post('description');
        $modified_by = $this->session->userdata['uNick'];

        if(empty($slug)) {
            $slug = slugify($name);
        } else {
            $slug = slugify($slug);
        }

        if ($desc == "") {
            $desc = '-';
        }

        $guid        = base_url().'category/'.$slug.'/';

        if ($modified_by == "") {
            $code = "400";
            return $code;
        } else {
            if (! $this->db->query("UPDATE `grgl_category` SET `ctr_name` = '$name', `ctr_slug` = '$slug', `ctr_desc` = '$desc', `ctr_guid` = '$guid', `modified_by` = '$modified_by' WHERE `ctr_id` = $id")) {
                $code = $this->db->error();
                return $code;
            } else {
                return 200;
            }
        }
    }

    function c_delete()
    {
        $slug        = $this->input->post('f_cdel');
        $status      = 'delete';
        $modified_by = $this->session->userdata['uNick'];

        if ($modified_by == "") {
            $code = "400";
            return $code;
        } else {
            if (! $this->db->query("UPDATE `grgl_category` SET `ctr_status` = '$status', `modified_by` = '$modified_by' WHERE `ctr_slug` = '$slug'")) {
                $code = $this->db->error();
                return $code;
            } else {
                return 200;
            }
        }
    }

    function pm_get_catbyId($id) 
    {
        $cat = "";
        $db_ct  = $this->db->query("SELECT `grgl_postsmeta`.*, `grgl_category`.`ctr_name` FROM `grgl_postsmeta` LEFT JOIN `grgl_category` ON `grgl_postsmeta`.`meta_value` = `grgl_category`.`ctr_id` WHERE `grgl_postsmeta`.`pst_id` = $id AND `grgl_postsmeta`.`meta_key` = '_post_category_'")->result();

        $i = 0;
        $len = count($db_ct);
        if ($len != 0) {
            foreach ($db_ct as $pmTemp) {
                if ($i == $len - 1) {
                    $cat .= $pmTemp->ctr_name;
                } else {
                    $cat .= $pmTemp->ctr_name . ", ";
                }
                $i++;
            }
        } else {
            $cat = "Uncategorized";
        }
        return $cat;
    }

    function pm_post_perslug($slug) 
    {
        $pCat = array();
        $db_ct  = $this->db->query("SELECT `grgl_postsmeta`.* FROM `grgl_postsmeta` LEFT JOIN `grgl_posts` ON `grgl_postsmeta`.`pst_id` = `grgl_posts`.`pst_id` WHERE `grgl_posts`.`pst_slug` = '$slug' AND `grgl_postsmeta`.meta_key = '_post_category_'");

        if ($db_ct->num_rows() > 0) {
            foreach ($db_ct->result() as $catval) {
                array_push($pCat, $catval->meta_value);
            }
        } else {
            $pCat = "";
        }

        $source = $this->db->query("SELECT `grgl_postsmeta`.* FROM `grgl_postsmeta` LEFT JOIN `grgl_posts` ON `grgl_postsmeta`.`pst_id` = `grgl_posts`.`pst_id` WHERE `grgl_posts`.`pst_slug` = '$slug' AND `meta_key` = '_post_source_'");

        if ($source->num_rows() > 0) {
            $source  = $source->row(); 
            $pSource = $source->meta_value;
        } else {
            $pSource = "";
        }
        
        $tags = $this->db->query("SELECT `grgl_postsmeta`.* FROM `grgl_postsmeta` LEFT JOIN `grgl_posts` ON `grgl_postsmeta`.`pst_id` = `grgl_posts`.`pst_id` WHERE `grgl_posts`.`pst_slug` = '$slug' AND `meta_key` = '_post_tags_'");

        if ($tags->num_rows() > 0) {
            $tags  = $tags->row(); 
            $pTags = $tags->meta_value;
        } else {
            $pTags = "";
        }

        return array('category' => $pCat, 'source' => $pSource, 'tags' => $pTags);
    }

    function p_get_publish()
    {   
        $cat;
        $pm_ = (object) array();
        $pmTemp_ = array();
        $db = $this->db->query("SELECT `grgl_posts`.* FROM `grgl_posts` WHERE `grgl_posts`.pst_status = 'publish' ORDER BY `grgl_posts`.`pst_id` DESC");

        if ($db->num_rows() != 0) {
            foreach ($db->result() as $data) {
                $id  = $data->pst_id;
                $cat = $this->pm_get_catbyId($id);
                array_push($pmTemp_, array(
                    'count'=>$db->num_rows(),
                    'pst_id'=>$id,
                    'pst_head'=>$data->pst_head,
                    'pst_slug'=>$data->pst_slug,
                    'pst_status'=>$data->pst_status,
                    'pst_txt'=>$data->pst_txt,
                    'pst_guid'=>$data->pst_guid,
                    'created_by'=>$data->created_by,
                    'modified_by'=>$data->modified_by,
                    'created_date'=>$data->created_date,
                    'category'=>$cat,
                ));
            }
        } else {
            array_push($pmTemp_, array(
                'count'=>0,
            ));
        }
        $pm_ = $pmTemp_;
        return $pm_;
    }

    function p_get_all()
    {   
        $cat;
        $pm_ = (object) array();
        $pmTemp_ = array();
        $db = $this->db->query("SELECT `grgl_posts`.* FROM `grgl_posts` WHERE `grgl_posts`.pst_status = 'publish' OR `grgl_posts`.pst_status = 'draft' ORDER BY `grgl_posts`.`created_date` DESC");

        if ($db->num_rows() != 0) {
            foreach ($db->result() as $data) {
                $id  = $data->pst_id;
                $cat = $this->pm_get_catbyId($id);
                array_push($pmTemp_, array(
                    'count'=>$db->num_rows(),
                    'pst_id'=>$id,
                    'pst_head'=>$data->pst_head,
                    'pst_slug'=>$data->pst_slug,
                    'pst_status'=>$data->pst_status,
                    'pst_txt'=>$data->pst_txt,
                    'created_by'=>$data->created_by,
                    'modified_by'=>$data->modified_by,
                    'created_date'=>$data->created_date,
                    'category'=>$cat,
                ));
            }
        } else {
            array_push($pmTemp_, array(
                'count'=>0,
            ));
        }
        $pm_ = $pmTemp_;
        return $pm_;
    }

    function p_get_slug($slug)
    {
        return $this->db->get_where($this->p_table,array('pst_slug =' => $slug));
    }

    function p_get_filter($w1, $w2)
    {
        $cat;
        $pm_ = (object) array();
        $pmTemp_ = array();

        if ($w1 == "" AND $w2 == "") {
            $db = $this->db->query("SELECT `grgl_posts`.* FROM `grgl_posts` WHERE `grgl_posts`.pst_status = 'publish' OR `grgl_posts`.pst_status = 'draft' ORDER BY `grgl_posts`.`created_date` DESC");
        } else if ($w1 != "" AND $w2 == "") {
            $db = $this->db->query("SELECT `grgl_posts`.* FROM `grgl_posts` LEFT JOIN `grgl_postsmeta` ON `grgl_posts`.pst_id = `grgl_postsmeta`.pst_id WHERE `grgl_postsmeta`.meta_key = '_post_category_' AND `grgl_postsmeta`.meta_value = $w1 AND (`grgl_posts`.pst_status = 'publish' OR `grgl_posts`.pst_status = 'draft') GROUP BY `grgl_posts`.pst_id ORDER BY `grgl_posts`.`created_date` DESC");
        } else if ($w1 == "" AND $w2 != "") {
            $db = $this->db->query("SELECT `grgl_posts`.* FROM `grgl_posts` WHERE `grgl_posts`.pst_status = '$w2' ORDER BY `grgl_posts`.`created_date` DESC");
        } else {
            $db = $this->db->query("SELECT `grgl_posts`.* FROM `grgl_posts` LEFT JOIN `grgl_postsmeta` ON `grgl_posts`.pst_id = `grgl_postsmeta`.pst_id WHERE `grgl_posts`.pst_status = '$w2' AND `grgl_postsmeta`.meta_key = '_post_category_' AND `grgl_postsmeta`.meta_value = $w1 GROUP BY `grgl_posts`.pst_id ORDER BY `grgl_posts`.`created_date` DESC");
        }

        if ($db->num_rows() != 0) {
            foreach ($db->result() as $data) {
                $id  = $data->pst_id;
                $cat = $this->pm_get_catbyId($id);
                array_push($pmTemp_, array(
                    'count'=>$db->num_rows(),
                    'pst_id'=>$id,
                    'pst_head'=>$data->pst_head,
                    'pst_slug'=>$data->pst_slug,
                    'pst_status'=>$data->pst_status,
                    'pst_txt'=>$data->pst_txt,
                    'created_by'=>$data->created_by,
                    'modified_by'=>$data->modified_by,
                    'created_date'=>$data->created_date,
                    'category'=>$cat,
                ));
            }
        } else {
            array_push($pmTemp_, array(
                'count'=>0,
            ));
        }
        $pm_ = $pmTemp_;
        return $pm_;
    }

    function p_insert()
    {
        $data['pst_head']    = $this->input->post('title');
        $data['pst_slug']    = slugify($data['pst_head']);
        $data['pst_txt']     = $this->input->post('text');
        $data['pst_status']  = $this->input->post('submit');
        $data['pst_guid']    = base_url().'article/'.$data['pst_slug'].'/';
        $data['created_by']  = $this->session->userdata['uNick'];
        $data['modified_by'] = $this->session->userdata['uNick'];
        $upload              = 'true';
        $source              = $this->input->post('source');
        $tags                = $this->input->post('tags');

        $category = $this->input->post('category');
        if ($category != "") {
            $data['pst_cat'] = count($this->input->post('category'));
        } else {
            $data['pst_cat'] = 0;
        }
        $pm_ = array();

        if ($data['modified_by'] != "") {
            $this->db->trans_start(); # Starting Transaction
            $this->db->insert($this->p_table, $data); # Inserting data
            $id = $this->db->insert_id();
            if ($data['pst_cat'] != 0) {
                foreach($category as $cat){
                    array_push($pm_, array(
                        'pst_id'=>$id,
                        'meta_key'=>'_post_category_',
                        'meta_value'=>$cat,
                    ));
                }
                # insert multiple category
                $this->db->insert_batch('grgl_postsmeta', $pm_);
            }

            if ($source != "") {
                $pm_ = array('pst_id'=>$id, 'meta_key'=>'_post_source_', 'meta_value'=>$source);
                $this->db->insert('grgl_postsmeta', $pm_); # Inserting source
            }

            if ($tags != "") {
                $pm_ = array('pst_id'=>$id, 'meta_key'=>'_post_tags_', 'meta_value'=>$tags);
                $this->db->insert('grgl_postsmeta', $pm_); # Inserting tags
            }

            if (!empty($_FILES['file-one']['name']) && $upload == 'true') {
                $filename = 'featured-1-'.$id.'-'.$data['pst_slug'];
                $image = $this->file_upload($filename, 'file-one');
                if ($image['status'] == 'UPL_FALSE') {
                    $ret    = $image;
                    $upload = 'false';
                } else {
                    $img['img_name']      = $image['data'];
                    $img['img_guid']      = base_url().'attachments/'.$image['data'];
                    $img['img_mime_type'] = $image['type'];
                    $img['created_by']    = $this->session->userdata['uNick'];
                    $img['modified_by']   = $this->session->userdata['uNick'];

                    if ($this->db->insert('grgl_img', $img)) {
                        $id_img = $this->db->insert_id();

                        $imeta['img_id'] = $id_img;
                        $imeta['meta_key'] = '_feature_img_';
                        $imeta['meta_value'] = '1';

                        if ($this->db->insert('grgl_imgmeta', $imeta)) {
                            $imeta['img_id'] = $id_img;
                            $imeta['meta_key'] = '_img_post_';
                            $imeta['meta_value'] = $id;
                            $this->db->insert('grgl_imgmeta', $imeta);
                        }
                    }
                }
            }

            if (!empty($_FILES['file-two']['name']) && $upload == 'true') {
                $filename2 = 'featured-2-'.$id.'-'.$data['pst_slug'];
                $image2 = $this->file_upload($filename2, 'file-two');
                if ($image2['status'] == 'UPL_FALSE') {
                    $ret    = $image2;
                    $upload = 'false';
                } else {
                    $img2['img_name']      = $image2['data'];
                    $img2['img_guid']      = base_url().'attachments/'.$image2['data'];
                    $img2['img_mime_type'] = $image2['type'];
                    $img2['created_by']    = $this->session->userdata['uNick'];
                    $img2['modified_by']   = $this->session->userdata['uNick'];

                    if ($this->db->insert('grgl_img', $img2)) {
                        $id_img2 = $this->db->insert_id();

                        $imeta2['img_id'] = $id_img2;
                        $imeta2['meta_key'] = '_feature_img_';
                        $imeta2['meta_value'] = '2';

                        if ($this->db->insert('grgl_imgmeta', $imeta2)) {
                            $imeta2['img_id'] = $id_img2;
                            $imeta2['meta_key'] = '_img_post_';
                            $imeta2['meta_value'] = $id;
                            $this->db->insert('grgl_imgmeta', $imeta2);
                        }
                    }
                }
            }

            // FILE UPLOAD 3
            if (!empty($_FILES['file-three']['name']) && $upload == 'true') {
                $filename3 = 'featured-3-'.$id.'-'.$data['pst_slug'];
                $image3 = $this->file_upload($filename3, 'file-three');
                if ($image3['status'] == 'UPL_FALSE') {
                    $ret    = $image3;
                    $upload = 'false';
                } else {
                    $img3['img_name']      = $image3['data'];
                    $img3['img_guid']      = base_url().'attachments/'.$image3['data'];
                    $img3['img_mime_type'] = $image3['type'];
                    $img3['created_by']    = $this->session->userdata['uNick'];
                    $img3['modified_by']   = $this->session->userdata['uNick'];

                    if ($this->db->insert('grgl_img', $img3)) {
                        $id_img3 = $this->db->insert_id();

                        $imeta3['img_id'] = $id_img3;
                        $imeta3['meta_key'] = '_feature_img_';
                        $imeta3['meta_value'] = '3';

                        if ($this->db->insert('grgl_imgmeta', $imeta3)) {
                            $imeta3['img_id'] = $id_img3;
                            $imeta3['meta_key'] = '_img_post_';
                            $imeta3['meta_value'] = $id;
                            $this->db->insert('grgl_imgmeta', $imeta3);
                        }
                    }
                }
            } // END FILE UPLOAD 3

                // FILE UPLOAD 4
            if (!empty($_FILES['file-four']['name']) && $upload == 'true') {
                $filename4 = 'featured-4-'.$id.'-'.$data['pst_slug'];
                $image4 = $this->file_upload($filename4, 'file-four');
                if ($image4['status'] == 'UPL_FALSE') {
                    $ret    = $image4;
                    $upload = 'false';
                } else {
                    $img4['img_name']      = $image4['data'];
                    $img4['img_guid']      = base_url().'attachments/'.$image4['data'];
                    $img4['img_mime_type'] = $image4['type'];
                    $img4['created_by']    = $this->session->userdata['uNick'];
                    $img4['modified_by']   = $this->session->userdata['uNick'];

                    if ($this->db->insert('grgl_img', $img4)) {
                        $id_img4 = $this->db->insert_id();

                        $imeta4['img_id'] = $id_img4;
                        $imeta4['meta_key'] = '_feature_img_';
                        $imeta4['meta_value'] = '4';

                        if ($this->db->insert('grgl_imgmeta', $imeta4)) {
                            $imeta4['img_id'] = $id_img4;
                            $imeta4['meta_key'] = '_img_post_';
                            $imeta4['meta_value'] = $id;
                            $this->db->insert('grgl_imgmeta', $imeta4);
                        }
                    }
                }
            } // END FILE UPLOAD 4

            // FILE UPLOAD 5
            if (!empty($_FILES['file-five']['name']) && $upload == 'true') {
                $filename5 = 'featured-5-'.$id.'-'.$data['pst_slug'];
                $image5 = $this->file_upload($filename5, 'file-five');
                if ($image5['status'] == 'UPL_FALSE') {
                    $ret    = $image5;
                    $upload = 'false';
                } else {
                    $img5['img_name']      = $image5['data'];
                    $img5['img_guid']      = base_url().'attachments/'.$image5['data'];
                    $img5['img_mime_type'] = $image5['type'];
                    $img5['created_by']    = $this->session->userdata['uNick'];
                    $img5['modified_by']   = $this->session->userdata['uNick'];

                    if ($this->db->insert('grgl_img', $img5)) {
                        $id_img5 = $this->db->insert_id();

                        $imeta5['img_id'] = $id_img5;
                        $imeta5['meta_key'] = '_feature_img_';
                        $imeta5['meta_value'] = '5';

                        if ($this->db->insert('grgl_imgmeta', $imeta5)) {
                            $imeta5['img_id'] = $id_img5;
                            $imeta5['meta_key'] = '_img_post_';
                            $imeta5['meta_value'] = $id;
                            $this->db->insert('grgl_imgmeta', $imeta5);
                        }
                    }
                }
            } // END FILE UPLOAD 5

            // UPLOAD FAILED EXCEPTION
            if ($upload == 'false') {
                if (!empty($image['path'])) {
                    $a = file_exists($image['path']);
                    if ($a > 0) {
                        unlink($image['path']);
                    }
                }

                if (!empty($image2['path'])) {
                    $a = file_exists($image2['path']);
                    if ($a > 0) {
                        unlink($image2['path']);
                    }
                }

                if (!empty($image3['path'])) {
                    $a = file_exists($image3['path']);
                    if ($a > 0) {
                        unlink($image3['path']);
                    }
                }

                if (!empty($image4['path'])) {
                    $a = file_exists($image4['path']);
                    if ($a > 0) {
                        unlink($image4['path']);
                    }
                }

                if (!empty($image5['path'])) {
                    $a = file_exists($image5['path']);
                    if ($a > 0) {
                        unlink($image5['path']);
                    }
                }
                $this->db->trans_rollback();
                return $ret;
            }


            /*Optional*/
            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
                if (!empty($image['path'])) {
                    $a = file_exists($image['path']);
                    if ($a > 0) {
                        unlink($image['path']);
                    }
                }

                if (!empty($image2['path'])) {
                    $a = file_exists($image2['path']);
                    if ($a > 0) {
                        unlink($image2['path']);
                    }
                }

                if (!empty($image3['path'])) {
                    $a = file_exists($image3['path']);
                    if ($a > 0) {
                        unlink($image3['path']);
                    }
                }

                if (!empty($image4['path'])) {
                    $a = file_exists($image4['path']);
                    if ($a > 0) {
                        unlink($image4['path']);
                    }
                }

                if (!empty($image5['path'])) {
                    $a = file_exists($image5['path']);
                    if ($a > 0) {
                        unlink($image5['path']);
                    }
                }

                $ret = array( 'status' => 'TRS_FALSE' );
                return $ret;
            } else {
                # Everything is Perfect.
                # Committing data to the database.
                $this->db->trans_commit();
                $ret = array( 'status' => 'TRS_TRUE' );
                return $ret;
            }

            $this->db->trans_complete(); # Completing transaction
        } else {
            $ret = array( 'status' => 'NO_AUTH' );
            return $ret;
        }
    }

    function p_update($old_slug)
    {
        $data['pst_head']    = $this->input->post('title');
        $data['pst_slug']    = slugify($data['pst_head']);
        $data['pst_txt']     = $this->input->post('text');
        $data['pst_status']  = $this->input->post('submit');
        $data['pst_guid']    = base_url().'article/'.$data['pst_slug'].'/';
        $data['created_by']  = $this->session->userdata['uNick'];
        $data['modified_by'] = $this->session->userdata['uNick'];
        $modified            = $data['modified_by'];
        $upload              = 'true';
        $source              = $this->input->post('source');
        $tags                = $this->input->post('tags');

        $category = $this->input->post('category');
        if ($category != "") {
            $data['pst_cat'] = count($this->input->post('category'));
        } else {
            $data['pst_cat'] = 0;
        }
        $pm_ = array();

        if ($data['modified_by'] != "" AND $old_slug != "") {
            $this->db->trans_start(); # Starting Transaction

            $old_post = $this->p_get_slug($old_slug)->row();
            $dt_fimg  = $this->get_fimg_bypost($old_slug);
            $tmp_slug = $old_slug.'-revision';
            $this->db->like('pst_slug', $tmp_slug, 'after');
            $pget = $this->db->get($this->p_table);

            if ($pget->num_rows() == 0) {
                $new_slug   = $tmp_slug;
                $guid       = base_url().'article/'.$new_slug.'/';
            } else {
                $pget       = $pget->num_rows() + 1; 
                $new_slug   = $old_slug.'-revision-'.$pget;
                $guid       = base_url().'article/'.$new_slug.'/';
            }

            $this->db->query("UPDATE `grgl_posts` SET `pst_slug` = '$new_slug', `pst_status` = 'revision', `pst_guid` = '$guid', `modified_by` = '$modified' WHERE `pst_slug` = '$old_slug'");

            $p_slug = $this->p_get_slug($data['pst_slug'])->num_rows();
            if ($p_slug > 0) {
                $this->db->trans_rollback();
                $ret = array( 'status' => 'SLG_FAIL' );
                return $ret;
            } else {  // SLUG CHECK ELSE
                $data['created_date'] = $old_post->created_date;
                $this->db->insert($this->p_table, $data); # Inserting data
                $id = $this->db->insert_id();
                if ($data['pst_cat'] != 0) {
                    foreach($category as $cat){
                        array_push($pm_, array(
                            'pst_id'=>$id,
                            'meta_key'=>'_post_category_',
                            'meta_value'=>$cat,
                        ));
                    }
                    # insert multiple category
                    $this->db->insert_batch('grgl_postsmeta', $pm_);
                }

                if ($source != "") {
                    $pm_ = array('pst_id'=>$id, 'meta_key'=>'_post_source_', 'meta_value'=>$source);
                    $this->db->insert('grgl_postsmeta', $pm_); # Inserting source
                }

                if ($tags != "") {
                    $pm_ = array('pst_id'=>$id, 'meta_key'=>'_post_tags_', 'meta_value'=>$tags);
                    $this->db->insert('grgl_postsmeta', $pm_); # Inserting tags   
                }

                // FILE UPLOAD 1
                if (!empty($_FILES['file-one']['name']) && $this->input->post('in-fone') != "" && $upload == 'true') {
                    $filename = 'featured-1-'.$id.'-'.$data['pst_slug'];
                    $image = $this->file_upload($filename, 'file-one');
                    if ($image['status'] == 'UPL_FALSE') {
                        $ret    = $image;
                        $upload = 'false';
                    } else {
                        $img['img_name']      = $image['data'];
                        $img['img_guid']      = base_url().'attachments/'.$image['data'];
                        $img['img_mime_type'] = $image['type'];
                        $img['created_by']    = $this->session->userdata['uNick'];
                        $img['modified_by']   = $this->session->userdata['uNick'];

                        if ($this->db->insert('grgl_img', $img)) {
                            $id_img = $this->db->insert_id();

                            $imeta['img_id'] = $id_img;
                            $imeta['meta_key'] = '_feature_img_';
                            $imeta['meta_value'] = '1';

                            if ($this->db->insert('grgl_imgmeta', $imeta)) {
                                $imeta['img_id'] = $id_img;
                                $imeta['meta_key'] = '_img_post_';
                                $imeta['meta_value'] = $id;
                                $this->db->insert('grgl_imgmeta', $imeta);
                            }
                        }
                    }
                } else if (empty($_FILES['file-one']['name']) && $this->input->post('in-fone') != "") {
                    if ($dt_fimg[0]['ifimg1'] != "") {
                        $imeta['img_id'] = $dt_fimg[0]['ifimg1'];
                        $imeta['meta_key'] = '_feature_img_';
                        $imeta['meta_value'] = '1';

                        if ($this->db->insert('grgl_imgmeta', $imeta)) {
                            $imeta['img_id'] = $dt_fimg[0]['ifimg1'];
                            $imeta['meta_key'] = '_img_post_';
                            $imeta['meta_value'] = $id;
                            $this->db->insert('grgl_imgmeta', $imeta);
                        }
                    }
                } // END FILE UPLOAD 1

                // FILE UPLOAD 2
                if (!empty($_FILES['file-two']['name']) && $this->input->post('in-ftwo') != "" && $upload == 'true') {
                    $filename2 = 'featured-2-'.$id.'-'.$data['pst_slug'];
                    $image2 = $this->file_upload($filename2, 'file-two');
                    if ($image2['status'] == 'UPL_FALSE') {
                        $ret    = $image2;
                        $upload = 'false';
                    } else {
                        $img2['img_name']      = $image2['data'];
                        $img2['img_guid']      = base_url().'attachments/'.$image2['data'];
                        $img2['img_mime_type'] = $image2['type'];
                        $img2['created_by']    = $this->session->userdata['uNick'];
                        $img2['modified_by']   = $this->session->userdata['uNick'];

                        if ($this->db->insert('grgl_img', $img2)) {
                            $id_img2 = $this->db->insert_id();

                            $imeta2['img_id'] = $id_img2;
                            $imeta2['meta_key'] = '_feature_img_';
                            $imeta2['meta_value'] = '2';

                            if ($this->db->insert('grgl_imgmeta', $imeta2)) {
                                $imeta2['img_id'] = $id_img2;
                                $imeta2['meta_key'] = '_img_post_';
                                $imeta2['meta_value'] = $id;
                                $this->db->insert('grgl_imgmeta', $imeta2);
                            }
                        }
                    }
                } else if (empty($_FILES['file-two']['name']) && $this->input->post('in-ftwo') != "") {
                    if ($dt_fimg[0]['ifimg2'] != "") {
                        $imeta2['img_id'] = $dt_fimg[0]['ifimg2'];
                        $imeta2['meta_key'] = '_feature_img_';
                        $imeta2['meta_value'] = '2';

                        if ($this->db->insert('grgl_imgmeta', $imeta2)) {
                            $imeta2['img_id'] = $dt_fimg[0]['ifimg2'];
                            $imeta2['meta_key'] = '_img_post_';
                            $imeta2['meta_value'] = $id;
                            $this->db->insert('grgl_imgmeta', $imeta2);
                        }
                    }
                } // END FILE UPLOAD 2

                // FILE UPLOAD 3
                if (!empty($_FILES['file-three']['name']) && $this->input->post('in-fthree') != "" && $upload == 'true') {
                    $filename3 = 'featured-3-'.$id.'-'.$data['pst_slug'];
                    $image3 = $this->file_upload($filename3, 'file-three');
                    if ($image3['status'] == 'UPL_FALSE') {
                        $ret    = $image3;
                        $upload = 'false';
                    } else {
                        $img3['img_name']      = $image3['data'];
                        $img3['img_guid']      = base_url().'attachments/'.$image3['data'];
                        $img3['img_mime_type'] = $image3['type'];
                        $img3['created_by']    = $this->session->userdata['uNick'];
                        $img3['modified_by']   = $this->session->userdata['uNick'];

                        if ($this->db->insert('grgl_img', $img3)) {
                            $id_img3 = $this->db->insert_id();

                            $imeta3['img_id'] = $id_img3;
                            $imeta3['meta_key'] = '_feature_img_';
                            $imeta3['meta_value'] = '3';

                            if ($this->db->insert('grgl_imgmeta', $imeta3)) {
                                $imeta3['img_id'] = $id_img3;
                                $imeta3['meta_key'] = '_img_post_';
                                $imeta3['meta_value'] = $id;
                                $this->db->insert('grgl_imgmeta', $imeta3);
                            }
                        }
                    }
                } else if (empty($_FILES['file-three']['name']) && $this->input->post('in-fthree') != "") {
                    if ($dt_fimg[0]['ifimg3'] != "") {
                        $imeta3['img_id'] = $dt_fimg[0]['ifimg3'];
                        $imeta3['meta_key'] = '_feature_img_';
                        $imeta3['meta_value'] = '3';

                        if ($this->db->insert('grgl_imgmeta', $imeta3)) {
                            $imeta3['img_id'] = $dt_fimg[0]['ifimg3'];
                            $imeta3['meta_key'] = '_img_post_';
                            $imeta3['meta_value'] = $id;
                            $this->db->insert('grgl_imgmeta', $imeta3);
                        }
                    }
                } // END FILE UPLOAD 3

                // FILE UPLOAD 4
                if (!empty($_FILES['file-four']['name']) && $this->input->post('in-ffour') != "" && $upload == 'true') {
                    $filename4 = 'featured-4-'.$id.'-'.$data['pst_slug'];
                    $image4 = $this->file_upload($filename4, 'file-four');
                    if ($image4['status'] == 'UPL_FALSE') {
                        $ret    = $image4;
                        $upload = 'false';
                    } else {
                        $img4['img_name']      = $image4['data'];
                        $img4['img_guid']      = base_url().'attachments/'.$image4['data'];
                        $img4['img_mime_type'] = $image4['type'];
                        $img4['created_by']    = $this->session->userdata['uNick'];
                        $img4['modified_by']   = $this->session->userdata['uNick'];

                        if ($this->db->insert('grgl_img', $img4)) {
                            $id_img4 = $this->db->insert_id();

                            $imeta4['img_id'] = $id_img4;
                            $imeta4['meta_key'] = '_feature_img_';
                            $imeta4['meta_value'] = '4';

                            if ($this->db->insert('grgl_imgmeta', $imeta4)) {
                                $imeta4['img_id'] = $id_img4;
                                $imeta4['meta_key'] = '_img_post_';
                                $imeta4['meta_value'] = $id;
                                $this->db->insert('grgl_imgmeta', $imeta4);
                            }
                        }
                    }
                } else if (empty($_FILES['file-four']['name']) && $this->input->post('in-ffour') != "") {
                    if ($dt_fimg[0]['ifimg4'] != "") {
                        $imeta4['img_id'] = $dt_fimg[0]['ifimg4'];
                        $imeta4['meta_key'] = '_feature_img_';
                        $imeta4['meta_value'] = '4';

                        if ($this->db->insert('grgl_imgmeta', $imeta4)) {
                            $imeta4['img_id'] = $dt_fimg[0]['ifimg4'];
                            $imeta4['meta_key'] = '_img_post_';
                            $imeta4['meta_value'] = $id;
                            $this->db->insert('grgl_imgmeta', $imeta4);
                        }
                    }
                } // END FILE UPLOAD 4

                // FILE UPLOAD 5
                if (!empty($_FILES['file-five']['name']) && $this->input->post('in-ffive') != "" && $upload == 'true') {
                    $filename5 = 'featured-5-'.$id.'-'.$data['pst_slug'];
                    $image5 = $this->file_upload($filename5, 'file-five');
                    if ($image5['status'] == 'UPL_FALSE') {
                        $ret    = $image5;
                        $upload = 'false';
                    } else {
                        $img5['img_name']      = $image5['data'];
                        $img5['img_guid']      = base_url().'attachments/'.$image5['data'];
                        $img5['img_mime_type'] = $image5['type'];
                        $img5['created_by']    = $this->session->userdata['uNick'];
                        $img5['modified_by']   = $this->session->userdata['uNick'];

                        if ($this->db->insert('grgl_img', $img5)) {
                            $id_img5 = $this->db->insert_id();

                            $imeta5['img_id'] = $id_img5;
                            $imeta5['meta_key'] = '_feature_img_';
                            $imeta5['meta_value'] = '5';

                            if ($this->db->insert('grgl_imgmeta', $imeta5)) {
                                $imeta5['img_id'] = $id_img5;
                                $imeta5['meta_key'] = '_img_post_';
                                $imeta5['meta_value'] = $id;
                                $this->db->insert('grgl_imgmeta', $imeta5);
                            }
                        }
                    }
                } else if (empty($_FILES['file-five']['name']) && $this->input->post('in-ffive') != "") {
                    if ($dt_fimg[0]['ifimg5'] != "") {
                        $imeta5['img_id'] = $dt_fimg[0]['ifimg5'];
                        $imeta5['meta_key'] = '_feature_img_';
                        $imeta5['meta_value'] = '5';

                        if ($this->db->insert('grgl_imgmeta', $imeta5)) {
                            $imeta5['img_id'] = $dt_fimg[0]['ifimg5'];
                            $imeta5['meta_key'] = '_img_post_';
                            $imeta5['meta_value'] = $id;
                            $this->db->insert('grgl_imgmeta', $imeta5);
                        }
                    }
                } // END FILE UPLOAD 5

                // UPLOAD FAILED EXCEPTION
                if ($upload == 'false') {
                    if (!empty($image['path'])) {
                        $a = file_exists($image['path']);
                        if ($a > 0) {
                            unlink($image['path']);
                        }
                    }

                    if (!empty($image2['path'])) {
                        $a = file_exists($image2['path']);
                        if ($a > 0) {
                            unlink($image2['path']);
                        }
                    }

                    if (!empty($image3['path'])) {
                        $a = file_exists($image3['path']);
                        if ($a > 0) {
                            unlink($image3['path']);
                        }
                    }

                    if (!empty($image4['path'])) {
                        $a = file_exists($image4['path']);
                        if ($a > 0) {
                            unlink($image4['path']);
                        }
                    }

                    if (!empty($image5['path'])) {
                        $a = file_exists($image5['path']);
                        if ($a > 0) {
                            unlink($image5['path']);
                        }
                    }
                    $this->db->trans_rollback();
                    return $ret;
                }

            }  // END SLUG CHECK ELSE

            /*Optional*/
            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
                if (!empty($image['path'])) {
                    $a = file_exists($image['path']);
                    if ($a > 0) {
                        unlink($image['path']);
                    }
                }

                if (!empty($image2['path'])) {
                    $a = file_exists($image2['path']);
                    if ($a > 0) {
                        unlink($image2['path']);
                    }
                }

                if (!empty($image3['path'])) {
                    $a = file_exists($image3['path']);
                    if ($a > 0) {
                        unlink($image3['path']);
                    }
                }

                if (!empty($image4['path'])) {
                    $a = file_exists($image4['path']);
                    if ($a > 0) {
                        unlink($image4['path']);
                    }
                }

                if (!empty($image5['path'])) {
                    $a = file_exists($image5['path']);
                    if ($a > 0) {
                        unlink($image5['path']);
                    }
                }

                $ret = array( 'status' => 'TRS_FALSE' );
                return $ret;
            } else {
                # Everything is Perfect.
                # Committing data to the database.
                $this->db->trans_commit();
                $ret = array( 'status' => 'TRS_TRUE' );
                return $ret;
            }

            $this->db->trans_complete(); # Completing transaction 
        } else {
            $ret = array( 'status' => 'NO_AUTH' );
            return $ret;
        }
    }

    function p_delete()
    {
        $new_slug    = "";
        $slug        = $this->input->post('f_pdel');
        $tmp_slug    = $this->input->post('f_pdel').'-delete';
        $status      = 'delete';
        $modified_by = $this->session->userdata['uNick'];

        $this->db->like('pst_slug', $tmp_slug, 'after');
        $p_slug = $this->db->get($this->p_table);

        if ($p_slug->num_rows() == 0) {
            $new_slug   = $this->input->post('f_pdel').'-delete';
        } else {
            $p_slug     = $p_slug->num_rows() + 1; 
            $new_slug   = $this->input->post('f_pdel').'-delete-'.$p_slug;
        }

        $guid = base_url().'article/'.$new_slug.'/';

        if ($modified_by == "" OR $new_slug == "") {
            $code = '400';
            return $code;
        } else {

            $get = $this->p_get_slug($slug)->row();
            
            $img_ = $this->db->query("SELECT I.`img_id`, I.`img_name`, I.`img_guid` FROM `grgl_img` I LEFT JOIN `grgl_imgmeta` IM ON I.`img_id` = IM.`img_id` WHERE IM.`meta_key` = '_img_post_' AND IM.`meta_value` = $get->pst_id GROUP BY I.`img_id`");

            $this->db->trans_start(); # Starting Transaction
            $this->db->query("UPDATE `grgl_posts` SET `pst_slug` = '$new_slug', `pst_status` = '$status', `pst_guid` = '$guid', `modified_by` = '$modified_by' WHERE `pst_slug` = '$slug'");

            if ($img_->num_rows() > 0) {
                $img_ = $img_->result();
                foreach ( $img_ as $img) {
                    $iname = $img->img_name;
                    $iguid = './attachments/'.$iname;
                    $nguid = base_url().'attachments/deleted/'.$iname;

                    if ($this->db->query("UPDATE `grgl_img` SET `img_guid` = '$nguid', `modified_by` = '$modified_by' WHERE `img_id` = $img->img_id")) {
                        $a = file_exists($iguid);
                        if ($a > 0) {
                            rename('./attachments/'.$iname, './attachments/deleted/'.$iname);                         
                        }
                    }
                }
            }

            /*Optional*/
            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
                return 400;
            } else {
                # Everything is Perfect.
                # Committing data to the database.
                $this->db->trans_commit();
                return 200;
            }

            $this->db->trans_complete(); # Completing transaction
        }
    }

    function file_upload($filename, $file)
    {
        $config['upload_path']      = './attachments/';
        $config['allowed_types']    = 'gif|jpg|png|JPG|jpeg';
        $config['file_name']        = $filename;
        $config['overwrite']        = true;
        $config['max_size']         = 3072; // 3MB

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->upload->do_upload($file)) {
            $path  = './attachments/'.$this->upload->data("file_name");
            $ret = array( 'status' => 'UPL_TRUE', 'path' => $path, 'data' => $this->upload->data('file_name'), 'type' => $this->upload->data('file_type'), 'file' => $this->upload->data() );
            return $ret;
        } else {
            $ret = array( 'status' => 'UPL_FALSE', 'error' => $this->upload->display_errors() );
            return $ret;
        }
    }

    function get_source_bypost($slug) {
        $post   = $this->p_get_slug($slug)->row();
        $source = $this->db->query("SELECT * FROM `grgl_postsmeta` WHERE `pst_id` = $post->pst_id AND `meta_key` = '_post_source_'");

        return $source->row();
    }

    function get_fimg_bypost($slug) {
        $post  = $this->p_get_slug($slug)->row();
        $fimg1 = $this->db->query("SELECT I.`img_id`, I.`img_name`, I.`img_guid` FROM `grgl_img` I LEFT JOIN `grgl_imgmeta` IM ON I.`img_id` = IM.`img_id` LEFT JOIN `grgl_imgmeta` IMM ON IM.`img_id` = IMM.`img_id` WHERE IM.`meta_key` = '_img_post_' AND IM.`meta_value` = $post->pst_id AND IMM.`meta_key`='_feature_img_' AND IMM.`meta_value` = 1 GROUP BY I.`img_id`");
        $fimg2 = $this->db->query("SELECT I.`img_id`, I.`img_name`, I.`img_guid` FROM `grgl_img` I LEFT JOIN `grgl_imgmeta` IM ON I.`img_id` = IM.`img_id` LEFT JOIN `grgl_imgmeta` IMM ON IM.`img_id` = IMM.`img_id` WHERE IM.`meta_key` = '_img_post_' AND IM.`meta_value` = $post->pst_id AND IMM.`meta_key`='_feature_img_' AND IMM.`meta_value` = 2 GROUP BY I.`img_id`");
        $fimg3 = $this->db->query("SELECT I.`img_id`, I.`img_name`, I.`img_guid` FROM `grgl_img` I LEFT JOIN `grgl_imgmeta` IM ON I.`img_id` = IM.`img_id` LEFT JOIN `grgl_imgmeta` IMM ON IM.`img_id` = IMM.`img_id` WHERE IM.`meta_key` = '_img_post_' AND IM.`meta_value` = $post->pst_id AND IMM.`meta_key`='_feature_img_' AND IMM.`meta_value` = 3 GROUP BY I.`img_id`");
        $fimg4 = $this->db->query("SELECT I.`img_id`, I.`img_name`, I.`img_guid` FROM `grgl_img` I LEFT JOIN `grgl_imgmeta` IM ON I.`img_id` = IM.`img_id` LEFT JOIN `grgl_imgmeta` IMM ON IM.`img_id` = IMM.`img_id` WHERE IM.`meta_key` = '_img_post_' AND IM.`meta_value` = $post->pst_id AND IMM.`meta_key`='_feature_img_' AND IMM.`meta_value` = 4 GROUP BY I.`img_id`");
        $fimg5 = $this->db->query("SELECT I.`img_id`, I.`img_name`, I.`img_guid` FROM `grgl_img` I LEFT JOIN `grgl_imgmeta` IM ON I.`img_id` = IM.`img_id` LEFT JOIN `grgl_imgmeta` IMM ON IM.`img_id` = IMM.`img_id` WHERE IM.`meta_key` = '_img_post_' AND IM.`meta_value` = $post->pst_id AND IMM.`meta_key`='_feature_img_' AND IMM.`meta_value` = 5 GROUP BY I.`img_id`");

        if ($fimg1->num_rows() > 0) {
            $fimg1      = $fimg1->row();
            $fimg1_name = $fimg1->img_name;
            $fimg1_id   = $fimg1->img_id;
        } else {
            $fimg1_name = "";
            $fimg1_id   = "";
        }

        if ($fimg2->num_rows() > 0) {
            $fimg2      = $fimg2->row();
            $fimg2_name = $fimg2->img_name;
            $fimg2_id   = $fimg2->img_id;
        } else {
            $fimg2_name = "";
            $fimg2_id   = "";
        }

        if ($fimg3->num_rows() > 0) {
            $fimg3      = $fimg3->row();
            $fimg3_name = $fimg3->img_name;
            $fimg3_id   = $fimg3->img_id;
        } else {
            $fimg3_name = "";
            $fimg3_id   = "";
        }

        if ($fimg4->num_rows() > 0) {
            $fimg4      = $fimg4->row();
            $fimg4_name = $fimg4->img_name;
            $fimg4_id   = $fimg4->img_id;
        } else {
            $fimg4_name = "";
            $fimg4_id   = "";
        }

        if ($fimg5->num_rows() > 0) {
            $fimg5      = $fimg5->row();
            $fimg5_name = $fimg5->img_name;
            $fimg5_id   = $fimg5->img_id;
        } else {
            $fimg5_name = "";
            $fimg5_id   = "";
        }

        $imTemp_ = array();
        array_push($imTemp_, array(
            'fimg1'=>$fimg1_name,
            'fimg2'=>$fimg2_name,
            'fimg3'=>$fimg3_name,
            'fimg4'=>$fimg4_name,
            'fimg5'=>$fimg5_name,
            'ifimg1'=>$fimg1_id,
            'ifimg2'=>$fimg2_id,
            'ifimg3'=>$fimg3_id,
            'ifimg4'=>$fimg4_id,
            'ifimg5'=>$fimg5_id,
        ));

        return $imTemp_;
    }

    function v_get_all() {
        $this->db->order_by('vdo_id', 'desc' );
        return $this->db->get($this->v_table);
    }

    function v_get_slug($slug) {
        return $this->db->get_where($this->v_table,array('vdo_slug =' => $slug));
    }

    function v_get_like($slug) {
        $this->db->like('vdo_slug', $slug, 'after');
        return $this->db->get($this->v_table);
    }

    function v_insert() {
        // $data['pst_guid']    = base_url().'article/'.$data['pst_slug'].'/';
        $created_by = $this->session->userdata['uNick'];
        $name       = $this->input->post('name');
        $tmpSlug    = slugify($name);

        $vCheck = $this->v_get_like($tmpSlug)->num_rows();
        if ($vCheck == 0) {
            $slug = $tmpSlug;
        } else {
            $vCheck = $this->v_get_all()->row();
            $vCheck = $vCheck->vdo_id + 1;
            $slug = $tmpSlug."-".$vCheck;
        }

        if ($created_by != "" AND $slug != "") {
            $this->db->trans_start(); # Starting Transaction

            if (!empty($_FILES['thumb']['name'])) {
                $filename = 'video-thumbnail-'.$slug;
                $thumbnail = $this->file_upload($filename, 'thumb');

                if ($thumbnail['status'] == 'UPL_FALSE') {
                    $a = file_exists($thumbnail['path']);
                    if ($a > 0) {
                        unlink($thumbnail['path']);
                    }
                    $this->db->trans_rollback();
                    return $thumbnail;
                } else {
                    $thumbname = $thumbnail['data'];
                    $thumb     = base_url() . "attachments/" . $thumbnail['data'];
                }
            } else {
                $thumbname = "nofile";
                $thumb = base_url() ."attachments/00-default.jpg";
            }

            if (!empty($_FILES['video']['name'])) {
                $filename = 'video-'.$slug;
                $video = $this->video_upload($filename, 'video');
                if ($video['status'] == 'VUPL_FALSE') {
                    $a = file_exists($video['path']);
                    if ($a > 0) {
                        unlink($video['path']);
                    }
                    if (!empty($thumbnail['path'])) {
                        $a = file_exists($thumbnail['path']);
                        if ($a > 0) {
                            unlink($thumbnail['path']);
                        }
                    }

                    $this->db->trans_rollback();
                    return $video;
                } else {
                    $link  = $video['data'];
                    $type  = $video['type'];
                    $vname = $video['file']['file_name'];
                }
            } else {
                $link = $this->input->post('otherlink');
                $type  = "video/web";
                $vname = "nofile";
            }

            if ($thumb != "" AND $link != "") {
                $data['vdo_head']       = $name;
                $data['vdo_slug']       = $slug;
                $data['vdo_videoname']  = $vname;
                $data['vdo_imgname']    = $thumbname;
                $data['vdo_link']       = $link;
                $data['vdo_img']        = $thumb;
                $data['vdo_guid']       = base_url() . "videos/detail/" . $slug;
                $data['vdo_type']       = $type;
                $data['created_by']     = $created_by;
                $data['modified_by']    = $created_by;
            }

            $this->db->insert($this->v_table, $data); # Inserting data

            /*Optional*/
            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
                if (!empty($thumbnail['path'])) {
                    $a = file_exists($thumbnail['path']);
                    if ($a > 0) {
                        unlink($thumbnail['path']);
                    }
                }

                if (!empty($video['path'])) {
                    $a = file_exists($video['path']);
                    if ($a > 0) {
                        unlink($video['path']);
                    }
                }

                $ret = array( 'status' => 'INS_FALSE' );
                return $ret;
            } else {
                # Everything is Perfect.
                # Committing data to the database.
                $this->db->trans_commit();
                $ret = array( 'status' => 'INS_TRUE' );
                return $ret;
            }

            $this->db->trans_complete(); # Completing transaction
        } else {
            $ret = array( 'status' => 'NO_AUTH' );
            return $ret;
        }
    }

    function v_delete() {
        $slug = $this->input->post('f_vdel');
        $post = $this->v_get_slug($slug);

        if ($post->num_rows() > 0) {
            $post = $post->row();
            if ($post->vdo_imgname != "nofile") {
                $img = file_exists("./attachments/".$post->vdo_imgname);
                if ($img > 0) {
                    unlink("./attachments/".$post->vdo_imgname);
                }
            }

            if ($post->vdo_videoname != "nofile") {
                $vdo = file_exists("./video/".$post->vdo_videoname);
                if ($vdo > 0) {
                    unlink("./video/".$post->vdo_videoname);
                }
            }

            if ( $this->db->delete($this->v_table, array('vdo_slug' => $slug)) ) {
                return array( 'status' => 'DEL_TRUE' );
            } else {
                return array( 'status' => 'DEL_FALSE' );
            }
        } else {
            return array( 'status' => 'NO_SLUG' );
        }
    }

    function video_upload($filename, $file) {
        $config['upload_path']      = './video/';
        $config['allowed_types']    = '*'; //'mp4|avi|mpeg|MOV|QT|FLV|SWF|WEBM|M4P|M4V|OGG|quicktime';
        $config['file_name']        = $filename;
        $config['overwrite']        = true;
        $config['max_size']         = 300072000; // 300MB

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->upload->do_upload($file)) {
            $path  = './video/'.$this->upload->data("file_name");
            $guid  = base_url().'video/'.$this->upload->data("file_name");
            $ret   = array( 'status' => 'VUPL_TRUE', 'path' => $path, 'data' => $guid, 'type' => $this->upload->data('file_type'), 'file' => $this->upload->data() );
            return $ret;
        } else {
            $ret = array( 'status' => 'VUPL_FALSE', 'error' => $this->upload->display_errors(), 'file' => $this->upload->data() );
            return $ret;
        }
    }

    function socials_get() {
        return $this->db->get('grgl_socialmedia');
    }

    function socials_update() {
        // return $this->db->get('grgl_socialmedia');

        $modified_by = $this->session->userdata['uNick'];

        if ($modified_by == "") {
            return 404;
        } else {
            $icF = $this->input->post('ic-facebook');
            $lnF = $this->input->post('ln-facebook');
            $icY = $this->input->post('ic-youtube');
            $lnY = $this->input->post('ln-youtube');
            $icI = $this->input->post('ic-instagram');
            $lnI = $this->input->post('ln-instagram');
            $icT = $this->input->post('ic-twitter');
            $lnT = $this->input->post('ln-twitter');
            $icP = $this->input->post('ic-pinterest');
            $icP = $this->input->post('ln-pinterest');

            $this->db->trans_start(); # Starting Transaction

            if (! $this->db->query("UPDATE `grgl_socialmedia` SET `soc_icon` = '$icF', `soc_guid` = '$lnF', `modified_by` = '$modified_by' WHERE `soc_name` = 'facebook'")) {
                $code = $this->db->error();
                $this->db->trans_rollback();
                return $code;
            } else {
                if (! $this->db->query("UPDATE `grgl_socialmedia` SET `soc_icon` = '$icY', `soc_guid` = '$lnY', `modified_by` = '$modified_by' WHERE `soc_name` = 'youtube'")) {
                    $code = $this->db->error();
                    $this->db->trans_rollback();
                    return $code;
                } else {
                    if (! $this->db->query("UPDATE `grgl_socialmedia` SET `soc_icon` = '$icI', `soc_guid` = '$lnI', `modified_by` = '$modified_by' WHERE `soc_name` = 'instagram'")) {
                        $code = $this->db->error();
                        $this->db->trans_rollback();
                        return $code;
                    } else {
                        if (! $this->db->query("UPDATE `grgl_socialmedia` SET `soc_icon` = '$icT', `soc_guid` = '$lnT', `modified_by` = '$modified_by' WHERE `soc_name` = 'twitter'")) {
                            $code = $this->db->error();
                            $this->db->trans_rollback();
                            return $code;
                        } else {
                            if (! $this->db->query("UPDATE `grgl_socialmedia` SET `soc_icon` = '$icP', `soc_guid` = '$icP', `modified_by` = '$modified_by' WHERE `soc_name` = 'pinterest'")) {
                                $code = $this->db->error();
                                $this->db->trans_rollback();
                                return $code;
                            } else {
                                $this->db->trans_commit();
                                return 200;
                            }
                        }
                    }
                }
            }

            /*Optional*/
            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
                $code = 500;
                return $code;
            } else {
                # Everything is Perfect.
                # Committing data to the database.
                $this->db->trans_commit();
                return 200;
            }

            $this->db->trans_complete(); # Completing transaction 
        }
    }
}