<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() 
	{ 
		parent::__construct();
		$this->load->model('Home_model');
	}

	public function angelsdb()
	{
		$db = array(
			array(
				'img' => 'merchandise/13-31-oktober-2019-2.jpg',
				'head' => 'Wika Salim',
				'desc' =>  '22 tahun'
			),
			array(
				'img' => 'merchandise/1-15-januari-2020.jpg',
				'head' => 'Lia Widi',
				'desc' =>  '20 tahun'
			),
			array(
				'img' => 'merchandise/1-15-september-2019.jpg',
				'head' => 'Nike Karmila',
				'desc' =>  '25 tahun'
			),
		);

		return $db;
	}

	public function merchandisedb()
	{
		$db = array(
			array(
				'img' => 'merchandise/1-15-november-2019.jpg',
				'head' => 'Valid 1-15 November 2019',
				'desc' =>  'Every purchase of 15 products package, will get 1 free GYM BAG or SNEAKER'
			),
			array(
				'img' => 'merchandise/13-23-februari-2020.jpg',
				'head' => 'Valid 13-23 Februari 2020',
				'desc' =>  'Every purchase of 15 products package, will get 1 free TUMBLR and 5 inner box glutathione'
			),
		);

		return $db;
	}

	public function index()
	{
		$data['newpost'] = $this->Home_model->newArticle();
		$data['content'] = $this->Home_model->homeMenu();
		$data['video']	 = $this->Home_model->getAllVideo()->row();
		$data['socials'] = $this->Home_model->getSocial()->result();
		$this->load->view('home', $data);
	}

	public function allcategory()
	{
		redirect(base_url());
	}

	public function category($link)
	{
		$data['sidebar']	= $this->Home_model->getSidebar();
		$data['video']	 	= $this->Home_model->getAllVideo()->row();
		$data['category']   = $this->Home_model->getCategory($link);
		$data['posts']		= $this->Home_model->postbyCategory($link);
		$this->load->view('blog', $data);
	}

	public function tags($tag)
	{
		$data['sidebar']	= $this->Home_model->getSidebar();
		$data['video']	 	= $this->Home_model->getAllVideo()->row();
		$data['posts']		= $this->Home_model->postbyTags($tag);
		$data['tag'] 		= $tag;
		$this->load->view('blog', $data);
	}

	public function allarticle()
	{
		$data['sidebar']	= $this->Home_model->getSidebar(); 
		$data['video']	 	= $this->Home_model->getAllVideo()->row();
		$data['posts']		= $this->Home_model->getallPost();
		$this->load->view('blog', $data);
	}

	public function article($link)
	{
		$data['sidebar']	= $this->Home_model->getSidebar();
		$data['video']	 	= $this->Home_model->getAllVideo()->row();
		$data['post'] 		= $this->Home_model->getpostbySlug($link)->row();
		$data['imeta'] 		= $this->Home_model->getfimgbyPost2($link);
		$data['related']	= $this->Home_model->getrelatedPost($link);
		$data['source']		= $this->Home_model->getpostSourcebySlug($link);
		$data['tags']		= $this->Home_model->getpostTagsbySlug($link);

		$i 		= 0;
		$len 	= count($data['tags']);
		$words 	= "";
		foreach ($data['tags'] as $tag) {
			$word = slugify($tag);
			if ($i != ($len - 1)) {
				$words .= ucwords($word) . ', '; 
			} else {
				$words .= ucwords($word);
			}
			$i++;
		}

		$data['metadesc'] 	= character_limiter($data['post']->pst_txt,160);
		$data['metakey'] 	= $words;

		$this->load->view('single-page', $data);
	}

	public function video($method = NULL, $slug = NULL)
	{
		$data['sidebar']	= $this->Home_model->getSidebar();
		
		if ($method == NULL) {
			$data['videos'] 	= $this->Home_model->getAllVideo()->result();
			$this->load->view('video', $data);
		} elseif ($method == 'detail') {
			$data['videos'] = $this->Home_model->getVideo($slug)->row();
			if (!empty($data['videos'])) {
				$this->load->view('video-single', $data);
			} else {
				redirect('404');
			}
		} else {
			redirect('404');
		}
	}

	public function allgallery()
	{
		$data['sidebar'] = $this->Home_model->getSidebar();
		$data['video']	 = $this->Home_model->getAllVideo()->row();
		$this->load->view('gallery', $data);
	}

	public function gallery($link)
	{
		$data['sidebar'] = $this->Home_model->getSidebar();
		$data['video']	 = $this->Home_model->getAllVideo()->row();

		if ($link == 'angels') {
			$data['title'] = 'Angels Gallery';
			$data['gallery'] = $this->angelsdb();
		} else if ($link == 'merchandise') {
			$data['title'] = 'Merchandise Gallery';
			$data['gallery'] = $this->merchandisedb();
		} else {
			show_404();
		}

		$this->load->view('gallery-single', $data);
	}

	public function search($get)
	{
		$data['sidebar']	= $this->Home_model->getSidebar(); 
		$data['video']	 	= $this->Home_model->getAllVideo()->row();
		$data['posts']		= $this->Home_model->getpostbySearch();
		$this->load->view('blog', $data);
	}
}
