<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() 
	{ 
		parent::__construct();
		$this->load->model('User_model', 'user');
	}

	public function index()
	{
		// $this->load->view('login');
	}

	function login()
	{
		/* Validation rule */
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == FALSE) { 
			$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-warning" role="alert">Kolom tidak boleh kosong!</div>', 3);
			redirect(site_url('login/'));
		} else {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$where = array(
				'usr_nick' => $username,
				'usr_psswrd' => md5($password)
			);
			$cek = $this->user->lCheck($where)->num_rows();
			
			if($cek > 0){
				$data = $this->user->login($where);

				if ($data->usr_stat == 'active' || $data->usr_stat == 'root') {
					$data_session = array(
						'uNick'	=> $data->usr_nick,
						'uName'	=> $data->usr_name,
						'uStat'	=> $data->usr_stat,
						'uLstat'=> "true",
					);
	 
					$this->session->set_userdata($data_session);

					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-success" role="alert">Login sukses</div>', 3);
					redirect(site_url('dashboard/admin/'));
				} else {
					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">Akun anda belum aktif, hubungi pihak administrator untuk mengaktifkan akun anda</div>', 3);
					redirect(site_url('login/'));
				}
			} else {
				$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">Username atau Password anda salah</div>', 3);
				redirect(site_url('login/'));
			}
		}
	}
 
	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}

	function save()
	{
		/* Validation rule */
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('name', 'Fullname', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[100]');

		if ($this->form_validation->run() == FALSE) { 
			$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-warning" role="alert">Semua kolom harus diisi!</div>', 3);
			redirect(site_url('dashboard/admin/adduser'));
		} else {
			if($this->user->insert()){
				$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-success" role="alert">User berhasil didaftarkan!</div>', 3);
				redirect(site_url('dashboard/admin/users/')); 
			} else {
				$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">Gagal menambahkan user baru!</div>', 3);
				redirect(site_url('dashboard/admin/adduser'));
			}
		}
	}

	function update()
	{
		$cond = $this->input->post('accsumbit');
		if ($cond == 'useredit') {
			/* Validation rule */
			$this->form_validation->set_rules('name', 'name', 'required');

			if ($this->form_validation->run() == FALSE) { 
				$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-warning" role="alert">Nama pengguna wajib diisi!</div>', 3);
				redirect(site_url('dashboard/admin/account/'));
			} else {
				$ret = $this->user->update_account($cond);

				if ($ret == 200) {
					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-success" role="alert">Nama user berhasil dirubah!</div>', 3);
					redirect(site_url('dashboard/admin/account/'));
				} elseif ($ret == 404) {
					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">Gagal mengubah data!<br>Error: Access Function Failed!</div>', 3);
					redirect(site_url('dashboard/admin/account/'));
				} else {
					$alert = '<div id="alert" class="alert alert-danger" role="alert">Gagal mengubah data!<br>Error Code: '.$db["code"].'</div>';
					$this->session->set_tempdata('alert_form', $alert, 3);
					redirect(site_url('dashboard/admin/account/'));
				}
			}
		} elseif ($cond == 'passedit') {
			/* Validation rule */
			$this->form_validation->set_rules('oldpass', 'oldpass', 'required');
			$this->form_validation->set_rules('newpass', 'newpass', 'required');

			if ($this->form_validation->run() == FALSE) { 
				$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-warning" role="alert">Kolom password lama atau password baru tidak boleh kosong!</div>', 3);
				redirect(site_url('dashboard/admin/account/'));
			} else {
				
				$ret = $this->user->update_account($cond);

				if ($ret == 200) {
					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-success" role="alert">Password berhasil dirubah!</div>', 3);
					redirect(site_url('dashboard/admin/account/'));
				} elseif ($ret == 404) {
					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">Gagal mengubah data!<br>Error: Access Function Failed!</div>', 3);
					redirect(site_url('dashboard/admin/account/'));
				} elseif ($ret == 500) {
					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-warning" role="alert">Password yang dimasukkan salah</div>', 3);
					redirect(site_url('dashboard/admin/account/'));
				} else {
					$alert = '<div id="alert" class="alert alert-danger" role="alert">Password gagal diubah!<br>Error Code: '.$db["code"].'</div>';
					$this->session->set_tempdata('alert_form', $alert, 3);
					redirect(site_url('dashboard/admin/account/'));
				}
				
			}
		} else {
			$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">Form Submit Error!</div>', 3);
			redirect(site_url('dashboard/admin/account/'));
		}
	}
}
