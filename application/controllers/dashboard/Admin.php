<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() 
	{ 
		parent::__construct();
		$this->load->model('Posts_model');
		$this->load->model('User_model');
	}

	public function index()
	{
		$data['head'] 	= $this->load->view('dashboard/b-head', NULL, TRUE);
		$data['menu'] 	= $this->load->view('dashboard/menu', NULL, TRUE);
		$data['header']	= $this->load->view('dashboard/header', NULL, TRUE);
		$data['posts']	= $this->Posts_model->p_get_publish();
		$data['cvideo'] = $this->Posts_model->v_get_all()->num_rows();
		$data['category'] = $this->Posts_model->c_get_all()->result();
		$data['cuser'] 	= $this->User_model->getall()->num_rows();
		$this->load->view('dashboard/index', $data);
	}

	public function allposts()
	{
		$filterone = $this->input->get('cat');
		$filtertwo = $this->input->get('st');

		if ($filterone == NULL && $filtertwo == NULL) {
			$data['head'] 	= $this->load->view('dashboard/b-head', NULL, TRUE);
			$data['menu'] 	= $this->load->view('dashboard/menu', NULL, TRUE);
			$data['header']	= $this->load->view('dashboard/header', NULL, TRUE);
			$data['posts']	= $this->Posts_model->p_get_all();
			$data['categories'] = $this->Posts_model->c_get_all()->result();
			$data['filterone'] = '';
			$data['filtertwo'] = '';
			$this->load->view('dashboard/article-all', $data);
		} else {
			$data['head'] 	= $this->load->view('dashboard/b-head', NULL, TRUE);
			$data['menu'] 	= $this->load->view('dashboard/menu', NULL, TRUE);
			$data['header']	= $this->load->view('dashboard/header', NULL, TRUE);
			$data['posts']	= $this->Posts_model->p_get_filter($filterone, $filtertwo);
			$data['categories'] = $this->Posts_model->c_get_all()->result();
			$data['filterone'] = $filterone;
			$data['filtertwo'] = $filtertwo;
			$this->load->view('dashboard/article-all', $data);
		}
	}

	public function addpost()
	{
		$data['head'] 		= $this->load->view('dashboard/b-head', NULL, TRUE);
		$data['menu'] 		= $this->load->view('dashboard/menu', NULL, TRUE);
		$data['header']		= $this->load->view('dashboard/header', NULL, TRUE);
		$data['category'] 	= $this->Posts_model->c_get_all()->result();
		$this->load->view('dashboard/article-add', $data);
	}

	public function editpost($slug)
	{
		$data['head'] 		= $this->load->view('dashboard/b-head', NULL, TRUE);
		$data['menu'] 		= $this->load->view('dashboard/menu', NULL, TRUE);
		$data['header']		= $this->load->view('dashboard/header', NULL, TRUE);
		$data['category'] 	= $this->Posts_model->c_get_all()->result();
		$data['post'] 		= $this->Posts_model->p_get_slug($slug)->row();
		$data['pmeta'] 		= $this->Posts_model->pm_post_perslug($slug);
		$data['imeta'] 		= $this->Posts_model->get_fimg_bypost($slug);
		$this->load->view('dashboard/article-edit', $data);
	}

	public function category($method = NULL, $slug = NULL)
	{
		if ($this->session->userdata['uStat']== "root") :
			$data['head'] 		= $this->load->view('dashboard/b-head', NULL, TRUE);
			$data['menu'] 		= $this->load->view('dashboard/menu', NULL, TRUE);
			$data['header']		= $this->load->view('dashboard/header', NULL, TRUE);

			if ($method == NULL) {
				$data['category'] 	= $this->Posts_model->c_get_all()->result();
				if (isset($data['category'])) {
					$this->load->view('dashboard/category', $data);
				} else {
					$this->load->view('dashboard/index', $data);
				}
			} elseif ($method == 'edit') {
				$data['category'] 	= $this->Posts_model->c_get_slug($slug)->result();
				if (isset($data['category'])) {
					$this->load->view('dashboard/e-category', $data);
				} else {
					redirect(site_url('dashboard/admin/category/'));
				}
			} else {
				redirect(site_url('dashboard/admin/'));
			}
		else :
			$data['heading'] = '404 Page Not Found';
			$data['message'] = 'The page you requested was not found.';
			$this->load->view('errors/html/error_404', $data);
		endif;
	}

	public function video($method = NULL, $slug = NULL)
	{
		$data['head'] 		= $this->load->view('dashboard/b-head', NULL, TRUE);
		$data['menu'] 		= $this->load->view('dashboard/menu', NULL, TRUE);
		$data['header']		= $this->load->view('dashboard/header', NULL, TRUE);

		if ($method == NULL) {
			$data['video'] 	= $this->Posts_model->v_get_all()->result();
			if (isset($data['video'])) {
				$this->load->view('dashboard/video', $data);
			} else {
				$this->load->view('dashboard/index', $data);
			}
		} elseif ($method == 'edit') {
			$data['category'] 	= $this->Posts_model->c_get_slug($slug)->result();
			if (isset($data['category'])) {
				$this->load->view('dashboard/e-video', $data);
			} else {
				redirect(site_url('dashboard/admin/video/'));
			}
		} else {
			redirect(site_url('dashboard/admin/'));
		}
	}

	public function account()
	{
		$where = array(
			'usr_nick' => $this->session->userdata['uNick']
		);

		$data['head'] 	= $this->load->view('dashboard/b-head', NULL, TRUE);
		$data['menu'] 	= $this->load->view('dashboard/menu', NULL, TRUE);
		$data['header']	= $this->load->view('dashboard/header', NULL, TRUE);
		$data['user'] 	= $this->User_model->getone($where)->row();
		$this->load->view('dashboard/account', $data);
	}

	public function users()
	{
		$filter = $this->input->get('filter');

		if ($filter == NULL || $filter == 'all') {
			if ($this->session->userdata['uStat']== "root") :
				$data['head'] 	= $this->load->view('dashboard/b-head', NULL, TRUE);
				$data['menu'] 	= $this->load->view('dashboard/menu', NULL, TRUE);
				$data['header']	= $this->load->view('dashboard/header', NULL, TRUE);
				$data['users']	= $this->User_model->getall()->result();
				$data['count']	= count($data['users']);
				$data['filter'] = 'all';
				$this->load->view('dashboard/user-all', $data);
			else :
				$data['heading'] = '404 Page Not Found';
				$data['message'] = 'The page you requested was not found.';
				$this->load->view('errors/html/error_404', $data);
			endif;
		} else {
			$data['head'] 	= $this->load->view('dashboard/b-head', NULL, TRUE);
			$data['menu'] 	= $this->load->view('dashboard/menu', NULL, TRUE);
			$data['header']	= $this->load->view('dashboard/header', NULL, TRUE);
			$data['users'] = $this->User_model->userbyFilters($filter)->result();
			$data['count']	= count($data['users']);
			$data['filter'] = $filter;

			$this->load->view('dashboard/user-all', $data);
		}
	}

	public function adduser()
	{
		if ($this->session->userdata['uStat']== "root") :
			$data['head'] 	= $this->load->view('dashboard/b-head', NULL, TRUE);
			$data['menu'] 	= $this->load->view('dashboard/menu', NULL, TRUE);
			$data['header']	= $this->load->view('dashboard/header', NULL, TRUE);
			$data['users']	= $this->User_model->getall()->result();
			$data['count']	= count($data['users']);
			$this->load->view('dashboard/user-add', $data);
		else :
			$data['heading'] = '404 Page Not Found';
			$data['message'] = 'The page you requested was not found.';
			$this->load->view('errors/html/error_404', $data);
		endif;
	}

	public function socials()
	{
		if ($this->session->userdata['uStat']== "root") :
			$data['head'] 	= $this->load->view('dashboard/b-head', NULL, TRUE);
			$data['menu'] 	= $this->load->view('dashboard/menu', NULL, TRUE);
			$data['header']	= $this->load->view('dashboard/header', NULL, TRUE);
			$data['socials']= $this->Posts_model->socials_get()->result();
			$this->load->view('dashboard/socials-media', $data);
		else :
			$data['heading'] = '404 Page Not Found';
			$data['message'] = 'The page you requested was not found.';
			$this->load->view('errors/html/error_404', $data);
		endif;
	}

	public function f_categoryadd()
	{
		/* Validation rule */
		$this->form_validation->set_rules('name', 'Name', 'required');

		if ($this->form_validation->run() == FALSE) { 
			$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-warning" role="alert">Kolom nama tidak boleh kosong!</div>', 3);
			redirect(site_url('dashboard/admin/category/'));
		} 
		else {
			if ($this->input->post('slug') != "") {
				$slug = slugify($this->input->post('slug'));
				$c_slug = $this->Posts_model->c_get_slug($slug)->num_rows();
				if ($c_slug > 0) {
					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">Gagal menambahkan kategori baru!<br>Slug sudah ada, silahkan gunakan slug lain.</div>', 3);
					redirect(site_url('dashboard/admin/category/'));
				} else {
					$c_slug = 'true';
				}
			} else {
				$slug = slugify($this->input->post('name'));
				$c_slug = $this->Posts_model->c_get_slug($slug)->num_rows();
				if ($c_slug > 0) {
					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">Gagal menambahkan kategori baru!<br>Slug dari nama kategori sudah ada, silahkan gunakan nama kategori lain.</div>', 3);
					redirect(site_url('dashboard/admin/category/'));
				} else {
					$c_slug = 'true';
				}
			}

			if ($c_slug == 'true') {
				$db = $this->Posts_model->c_insert();

				if ($db == 200) {
					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-success" role="alert">Kategori berhasil didaftarkan!</div>', 3);
					redirect(site_url('dashboard/admin/category/'));
				} else {
					$alert = '<div id="alert" class="alert alert-danger" role="alert">Gagal menambahkan kategori baru!<br>Error Code: '.$db["code"].'</div>';
					$this->session->set_tempdata('alert_form', $alert, 3);
					redirect(site_url('dashboard/admin/category/'));
				}
			} else {
				$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">Gagal menambahkan kategori baru!<br>Invalid slug data</div>', 3);
				redirect(site_url('dashboard/admin/category/'));
			}
		}
	}

	public function f_categoryedit($link)
	{
		/* Validation rule */
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('id', 'Id', 'required');

		if ($this->form_validation->run() == FALSE) { 
			$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-warning" role="alert">Kolom nama tidak boleh kosong! atau validation form error.</div>', 3);
			redirect(site_url('dashboard/admin/category/edit/'.$link));
		} 
		else {
			if ($this->input->post('slug') != "") {
				$slug = slugify($this->input->post('slug'));
				$id = slugify($this->input->post('id'));
				$c_slug = $this->Posts_model->c_get_notin_id($id, $slug)->num_rows();
				if ($c_slug > 0) {
					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">Gagal mengubah kategori!<br>Slug sudah ada, silahkan gunakan slug lain.</div>', 3);
					redirect(site_url('dashboard/admin/category/edit/'.$link));
				} else {
					$c_slug = 'true';
				}
			} else {
				$slug = slugify($this->input->post('name'));
				$c_slug = $this->Posts_model->c_get_notin_id($id, $slug)->num_rows();
				if ($c_slug > 0) {
					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">Gagal mengubah kategori!<br>Slug dari nama kategori sudah digunakan, silahkan gunakan nama kategori lain.</div>', 3);
					redirect(site_url('dashboard/admin/category/edit/'.$link));
				} else {
					$c_slug = 'true';
				}
			}

			if ($c_slug == 'true') {
				$db = $this->Posts_model->c_update();

				if ($db == 200) {
					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-success" role="alert">Kategori berhasil diubah!</div>', 3);
					redirect(site_url('dashboard/admin/category/'));
				} else {
					$alert = '<div id="alert" class="alert alert-danger" role="alert">Gagal mengubah kategori!<br>Error Code: '.$db["code"].'</div>';
					$this->session->set_tempdata('alert_form', $alert, 3);
					redirect(site_url('dashboard/admin/category/edit/'.$link));
				}
			} else {
				$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">Gagal mengubah kategori!<br>Invalid slug data</div>', 3);
				redirect(site_url('dashboard/admin/category/edit/'.$link));
			}
		}
	}

	public function f_categorydelete()
	{
		/* Validation rule */
		$this->form_validation->set_rules('f_cdel', 'f_cdel', 'required');

		if ($this->form_validation->run() == FALSE) { 
			$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-warning" role="alert">Data yang ingin dihapus tidak ditemukan.</div>', 3);
			redirect(site_url('dashboard/admin/category/'));
		} else {
			if ($this->input->post('f_cdel') != "") {
				$slug = $this->input->post('f_cdel');
				$c_slug = $this->Posts_model->c_get_slug($slug)->num_rows();
				if ($c_slug > 0) {
					$c_slug = 'true';
				} else {
					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">Gagal menghapus kategori!<br>Error: Pengambilan link kategori error.</div>', 3);
					redirect(site_url('dashboard/admin/category/'));
				}
			} else {
				$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-warning" role="alert">Data yang ingin dihapus tidak ditemukan.</div>', 3);
				redirect(site_url('dashboard/admin/category/'));
			}

			if ($c_slug == 'true') {
				$db = $this->Posts_model->c_delete();

				if ($db == 200) {
					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-success" role="alert">Kategori berhasil dihapus!</div>', 3);
					redirect(site_url('dashboard/admin/category/'));
				} else {
					$alert = '<div id="alert" class="alert alert-danger" role="alert">Gagal menghapus kategori!<br>Error Code: '.$db["code"].'</div>';
					$this->session->set_tempdata('alert_form', $alert, 3);
					redirect(site_url('dashboard/admin/category/'));
				}
			} else {
				$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">Gagal menghapus kategori!<br>Kategori tidak ditemukan.</div>', 3);
				redirect(site_url('dashboard/admin/category/'));
			}
		}
	}

	public function f_postadd()
	{
		if ( $this->input->post('submit') != "" ) {
			/* Validation rule */
			$this->form_validation->set_rules('title', 'Title', 'required');

			if ($this->form_validation->run() == FALSE) { 
			$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-warning" role="alert">Judul artikel tidak boleh kosong.</div>', 3);
			redirect(site_url('dashboard/admin/addpost/'));
			} else {
				$slug = slugify($this->input->post('title'));
				$p_slug = $this->Posts_model->p_get_slug($slug)->num_rows();
				if ($p_slug > 0) {
					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">Gagal menambahkan artikel baru!<br>Judul link sudah digunakan, silahkan gunakan judul lain.</div>', 3);
					redirect(site_url('dashboard/admin/addpost/'));
				} else {
					$p_slug = 'true';
				}
			}

			if ($p_slug == 'true') {
				$db = $this->Posts_model->p_insert();

				if ($db['status'] == 'TRS_TRUE') {
					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-success" role="alert">Artikel berhasil ditambahkan!</div>', 3);
					redirect(site_url('dashboard/admin/allposts/'));
				} else if ($db['status'] == 'TRS_FALSE') {
					$alert = '<div id="alert" class="alert alert-danger" role="alert">Artikel gagal ditambahkan!<br>Kesalahan pada insert transaction</div>';
					$this->session->set_tempdata('alert_form', $alert, 3);
					redirect(site_url('dashboard/admin/addpost/'));
				} else if ($db['status'] == 'UPL_FALSE') {
					$alert = '<div id="alert" class="alert alert-danger" role="alert">Feature Image gagal ditambahkan!<br>Error: '.$db['error'].'</div>';
					$this->session->set_tempdata('alert_form', $alert, 3);
					redirect(site_url('dashboard/admin/addpost/'));
				} else {
					$alert = '<div id="alert" class="alert alert-danger" role="alert">Artikel gagal ditambahkan!<br>Kesalahan unknown</div>';
					$this->session->set_tempdata('alert_form', $alert, 3);
					redirect(site_url('dashboard/admin/addpost/'));
				}
			} else {
				$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert"Gagal menambahkan artikel baru!<br>Artikel link tidak ditemukan.</div>', 3);
				redirect(site_url('dashboard/admin/addpost/'));
			}

		} else {
			$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">Gagal menambahkan artikel! <br>Submit problem.</div>', 3);
			redirect(site_url('dashboard/admin/addpost/'));
		}
	}

	public function f_postedit($default)
	{
		if ( $this->input->post('submit') != "" AND $this->session->userdata['uNick'] != "") {
			/* Validation rule */
			$this->form_validation->set_rules('title', 'Title', 'required');

			if ($this->form_validation->run() == FALSE) { 
			$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-warning" role="alert">Judul artikel tidak boleh kosong.</div>', 3);
			redirect(site_url('dashboard/admin/editpost/'.$default));
			} else {
				$p_slug = $this->Posts_model->p_get_slug($default)->num_rows();
				if ($p_slug > 0) {
					$p_slug = 'true';
				} else {
					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert"Gagal mengubah artikel!<br>Artikel link tidak ditemukan.</div>', 3);
					redirect(site_url('dashboard/admin/editpost/'.$default));
				}
			}

			if ($p_slug == 'true') {
				$db = $this->Posts_model->p_update($default);

				if ($db['status'] == 'TRS_TRUE') {
					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-success" role="alert">Artikel berhasil diubah!</div>', 3);
					redirect(site_url('dashboard/admin/allposts/'));
				} else if ($db['status'] == 'TRS_FALSE') {
					$alert = '<div id="alert" class="alert alert-danger" role="alert">Artikel gagal diubah!<br>Kesalahan pada insert transaction</div>';
					$this->session->set_tempdata('alert_form', $alert, 3);
					redirect(site_url('dashboard/admin/editpost/'.$default));
				} else if ($db['status'] == 'UPL_FALSE') {
					$alert = '<div id="alert" class="alert alert-danger" role="alert">Feature Image gagal diubah!<br>Error: '.$db['error'].'</div>';
					$this->session->set_tempdata('alert_form', $alert, 3);
					redirect(site_url('dashboard/admin/editpost/'.$default));
				} else if ($db['status'] == 'SLG_FAIL') {
					$alert = '<div id="alert" class="alert alert-danger" role="alert">Artikel gagal diubah!<br>Judul atau link artikel telah digunakan </div>';
					$this->session->set_tempdata('alert_form', $alert, 3);
					redirect(site_url('dashboard/admin/editpost/'.$default));
				} else {
					$alert = '<div id="alert" class="alert alert-danger" role="alert">Artikel gagal diubah!<br>Kesalahan unknown</div>';
					$this->session->set_tempdata('alert_form', $alert, 3);
					redirect(site_url('dashboard/admin/editpost/'.$default));
				}
			}

		} else {
			$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">Gagal mengubah artikel! <br>Submit problem.</div>', 3);
			redirect(site_url('dashboard/admin/editpost/'.$default));
		}
	}

	public function f_postdelete()
	{
		/* Validation rule */
		$this->form_validation->set_rules('f_pdel', 'f_pdel', 'required');

		if ($this->form_validation->run() == FALSE) { 
			$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-warning" role="alert">Data yang ingin dihapus tidak ditemukan.</div>', 3);
			redirect(site_url('dashboard/admin/allposts/'));
		} else {
			if ($this->input->post('f_pdel') != "") {
				$slug = $this->input->post('f_pdel');
				$p_slug = $this->Posts_model->p_get_slug($slug)->num_rows();
				if ($p_slug > 0) {
					$p_slug = 'true';
				} else {
					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">Gagal menghapus artikel!<br>Error: Pengambilan link artikel error.</div>', 3);
					redirect(site_url('dashboard/admin/allposts/'));
				}
			} else {
				$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-warning" role="alert">Data yang ingin dihapus tidak ditemukan.</div>', 3);
				redirect(site_url('dashboard/admin/allposts/'));
			}

			if ($p_slug == 'true') {
				$db = $this->Posts_model->p_delete();

				if ($db == 200) {
					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-success" role="alert">Artikel berhasil dihapus!</div>', 3);
					redirect(site_url('dashboard/admin/allposts/'));
				} else {
					$alert = '<div id="alert" class="alert alert-danger" role="alert">Gagal menghapus artikel!</div>';
					$this->session->set_tempdata('alert_form', $alert, 3);
					redirect(site_url('dashboard/admin/allposts/'));
				}
			} else {
				$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">Gagal menghapus artikel!<br>Error: Artikel link is <b>false</b>.</div>', 3);
				redirect(site_url('dashboard/admin/allposts/'));
			}
		}
	}

	public function f_videoadd()
	{

		$this->form_validation->set_rules('name', 'name', 'required');

		if ($this->form_validation->run() == FALSE) { 
			$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-warning" role="alert">Kolom judul tidal boleh kosong.</div>', 3);
			redirect(site_url('dashboard/admin/video/'));
		} else {
			if ($this->input->post('otherlink') == "" && $this->input->post('in-video') == "") {
				$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-warning" role="alert">Link video maupun video upload tidak ditemukan.</div>', 3);
				redirect(site_url('dashboard/admin/video/'));
			} else {
				$db = $this->Posts_model->v_insert();

				if ($db['status'] == 'INS_TRUE') {
					$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-success" role="alert">Video berhasil ditambahkan!</div>', 3);
					redirect(site_url('dashboard/admin/video/'));
				} else if ($db['status'] == 'INS_FALSE') {
					$alert = '<div id="alert" class="alert alert-danger" role="alert">Video gagal ditambahkan!<br>Kesalahan pada penginputan data</div>';
					$this->session->set_tempdata('alert_form', $alert, 3);
					redirect(site_url('dashboard/admin/video/'));
				} else if ($db['status'] == 'UPL_FALSE') {
					$alert = '<div id="alert" class="alert alert-danger" role="alert">Thumbnail gagal diupload!<br>Error: '.$db['error'].'</div>';
					$this->session->set_tempdata('alert_form', $alert, 3);
					redirect(site_url('dashboard/admin/video/'));
				} else if ($db['status'] == 'VUPL_FALSE') {
					$alert = '<div id="alert" class="alert alert-danger" role="alert">Vieo gagal diupload!<br>Error: '.$db['error'].'</div>';
					$this->session->set_tempdata('alert_form', $alert, 3);
					redirect(site_url('dashboard/admin/video/'));
				} else {
					$alert = '<div id="alert" class="alert alert-danger" role="alert">Video gagal diupload!<br>Kesalahan unknown</div>';
					$this->session->set_tempdata('alert_form', $alert, 3);
					redirect(site_url('dashboard/admin/video/'));
				}
			}
		}
	}

	public function f_videodelete()
	{
		$this->form_validation->set_rules('f_vdel', 'f_vdel', 'required');

		if ($this->form_validation->run() == FALSE) { 
			$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-warning" role="alert">Hapus video gagal.</div>', 3);
			redirect(site_url('dashboard/admin/video/'));
		} else {
			$db = $this->Posts_model->v_delete();

			if ($db['status'] == 'DEL_TRUE') {
				$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-success" role="alert">Video berhasil dihapus!</div>', 3);
				redirect(site_url('dashboard/admin/video/'));
			} else if ($db['status'] == 'DEL_FALSE') {
				$alert = '<div id="alert" class="alert alert-danger" role="alert">Video gagal dihapus!<br></div>';
				$this->session->set_tempdata('alert_form', $alert, 3);
				redirect(site_url('dashboard/admin/video/'));
			} else if ($db['status'] == 'NO_SLUG') {
				$alert = '<div id="alert" class="alert alert-danger" role="alert">Video gagal dihapus!<br>Data link tidak ditemukan</div>';
				$this->session->set_tempdata('alert_form', $alert, 3);
				redirect(site_url('dashboard/admin/video/'));
			} else {
				$alert = '<div id="alert" class="alert alert-danger" role="alert">Video gagal dihapus!<br>Kesalahan unknown</div>';
				$this->session->set_tempdata('alert_form', $alert, 3);
				redirect(site_url('dashboard/admin/video/'));
			}
		}
	}

	public function f_userupdate()
	{
		$ret = $this->User_model->update_role();
		if ($ret == 200) {
			if ($this->input->post('user') == $this->session->userdata['uNick']) {
				
				if ($this->input->post('status') == 'deactive') {
					$Lstat = 'false';
				}

				$data_session = array(
					'uStat'	=> $this->input->post('status'),
					'uLstat'=> $Lstat,
				);
				$this->session->set_userdata($data_session);
			}

			$data = 'true';
		} else if ($ret == 404) {
			$data = $ret;
		} else {
			$data = $ret["code"];
		}
        echo json_encode($data);
	}

	public function f_userdelete()
	{
		/* Validation rule */
		$this->form_validation->set_rules('user', 'user', 'required');

		if ($this->form_validation->run() == FALSE) { 
			$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-warning" role="alert">Data yang ingin dihapus tidak ditemukan.</div>', 3);
			redirect(site_url('dashboard/admin/users/'));
		} else {
			$ret = $this->User_model->delete();

			if ($ret == 200) {
				if ($this->input->post('user') == $this->session->userdata['uNick']) {
					$data_session = array(
						'uNick'	=> "",
						'uName'	=> "",
						'uStat'	=> "",
						'uLstat'=> "false",
					);
					$this->session->set_userdata($data_session);
				}

				$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-success" role="alert">User berhasil dihapus!</div>', 3);
				redirect(site_url('dashboard/admin/users/'));
			} else if ($ret == 404) {
				$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">Gagal menghapus user!<br>Error: Access Function Failed!</div>', 3);
				redirect(site_url('dashboard/admin/users/'));
			} else {
				$alert = '<div id="alert" class="alert alert-danger" role="alert">Gagal menghapus user!<br>Error Code: '.$ret['code'].'!</div>';
				$this->session->set_tempdata('alert_form', $alert, 3);
				redirect(site_url('dashboard/admin/allposts/'));
			}
		}
	}

	public function f_socialupdate() {
		$ret = $this->Posts_model->socials_update();

		if ($ret == 200) {
			$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-success" role="alert">Pengaturan social media berhasil disimpan!</div>', 3);
			redirect(site_url('dashboard/admin/socials/'));
		} else if ($ret == 404) {
			$this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">Pengaturan social media gagal disimpan!<br>Error: Access Function Failed!</div>', 3);
			redirect(site_url('dashboard/admin/socials/'));
		} else {
			$alert = '<div id="alert" class="alert alert-danger" role="alert">Pengaturan social media gagal disimpan!<br>Error Code: '.$ret['code'].'!</div>';
			$this->session->set_tempdata('alert_form', $alert, 3);
			redirect(site_url('dashboard/admin/socials/'));
		}
	}
}
