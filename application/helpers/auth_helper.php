<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('auth'))
{
	function auth()
	{
		if (isset($this->session->userdata['uLstat'])) :
        	if ($this->session->userdata['uLstat'] != "true") :
	            $this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">User Authentification Eror!</div>', 3);
	            redirect(base_url());
	            return 'false';
        	else :
        		return 'true';
        	endif;
	    else :
	        $this->session->set_tempdata('alert_form', '<div id="alert" class="alert alert-danger" role="alert">User Authentification Eror!</div>', 3);
	        redirect(base_url());
	    endif;
	}
}