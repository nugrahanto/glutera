-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 13, 2020 at 08:53 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `grid_glutera`
--

-- --------------------------------------------------------

--
-- Table structure for table `grgl_category`
--

CREATE TABLE `grgl_category` (
  `ctr_id` int(255) NOT NULL,
  `ctr_name` varchar(255) NOT NULL,
  `ctr_slug` varchar(255) NOT NULL,
  `ctr_desc` longtext DEFAULT '-',
  `ctr_status` char(10) NOT NULL DEFAULT 'Draft',
  `ctr_guid` varchar(255) NOT NULL,
  `created_by` varchar(255) DEFAULT 'anonim',
  `modified_by` varchar(255) DEFAULT 'anonim',
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `grgl_categorymeta`
--

CREATE TABLE `grgl_categorymeta` (
  `meta_id` int(255) NOT NULL,
  `ctr_id` int(255) NOT NULL,
  `meta_key` varchar(255) NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `grgl_img`
--

CREATE TABLE `grgl_img` (
  `img_id` int(255) NOT NULL,
  `img_name` varchar(255) NOT NULL,
  `img_guid` varchar(255) NOT NULL,
  `img_mime_type` char(255) NOT NULL,
  `created_by` varchar(255) DEFAULT 'anonim',
  `modified_by` varchar(255) DEFAULT 'anonim',
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `grgl_imgmeta`
--

CREATE TABLE `grgl_imgmeta` (
  `meta_id` int(255) NOT NULL,
  `img_id` int(255) NOT NULL,
  `meta_key` varchar(255) NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `grgl_posts`
--

CREATE TABLE `grgl_posts` (
  `pst_id` int(255) NOT NULL,
  `pst_head` varchar(255) NOT NULL,
  `pst_slug` text NOT NULL,
  `pst_txt` text DEFAULT NULL,
  `pst_status` char(10) NOT NULL DEFAULT 'draft',
  `pst_cat` int(255) DEFAULT NULL,
  `pst_guid` varchar(255) NOT NULL,
  `created_by` varchar(255) DEFAULT 'anonim',
  `modified_by` varchar(255) DEFAULT 'anonim',
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `grgl_postsmeta`
--

CREATE TABLE `grgl_postsmeta` (
  `meta_id` int(255) NOT NULL,
  `pst_id` int(255) NOT NULL,
  `meta_key` varchar(255) NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `grgl_socialmedia`
--

CREATE TABLE `grgl_socialmedia` (
  `soc_id` int(255) NOT NULL,
  `soc_name` varchar(255) NOT NULL,
  `soc_guid` varchar(255) DEFAULT NULL,
  `soc_icon` varchar(255) DEFAULT NULL,
  `modified_by` varchar(255) NOT NULL DEFAULT 'anonim',
  `modified_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `grgl_user`
--

CREATE TABLE `grgl_user` (
  `usr_id` int(255) NOT NULL,
  `usr_nick` varchar(255) NOT NULL,
  `usr_psswrd` varchar(255) NOT NULL,
  `usr_name` varchar(255) NOT NULL,
  `usr_stat` char(255) NOT NULL DEFAULT 'deactive',
  `created_by` varchar(255) DEFAULT 'anonim',
  `modified_by` varchar(255) DEFAULT 'anonim',
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `grgl_video`
--

CREATE TABLE `grgl_video` (
  `vdo_id` int(255) NOT NULL,
  `vdo_head` varchar(255) NOT NULL,
  `vdo_slug` varchar(255) NOT NULL,
  `vdo_videoname` varchar(255) DEFAULT 'NULL',
  `vdo_imgname` varchar(255) DEFAULT 'NULL',
  `vdo_link` varchar(255) NOT NULL,
  `vdo_img` varchar(255) DEFAULT 'NULL',
  `vdo_guid` varchar(255) NOT NULL,
  `vdo_type` varchar(255) DEFAULT 'video/web',
  `created_by` varchar(255) DEFAULT 'anonim',
  `modified_by` varchar(255) DEFAULT 'anonim',
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `grgl_category`
--
ALTER TABLE `grgl_category`
  ADD PRIMARY KEY (`ctr_id`),
  ADD UNIQUE KEY `ctr_slug` (`ctr_slug`),
  ADD UNIQUE KEY `ctr_guid` (`ctr_guid`);

--
-- Indexes for table `grgl_categorymeta`
--
ALTER TABLE `grgl_categorymeta`
  ADD PRIMARY KEY (`meta_id`);

--
-- Indexes for table `grgl_img`
--
ALTER TABLE `grgl_img`
  ADD PRIMARY KEY (`img_id`),
  ADD UNIQUE KEY `img_guid` (`img_guid`),
  ADD UNIQUE KEY `img_name` (`img_name`);

--
-- Indexes for table `grgl_imgmeta`
--
ALTER TABLE `grgl_imgmeta`
  ADD PRIMARY KEY (`meta_id`);

--
-- Indexes for table `grgl_posts`
--
ALTER TABLE `grgl_posts`
  ADD PRIMARY KEY (`pst_id`),
  ADD UNIQUE KEY `pst_slug` (`pst_slug`) USING HASH,
  ADD UNIQUE KEY `pst_guid` (`pst_guid`);

--
-- Indexes for table `grgl_postsmeta`
--
ALTER TABLE `grgl_postsmeta`
  ADD PRIMARY KEY (`meta_id`);

--
-- Indexes for table `grgl_socialmedia`
--
ALTER TABLE `grgl_socialmedia`
  ADD PRIMARY KEY (`soc_id`);

--
-- Indexes for table `grgl_user`
--
ALTER TABLE `grgl_user`
  ADD PRIMARY KEY (`usr_id`),
  ADD UNIQUE KEY `usr_nick` (`usr_nick`);

--
-- Indexes for table `grgl_video`
--
ALTER TABLE `grgl_video`
  ADD PRIMARY KEY (`vdo_id`),
  ADD UNIQUE KEY `vdo_slug` (`vdo_slug`),
  ADD UNIQUE KEY `vdo_guid` (`vdo_guid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `grgl_category`
--
ALTER TABLE `grgl_category`
  MODIFY `ctr_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grgl_categorymeta`
--
ALTER TABLE `grgl_categorymeta`
  MODIFY `meta_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grgl_img`
--
ALTER TABLE `grgl_img`
  MODIFY `img_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grgl_imgmeta`
--
ALTER TABLE `grgl_imgmeta`
  MODIFY `meta_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grgl_posts`
--
ALTER TABLE `grgl_posts`
  MODIFY `pst_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grgl_postsmeta`
--
ALTER TABLE `grgl_postsmeta`
  MODIFY `meta_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grgl_socialmedia`
--
ALTER TABLE `grgl_socialmedia`
  MODIFY `soc_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grgl_user`
--
ALTER TABLE `grgl_user`
  MODIFY `usr_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grgl_video`
--
ALTER TABLE `grgl_video`
  MODIFY `vdo_id` int(255) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
